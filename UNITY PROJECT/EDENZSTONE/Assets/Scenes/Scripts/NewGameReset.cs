﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewGameReset : MonoBehaviour
{

    // This class resets all game variables to default values
    // And resets all GUI texts with these values
    // Used when starting a new game


    //-------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
    // panelCharacterCreation
    //-------------------------------------------------------------------------------------------------------------------------------------------------------------------- //

    public InputField inputCommanderName;                          // REFERENCE: The custom commander name entered by the player


    //-------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
    // panelCharacterSetup
    //-------------------------------------------------------------------------------------------------------------------------------------------------------------------- //

    public Text txtCharacterSetupHP;
    public Text txtCharacterSetupSP;
    public Text txtCharacterSetupMP;
    public Text txtCharacterSetupDX;
    public Text txtCharacterSetupSTR;
    public Text txtCharacterSetupSPD;
    public Text txtCharacterSetupINT;

    public Text txtCharacterSetupVitalPoints;
    public Text txtCharacterSetupSecondaryPoints;
    public Text txtCharacterSetupName;


    //-------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
    // panelAdventureMain
    //-------------------------------------------------------------------------------------------------------------------------------------------------------------------- //

    public Text txtAdventureMainCharacterName;

    public Text txtAdventureDMGEarth;
    public Text txtAdventureDMGMetal;
    public Text txtAdventureDMGWater;
    public Text txtAdventureDMGWood;
    public Text txtAdventureDMGFire;

    public Text txtAdventureSHIELDEarth;
    public Text txtAdventureSHIELDMetal;
    public Text txtAdventureSHIELDWater;
    public Text txtAdventureSHIELDWood;
    public Text txtAdventureSHIELDFire;

    public Text txtAdventureSHIELDRegenEarth;
    public Text txtAdventureSHIELDRegenMetal;
    public Text txtAdventureSHIELDRegenWater;
    public Text txtAdventureSHIELDRegenWood;
    public Text txtAdventureSHIELDRegenFire;

    public Text txtAdventureHPCurrent;
    public Text txtAdventureSPCurrent;
    public Text txtAdventureMPCurrent;

    public Text txtAdventureHPMax;
    public Text txtAdventureSPMax;
    public Text txtAdventureMPMax;

    public Text txtAdventureHPRegen;
    public Text txtAdventureSPRegen;
    public Text txtAdventureMPRegen;

    public Text txtAdventureDX;
    public Text txtAdventureSTR;
    public Text txtAdventureSPD;
    public Text txtAdventureINT;

    public Text txtTabAffinitiesAMPHIBIAN;
    public Text txtTabAffinitiesAQUATIC;
    public Text txtTabAffinitiesAVIAN;
    public Text txtTabAffinitiesBEAST;
    public Text txtTabAffinitiesBRUTE;
    public Text txtTabAffinitiesBUG;
    public Text txtTabAffinitiesDEMON;
    public Text txtTabAffinitiesDRAGON;
    public Text txtTabAffinitiesELEMENTAL;
    public Text txtTabAffinitiesETHEREAL;
    public Text txtTabAffinitiesHUMAN;
    public Text txtTabAffinitiesPHYTO;
    public Text txtTabAffinitiesREPTILIAN;
    public Text txtTabAffinitiesUNDEAD;

    public Text txtAdventureMainChainElemental;
    public Text txtAdventureMainChainAffinity;
    public Text txtAdventureMainChainCost;
    public Text txtAdventureMainChainTotal;

    public Text txtTabGoldTOTAL;
    public Text txtTabGoldINCOME;
    public Text txtTabGoldCREATURES;

    public Slider sliderHP;                                     // REFERENCE: The sliders for HP / SP / MP / DX / STR / SPD / INT
    public Slider sliderSP;
    public Slider sliderMP;
    public Slider sliderDX;
    public Slider sliderSTR;
    public Slider sliderSPD;
    public Slider sliderINT;


    public void defaultAllPhysicalFields()                              // Run resetMainCharacter() FIRST, then this (run from NEW GAME button in main menu)
    {
        inputCommanderName.text = MainCharacter.name;                   // The textbox in panelCharacterCreation
        txtCharacterSetupName.text = MainCharacter.name;                // The character name shown in panelCharacterSetup

        txtCharacterSetupHP.text = MainCharacter.HPmax.ToString();      // The text labels in panelCharacterSetup  
        txtCharacterSetupSP.text = MainCharacter.SPmax.ToString();
        txtCharacterSetupMP.text = MainCharacter.MPmax.ToString();
        txtCharacterSetupDX.text = MainCharacter.DX.ToString();
        txtCharacterSetupSTR.text = MainCharacter.STR.ToString();
        txtCharacterSetupSPD.text = MainCharacter.SPD.ToString();
        txtCharacterSetupINT.text = MainCharacter.INT.ToString();

        txtCharacterSetupVitalPoints.text = MainCharacter.maxVitalPoints.ToString();
        txtCharacterSetupSecondaryPoints.text = MainCharacter.maxSecondaryPoints.ToString();

        txtAdventureMainCharacterName.text = MainCharacter.name;

        txtAdventureDMGEarth.text = MainCharacter.damageEARTH.ToString();
        txtAdventureDMGMetal.text = MainCharacter.damageMETAL.ToString();
        txtAdventureDMGWater.text = MainCharacter.damageWATER.ToString();
        txtAdventureDMGWood.text = MainCharacter.damageWOOD.ToString();
        txtAdventureDMGFire.text = MainCharacter.damageFIRE.ToString();

        txtAdventureSHIELDEarth.text = MainCharacter.shieldEARTH.ToString();
        txtAdventureSHIELDMetal.text = MainCharacter.shieldMETAL.ToString();
        txtAdventureSHIELDWater.text = MainCharacter.shieldWATER.ToString();
        txtAdventureSHIELDWood.text = MainCharacter.shieldWOOD.ToString();
        txtAdventureSHIELDFire.text = MainCharacter.shieldFIRE.ToString();

        txtAdventureSHIELDRegenEarth.text = MainCharacter.shieldEARTHregen.ToString();
        txtAdventureSHIELDRegenMetal.text = MainCharacter.shieldMETALregen.ToString();
        txtAdventureSHIELDRegenWater.text = MainCharacter.shieldWATERregen.ToString();
        txtAdventureSHIELDRegenWood.text = MainCharacter.shieldWOODregen.ToString();
        txtAdventureSHIELDRegenFire.text = MainCharacter.shieldFIREregen.ToString();

        txtAdventureHPCurrent.text = MainCharacter.HPcurrent.ToString();
        txtAdventureSPCurrent.text = MainCharacter.SPcurrent.ToString();
        txtAdventureMPCurrent.text = MainCharacter.MPcurrent.ToString();

        txtAdventureHPMax.text = MainCharacter.HPmax.ToString();
        txtAdventureSPMax.text = MainCharacter.SPmax.ToString();
        txtAdventureMPMax.text = MainCharacter.MPmax.ToString();

        txtAdventureHPRegen.text = "+" + MainCharacter.HPregen;
        txtAdventureSPRegen.text = "+" + MainCharacter.SPregen;
        txtAdventureMPRegen.text = "+" + MainCharacter.MPregen;

        txtAdventureDX.text = MainCharacter.DX.ToString();
        txtAdventureSTR.text = MainCharacter.STR.ToString();
        txtAdventureSPD.text = MainCharacter.SPD.ToString();
        txtAdventureINT.text = MainCharacter.INT.ToString();

        txtTabAffinitiesAMPHIBIAN.text = MainCharacter.affinityAMPHIBIAN.ToString();
        txtTabAffinitiesAQUATIC.text = MainCharacter.affinityAQUATIC.ToString();
        txtTabAffinitiesAVIAN.text = MainCharacter.affinityAVIAN.ToString();
        txtTabAffinitiesBEAST.text = MainCharacter.affinityBEAST.ToString();
        txtTabAffinitiesBRUTE.text = MainCharacter.affinityBRUTE.ToString();
        txtTabAffinitiesBUG.text = MainCharacter.affinityBUG.ToString();
        txtTabAffinitiesDEMON.text = MainCharacter.affinityDEMON.ToString();
        txtTabAffinitiesDRAGON.text = MainCharacter.affinityDRAGON.ToString();
        txtTabAffinitiesELEMENTAL.text = MainCharacter.affinityELEMENTAL.ToString();
        txtTabAffinitiesETHEREAL.text = MainCharacter.affinityETHEREAL.ToString();
        txtTabAffinitiesHUMAN.text = MainCharacter.affinityHUMAN.ToString();
        txtTabAffinitiesPHYTO.text = MainCharacter.affinityPHYTO.ToString();
        txtTabAffinitiesREPTILIAN.text = MainCharacter.affinityREPTILIAN.ToString();
        txtTabAffinitiesUNDEAD.text = MainCharacter.affinityUNDEAD.ToString();

        txtAdventureMainChainElemental.text = MainCharacter.chainLengthElemental.ToString();
        txtAdventureMainChainAffinity.text = MainCharacter.chainLengthRacial.ToString();
        txtAdventureMainChainCost.text = MainCharacter.chainLengthCost.ToString();
        txtAdventureMainChainTotal.text = MainCharacter.chainLengthTotal.ToString();

        txtTabGoldTOTAL.text = MainCharacter.goldTOTAL.ToString();
        txtTabGoldINCOME.text = "+" + MainCharacter.goldINCOME.ToString();
        txtTabGoldCREATURES.text = MainCharacter.goldCREATURES.ToString();

        sliderHP.value = 0;
        sliderSP.value = 0;
        sliderMP.value = 0;
        sliderDX.value = 0;
        sliderSTR.value = 0;
        sliderSPD.value = 0;
        sliderINT.value = 0;
}

        
    public void resetMainCharacter()                     // Used to reset all values to default when a new character is created (run from NEW GAME button in main menu)
    {
        MainCharacter.name = "";
        MainCharacter.race = "";
        MainCharacter.chosenCommander = "";

        MainCharacter.HPmax = 100;                      // Default start values for sliders in panelCharacterSetup
        MainCharacter.SPmax = 50;
        MainCharacter.MPmax = 50;

        MainCharacter.HPcurrent = 100;                  
        MainCharacter.SPcurrent = 50;
        MainCharacter.MPcurrent = 50;

        MainCharacter.HPcurrentPercent = 1.0f;
        MainCharacter.SPcurrentPercent = 1.0f;
        MainCharacter.MPcurrentPercent = 1.0f;

        MainCharacter.HPregen = 5;                      // First seen in panelAdventureMain
        MainCharacter.SPregen = 5;
        MainCharacter.MPregen = 5;

        MainCharacter.HPstartValue = MainCharacter.HPmax;   // These are only needed in CharacterCreation.cs to calculate new HPmax value by adding current slider value to starting HP value      
        MainCharacter.SPstartValue = MainCharacter.SPmax;
        MainCharacter.MPstartValue = MainCharacter.MPmax;

        MainCharacter.DX = 10;                          // Default start values above sliders in panelCharacterSetup
        MainCharacter.STR = 10;
        MainCharacter.SPD = 10;
        MainCharacter.INT = 10;

        MainCharacter.DXstartValue = MainCharacter.DX;
        MainCharacter.STRstartValue = MainCharacter.STR;
        MainCharacter.SPDstartValue = MainCharacter.SPD;
        MainCharacter.INTstartValue = MainCharacter.INT;

        MainCharacter.maxVitalPoints = 200;
        MainCharacter.currentVitalPoints = 200;

        MainCharacter.maxSecondaryPoints = 100;
        MainCharacter.currentSecondaryPoints = 100;

        MainCharacter.damageEARTH = 5;                  // First seen in panelAdventureMain
        MainCharacter.damageMETAL = 5;
        MainCharacter.damageWATER = 5;
        MainCharacter.damageWOOD = 5;
        MainCharacter.damageFIRE = 5;

        MainCharacter.damageTOTAL = 25;

        MainCharacter.shieldEARTH = 10;                 // First seen in panelAdventureMain
        MainCharacter.shieldMETAL = 10;
        MainCharacter.shieldWATER = 10;
        MainCharacter.shieldWOOD = 10;
        MainCharacter.shieldFIRE = 10;

        MainCharacter.shieldEARTHregen = 1;             // First seen in panelAdventureMain
        MainCharacter.shieldMETALregen = 1;
        MainCharacter.shieldWATERregen = 1;
        MainCharacter.shieldWOODregen = 1;
        MainCharacter.shieldFIREregen = 1;

        MainCharacter.affinityAMPHIBIAN = 0;    // Initially everything 0 - Player will get +50 to the affinity of their own race
        MainCharacter.affinityAQUATIC = 0;
        MainCharacter.affinityAVIAN = 0;
        MainCharacter.affinityBEAST = 0;
        MainCharacter.affinityBRUTE = 0;
        MainCharacter.affinityBUG = 0;
        MainCharacter.affinityDEMON = 0;
        MainCharacter.affinityDRAGON = 0;
        MainCharacter.affinityELEMENTAL = 0;
        MainCharacter.affinityETHEREAL = 0;
        MainCharacter.affinityHUMAN = 0;
        MainCharacter.affinityPHYTO = 0;
        MainCharacter.affinityREPTILIAN = 0;
        MainCharacter.affinityUNDEAD = 0;

        MainCharacter.goldTOTAL = 100;
        MainCharacter.goldINCOME = 10;
        MainCharacter.goldCREATURES = 0;

        MainCharacter.chainLengthElemental = 2;
        MainCharacter.chainLengthRacial = 2;
        MainCharacter.chainLengthCost = 2;
        MainCharacter.chainLengthTotal = 6;
}


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


}
