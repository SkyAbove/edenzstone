﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MainCharacter {

    public static string name;                          // The final name of the character, whether chosen or default
    public static string race;
    public static string chosenCommander;               // The name of the player's chosen commander - Determines portrait displayed - Passed as a parameter by the image's onClick() event in panelCharacterCreation

    public static int HPmax;                            // The max values for max HP / SP / MP / DX / STR / SPD / INT  (used in panelCharacterSetup sliders)
    public static int SPmax;
    public static int MPmax;

    public static int HPcurrent;
    public static int SPcurrent;
    public static int MPcurrent;

    public static float HPcurrentPercent;
    public static float SPcurrentPercent;
    public static float MPcurrentPercent;

    public static int HPregen;
    public static int SPregen;
    public static int MPregen;

    public static int HPstartValue;                     // The default starting values for HP / SP / MP
    public static int SPstartValue;                     // Needed in CharacterCreation.cs to calculate the new value for HPmax / SPmax / MPmax
    public static int MPstartValue;                     // (By adding current slider value to starting value)

    public static int DX;
    public static int STR;
    public static int SPD;
    public static int INT;

    public static int DXstartValue;                     // The default starting values for DX / STR / SPD / INT
    public static int STRstartValue;                    // Needed in CharacterCreation.cs to calculate the new value for DX / STR / SPD / INT
    public static int SPDstartValue;                    // (By adding current slider value to starting value)
    public static int INTstartValue;                    

    public static int maxVitalPoints;                           // The max available starting points for HP / SP / MP allocation (used in panelCharacterSetup)
    public static int currentVitalPoints;                       // The number of points left to allocate

    public static int maxSecondaryPoints;                       // The max available starting points for DX / STR / SPD / INT allocation
    public static int currentSecondaryPoints;                   // The number of points left to allocate

    public static int damageEARTH;
    public static int damageMETAL;
    public static int damageWATER;
    public static int damageWOOD;
    public static int damageFIRE;

    public static int damageTOTAL;

    public static int shieldEARTH;
    public static int shieldMETAL;
    public static int shieldWATER;
    public static int shieldWOOD;
    public static int shieldFIRE;

    public static int shieldEARTHregen;
    public static int shieldMETALregen;
    public static int shieldWATERregen;
    public static int shieldWOODregen;
    public static int shieldFIREregen;

    public static int affinityAMPHIBIAN;
    public static int affinityAQUATIC;
    public static int affinityAVIAN;
    public static int affinityBEAST;
    public static int affinityBRUTE;
    public static int affinityBUG;
    public static int affinityDEMON;
    public static int affinityDRAGON;
    public static int affinityELEMENTAL;
    public static int affinityETHEREAL;
    public static int affinityHUMAN;
    public static int affinityPHYTO;
    public static int affinityREPTILIAN;
    public static int affinityUNDEAD;

    public static int goldTOTAL;
    public static int goldINCOME;
    public static int goldCREATURES;

    public static int chainLengthElemental;
    public static int chainLengthRacial;
    public static int chainLengthCost;
    public static int chainLengthTotal;

}
