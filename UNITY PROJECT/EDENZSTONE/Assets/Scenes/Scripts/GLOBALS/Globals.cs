﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Globals {

    public static string currentPortraitGroup;                // The currently active group of portraits in panelCharacterCreation
    public static string chosenCommander;                     // The name of the player's chosen commander
    public static string playerName;                          // The custom commander name entered by the player

    public static bool characterHasBeenSetup;                        // Only populate commander panel in AdventureMain if a character has already been selected and set up 
}
