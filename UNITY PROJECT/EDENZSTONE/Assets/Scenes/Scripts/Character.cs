﻿/*using UnityEngine;

public class Character : MonoBehaviour {

	// ----------------------------------------------------------------------------------------------------------------------------
	// PROPERTIES
	// ----------------------------------------------------------------------------------------------------------------------------

		// ------- BASIC STATS ------------------------------------------------------------------------------

			string name;
			string race;
			int cost;

		// ------- VITAL STATS ------------------------------------------------------------------------------

			int HPmax, SPmax, MPmax;
			int HPcurrent, SPcurrent, MPcurrent;
			int HPregen, SPregen, MPregen;		

		// ------- SECONDARY STATS ------------------------------------------------------------------------------

			int dexterity, strength, speed, intelligence;

		// ------- ELEMENTAL STATS ------------------------------------------------------------------------------

			int attackTotal;																				// Total damage inflicted per attack (all elements combined)
			int attackEarth, attackMetal, attackWater, attackWood, attackFire;								// Character's attack deals this much damage in each element
			int shieldEarth, shieldMetal, shieldWater, shieldWood, shieldFire;								// Current value of each shield							
			int shieldEarthRegen, shieldMetalRegen, shieldWaterRegen, shieldWoodRegen, shieldFireRegen;		// Number of points regenerated in each shield per round
			int damageTakenTotal;																			// Total amount of damage taken so far (all elements combined)
			int damageTakenEarth, damageTakenMetal, damageTakenWater, damageTakenWood, damageTakenFire;		// All damage received so far in that element
			int damageDealtEarth, damageDealtMetal, damageDealtWater, damageDealtWood, damageDealtFire;		// All damage dealt so far in that element

			
		// ------- FACTION AFFINITIES ---------------------------------------------------------------------------
			
			int faBug, faPhyto, faAmphibian, faAquatic, faAvian, faBeast, faDragon, faReptilian, fabrute, faHuman, faDemon, faUndead, faElemental, faEthereal;

		// ------- EFFECTS --------------------------------------------------------------------------------------

			bool hasWard = false;				// if character currently has WARD enabled
			bool hasStealth = false;			// if character currently has STEALTH enabled
			bool hasTurnPending = false;		// if character is yet to have its turn this round
			bool hasBlessing = false;			// if a BLESSING spell is currently active on the character
			bool hasCurse = false;				// if a CURSE spell is currently active on the character
			bool hasSurvived = false;			// if character has survived one attack
			bool isWounded = false;				// if character's CURRENT HP is less than its MAX HP
			bool isTired = false;				// if character's CURRENT SP is less than its MAX SP
			bool isSpent = false;				// if character's CURRENT MP is less than its MAX MP
			bool hasAbstained = false;			// if character has taken offensive action this round or not
			bool isDefending = false;			// if character is currently defending
			bool hasLostAnyShield = false;		// if one of the character's shields (in any element) has reached 0
			bool hasLostEarthShield = false;	// if character has lost any amount from their original Metal Shield value
			bool hasLostMetalShield = false;	// if character has lost any amount from their original Metal Shield value
			bool hasLostWaterShield = false;	// if character has lost any amount from their original Water Shield value
			bool hasLostWoodShield = false;		// if character has lost any amount from their original Wood Shield value
			bool hasLostFireShield = false;		// if character has lost any amount from their original Fire Shield value


}
*/