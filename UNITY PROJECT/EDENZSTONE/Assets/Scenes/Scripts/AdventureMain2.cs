﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdventureMain2 : MonoBehaviour
{

    public Sprite spriteCostAffinitiesAMPHIBIAN;
    public Sprite spriteCostAffinitiesAQUATIC;
    public Sprite spriteCostAffinitiesAVIAN;
    public Sprite spriteCostAffinitiesBEAST;
    public Sprite spriteCostAffinitiesBRUTE;
    public Sprite spriteCostAffinitiesBUG;
    public Sprite spriteCostAffinitiesDEMON;
    public Sprite spriteCostAffinitiesDRAGON;
    public Sprite spriteCostAffinitiesELEMENTAL;
    public Sprite spriteCostAffinitiesETHEREAL;
    public Sprite spriteCostAffinitiesHUMAN;
    public Sprite spriteCostAffinitiesPHYTO;
    public Sprite spriteCostAffinitiesREPTILIAN;
    public Sprite spriteCostAffinitiesUNDEAD;

    public Sprite spriteCostGoldAMOUNT;
    public Sprite spriteCostGoldINCOME;

    public Sprite spriteRewardGoldAMOUNT;
    public Sprite spriteRewardGoldINCOME;

    public Sprite spriteCostStatisticsATTACK_EARTH;
    public Sprite spriteCostStatisticsATTACK_METAL;
    public Sprite spriteCostStatisticsATTACK_WATER;
    public Sprite spriteCostStatisticsATTACK_WOOD;
    public Sprite spriteCostStatisticsATTACK_FIRE;

    public Sprite spriteCostStatisticsSHIELD_EARTH;
    public Sprite spriteCostStatisticsSHIELD_METAL;
    public Sprite spriteCostStatisticsSHIELD_WATER;
    public Sprite spriteCostStatisticsSHIELD_WOOD;
    public Sprite spriteCostStatisticsSHIELD_FIRE;

    public Sprite spriteCostStatisticsSHIELD_EARTH_REGEN;
    public Sprite spriteCostStatisticsSHIELD_METAL_REGEN;
    public Sprite spriteCostStatisticsSHIELD_WATER_REGEN;
    public Sprite spriteCostStatisticsSHIELD_WOOD_REGEN;
    public Sprite spriteCostStatisticsSHIELD_FIRE_REGEN;

    public Sprite spriteCostStatisticsDX;
    public Sprite spriteCostStatisticsSTR;
    public Sprite spriteCostStatisticsSPD;
    public Sprite spriteCostStatisticsINT;

    public Sprite spriteCostStatisticsHP_MAX;
    public Sprite spriteCostStatisticsHP_REGEN;
    public Sprite spriteCostStatisticsSP_MAX;
    public Sprite spriteCostStatisticsSP_REGEN;
    public Sprite spriteCostStatisticsMP_MAX;
    public Sprite spriteCostStatisticsMP_REGEN;

    public Sprite spriteCostCreaturesYOUNG_MARSH_DRAKE;
    public Sprite spriteCostCreaturesFROST_WORM;
    public Sprite spriteCostCreaturesGRIFFIN;
    public Sprite spriteCostCreaturesSHADE;
    public Sprite spriteCostCreaturesTHUNDER_BRUTE;
    public Sprite spriteCostCreaturesFIRE_ELEMENTAL;
    public Sprite spriteCostCreaturesTAMORI;
    public Sprite spriteCostCreaturesLIGHTNING_DRAGON;
    public Sprite spriteCostCreaturesNIGHTCALLER;
    public Sprite spriteCostCreaturesMIST_WEAVER;

    public Sprite spriteCostSpellsARCANE_BRILLIANCE;
    public Sprite spriteCostSpellsBLESS;
    public Sprite spriteCostSpellsBLIND;
    public Sprite spriteCostSpellsENRAGE;
    public Sprite spriteCostSpellsGUARDIAN;
    public Sprite spriteCostSpellsINFERNO;
    public Sprite spriteCostSpellsMIRROR_IMAGE;
    public Sprite spriteCostSpellsRAISE_SKELETON;
    public Sprite spriteCostSpellsSLAYER;
    public Sprite spriteCostSpellsSNIPE;

    public Sprite spriteCostPotionsPOISON_ATTACK_EARTH;
    public Sprite spriteCostPotionsPOISON_ATTACK_METAL;
    public Sprite spriteCostPotionsPOISON_ATTACK_WATER;
    public Sprite spriteCostPotionsPOISON_ATTACK_WOOD;
    public Sprite spriteCostPotionsPOISON_ATTACK_FIRE;
    public Sprite spriteCostPotionsPOISON_SHIELD_EARTH;
    public Sprite spriteCostPotionsPOISON_SHIELD_METAL;
    public Sprite spriteCostPotionsPOISON_SHIELD_WATER;
    public Sprite spriteCostPotionsPOISON_SHIELD_WOOD;
    public Sprite spriteCostPotionsPOISON_SHIELD_FIRE;
    public Sprite spriteCostPotionsPOISON_SHIELD_EARTH_REGEN;
    public Sprite spriteCostPotionsPOISON_SHIELD_METAL_REGEN;
    public Sprite spriteCostPotionsPOISON_SHIELD_WATER_REGEN;
    public Sprite spriteCostPotionsPOISON_SHIELD_WOOD_REGEN;
    public Sprite spriteCostPotionsPOISON_SHIELD_FIRE_REGEN;
    public Sprite spriteCostPotionsPOISON_DX;
    public Sprite spriteCostPotionsPOISON_STR;
    public Sprite spriteCostPotionsPOISON_SPD;
    public Sprite spriteCostPotionsPOISON_INT;
    public Sprite spriteCostPotionsPOISON_HP_CURRENT;
    public Sprite spriteCostPotionsPOISON_HP_MAX;
    public Sprite spriteCostPotionsPOISON_HP_REGEN;
    public Sprite spriteCostPotionsPOISON_SP_CURRENT;
    public Sprite spriteCostPotionsPOISON_SP_MAX;
    public Sprite spriteCostPotionsPOISON_SP_REGEN;
    public Sprite spriteCostPotionsPOISON_MP_CURRENT;
    public Sprite spriteCostPotionsPOISON_MP_MAX;
    public Sprite spriteCostPotionsPOISON_MP_REGEN;

    public Sprite spriteCostPotionsPOTION_ATTACK_EARTH;
    public Sprite spriteCostPotionsPOTION_ATTACK_METAL;
    public Sprite spriteCostPotionsPOTION_ATTACK_WATER;
    public Sprite spriteCostPotionsPOTION_ATTACK_WOOD;
    public Sprite spriteCostPotionsPOTION_ATTACK_FIRE;
    public Sprite spriteCostPotionsPOTION_SHIELD_EARTH;
    public Sprite spriteCostPotionsPOTION_SHIELD_METAL;
    public Sprite spriteCostPotionsPOTION_SHIELD_WATER;
    public Sprite spriteCostPotionsPOTION_SHIELD_WOOD;
    public Sprite spriteCostPotionsPOTION_SHIELD_FIRE;
    public Sprite spriteCostPotionsPOTION_SHIELD_EARTH_REGEN;
    public Sprite spriteCostPotionsPOTION_SHIELD_METAL_REGEN;
    public Sprite spriteCostPotionsPOTION_SHIELD_WATER_REGEN;
    public Sprite spriteCostPotionsPOTION_SHIELD_WOOD_REGEN;
    public Sprite spriteCostPotionsPOTION_SHIELD_FIRE_REGEN;
    public Sprite spriteCostPotionsPOTION_DX;
    public Sprite spriteCostPotionsPOTION_STR;
    public Sprite spriteCostPotionsPOTION_SPD;
    public Sprite spriteCostPotionsPOTION_INT;
    public Sprite spriteCostPotionsPOTION_HP_CURRENT;
    public Sprite spriteCostPotionsPOTION_HP_MAX;
    public Sprite spriteCostPotionsPOTION_HP_REGEN;
    public Sprite spriteCostPotionsPOTION_SP_CURRENT;
    public Sprite spriteCostPotionsPOTION_SP_MAX;
    public Sprite spriteCostPotionsPOTION_SP_REGEN;
    public Sprite spriteCostPotionsPOTION_MP_CURRENT;
    public Sprite spriteCostPotionsPOTION_MP_MAX;
    public Sprite spriteCostPotionsPOTION_MP_REGEN;

    public Sprite spriteCostSupportsANKH;
    public Sprite spriteCostSupportsCURSED_URN;
    public Sprite spriteCostSupportsDRAGON_RING;
    public Sprite spriteCostSupportsHEAVY_BALLISTA;
    public Sprite spriteCostSupportsLION_SHIELD;
    public Sprite spriteCostSupportsMAGMA_ORB;
    public Sprite spriteCostSupportsPALADIN_SWORD;
    public Sprite spriteCostSupportsRUBY_GAUNTLET;
    public Sprite spriteCostSupportsSOUL_AMULET;
    public Sprite spriteCostSupportsWIZARD_STAFF;



    public Sprite spriteRewardAffinitiesAMPHIBIAN;
    public Sprite spriteRewardAffinitiesAQUATIC;
    public Sprite spriteRewardAffinitiesAVIAN;
    public Sprite spriteRewardAffinitiesBEAST;
    public Sprite spriteRewardAffinitiesBRUTE;
    public Sprite spriteRewardAffinitiesBUG;
    public Sprite spriteRewardAffinitiesDEMON;
    public Sprite spriteRewardAffinitiesDRAGON;
    public Sprite spriteRewardAffinitiesELEMENTAL;
    public Sprite spriteRewardAffinitiesETHEREAL;
    public Sprite spriteRewardAffinitiesHUMAN;
    public Sprite spriteRewardAffinitiesPHYTO;
    public Sprite spriteRewardAffinitiesREPTILIAN;
    public Sprite spriteRewardAffinitiesUNDEAD;

    public Sprite spriteRewardStatisticsATTACK_EARTH;
    public Sprite spriteRewardStatisticsATTACK_METAL;
    public Sprite spriteRewardStatisticsATTACK_WATER;
    public Sprite spriteRewardStatisticsATTACK_WOOD;
    public Sprite spriteRewardStatisticsATTACK_FIRE;

    public Sprite spriteRewardStatisticsSHIELD_EARTH;
    public Sprite spriteRewardStatisticsSHIELD_METAL;
    public Sprite spriteRewardStatisticsSHIELD_WATER;
    public Sprite spriteRewardStatisticsSHIELD_WOOD;
    public Sprite spriteRewardStatisticsSHIELD_FIRE;

    public Sprite spriteRewardStatisticsSHIELD_EARTH_REGEN;
    public Sprite spriteRewardStatisticsSHIELD_METAL_REGEN;
    public Sprite spriteRewardStatisticsSHIELD_WATER_REGEN;
    public Sprite spriteRewardStatisticsSHIELD_WOOD_REGEN;
    public Sprite spriteRewardStatisticsSHIELD_FIRE_REGEN;

    public Sprite spriteRewardStatisticsDX;
    public Sprite spriteRewardStatisticsSTR;
    public Sprite spriteRewardStatisticsSPD;
    public Sprite spriteRewardStatisticsINT;

    public Sprite spriteRewardStatisticsHP_MAX;
    public Sprite spriteRewardStatisticsHP_REGEN;
    public Sprite spriteRewardStatisticsSP_MAX;
    public Sprite spriteRewardStatisticsSP_REGEN;
    public Sprite spriteRewardStatisticsMP_MAX;
    public Sprite spriteRewardStatisticsMP_REGEN;

    public Sprite spriteRewardCreaturesYOUNG_MARSH_DRAKE;
    public Sprite spriteRewardCreaturesFROST_WORM;
    public Sprite spriteRewardCreaturesGRIFFIN;
    public Sprite spriteRewardCreaturesSHADE;
    public Sprite spriteRewardCreaturesTHUNDER_BRUTE;
    public Sprite spriteRewardCreaturesFIRE_ELEMENTAL;
    public Sprite spriteRewardCreaturesTAMORI;
    public Sprite spriteRewardCreaturesLIGHTNING_DRAGON;
    public Sprite spriteRewardCreaturesNIGHTCALLER;
    public Sprite spriteRewardCreaturesMIST_WEAVER;

    public Sprite spriteRewardSpellsARCANE_BRILLIANCE;
    public Sprite spriteRewardSpellsBLESS;
    public Sprite spriteRewardSpellsBLIND;
    public Sprite spriteRewardSpellsENRAGE;
    public Sprite spriteRewardSpellsGUARDIAN;
    public Sprite spriteRewardSpellsINFERNO;
    public Sprite spriteRewardSpellsMIRROR_IMAGE;
    public Sprite spriteRewardSpellsRAISE_SKELETON;
    public Sprite spriteRewardSpellsSLAYER;
    public Sprite spriteRewardSpellsSNIPE;

    public Sprite spriteRewardSupportsANKH;
    public Sprite spriteRewardSupportsCURSED_URN;
    public Sprite spriteRewardSupportsDRAGON_RING;
    public Sprite spriteRewardSupportsHEAVY_BALLISTA;
    public Sprite spriteRewardSupportsLION_SHIELD;
    public Sprite spriteRewardSupportsMAGMA_ORB;
    public Sprite spriteRewardSupportsPALADIN_SWORD;
    public Sprite spriteRewardSupportsRUBY_GAUNTLET;
    public Sprite spriteRewardSupportsSOUL_AMULET;
    public Sprite spriteRewardSupportsWIZARD_STAFF;


    private string[] arrayGeneratedCostOrRewardTypes = new string[5];       // Holds the collection of randomly generated costs or rewards for the current encounter cycle

    private string[] arrayCostRewardTypesSTATISTICS = new string[] { "ATTACK_EARTH", "ATTACK_METAL", "ATTACK_WATER", "ATTACK_WOOD", "ATTACK_FIRE", "SHIELD_EARTH", "SHIELD_METAL", "SHIELD_WATER", "SHIELD_WOOD", "SHIELD_FIRE", "SHIELD_EARTH_REGEN", "SHIELD_METAL_REGEN", "SHIELD_WATER_REGEN", "SHIELD_WOOD_REGEN", "SHIELD_FIRE_REGEN", "DX", "STR", "SPD", "INT", "HP_MAX", "HP_REGEN", "SP_MAX", "SP_REGEN", "MP_MAX", "MP_REGEN" };
    private string[] arrayCostRewardTypesCREATURES = new string[] { "YOUNG_MARSH_DRAKE", "SHADE", "THUNDER_BRUTE", "FROST_WORM", "GRIFFIN", "FIRE_ELEMENTAL", "LIGHTNING_DRAGON", "MIST_WEAVER", "NIGHTCALLER", "TAMORI" };
    private string[] arrayCostRewardTypesSPELLS = new string[] { "ARCANE_BRILLIANCE", "BLESS", "BLIND", "ENRAGE", "GUARDIAN", "INFERNO", "MIRROR_IMAGE", "RAISE_SKELETON", "SLAYER", "SNIPE" };
    private string[] arrayCostRewardTypesPOTIONS = new string[] { "POISON_HP_CURRENT", "POISON_HP_MAX", "POISON_HP_REGEN", "POISON_SP_CURRENT", "POISON_SP_MAX", "POISON_SP_REGEN", "POISON_MP_CURRENT", "POISON_MP_MAX", "POISON_MP_REGEN", "POTION_HP_CURRENT", "POTION_HP_MAX", "POTION_HP_REGEN", "POTION_SP_CURRENT", "POTION_SP_MAX", "POTION_SP_REGEN", "POTION_MP_CURRENT", "POTION_MP_MAX", "POTION_MP_REGEN", "POISON_ATTACK_EARTH", "POISON_ATTACK_METAL", "POISON_ATTACK_WATER", "POISON_ATTACK_WOOD", "POISON_ATTACK_FIRE", "POTION_ATTACK_EARTH", "POTION_ATTACK_METAL", "POTION_ATTACK_WATER", "POTION_ATTACK_WOOD", "POTION_ATTACK_FIRE", "POISON_SHIELD_EARTH", "POISON_SHIELD_METAL", "POISON_SHIELD_WATER", "POISON_SHIELD_WOOD", "POISON_SHIELD_FIRE", "POTION_SHIELD_EARTH", "POTION_SHIELD_METAL", "POTION_SHIELD_WATER", "POTION_SHIELD_WOOD", "POTION_SHIELD_FIRE", "POISON_SHIELD_EARTH_REGEN", "POISON_SHIELD_METAL_REGEN", "POISON_SHIELD_WATER_REGEN", "POISON_SHIELD_WOOD_REGEN", "POISON_SHIELD_FIRE_REGEN", "POTION_SHIELD_EARTH_REGEN", "POTION_SHIELD_METAL_REGEN", "POTION_SHIELD_WATER_REGEN", "POTION_SHIELD_WOOD_REGEN", "POTION_SHIELD_FIRE_REGEN", "POISON_DX", "POTION_DX", "POISON_STR", "POTION_STR", "POISON_SPD", "POTION_SPD", "POISON_INT", "POTION_INT" };
    private string[] arrayCostRewardTypesSUPPORTS = new string[] { "ANKH", "CURSED_URN", "DRAGON_RING", "HEAVY_BALLISTA", "LION_SHIELD", "MAGMA_ORB", "PALADIN_SWORD", "RUBY_GAUNTLET", "SOUL_AMULET", "WIZARD_STAFF" };
    private string[] arrayCostRewardTypesAFFINITIES = new string[] { "AMPHIBIAN", "AQUATIC", "AVIAN", "BEAST", "BRUTE", "BUG", "DEMON", "DRAGON", "ELEMENTAL", "ETHEREAL", "HUMAN", "PHYTO", "REPTILIAN", "UNDEAD" };

    public Button[] arrayChoiceButtons = new Button[5];







    private string GenerateRandomCostOrRewardSTATISTICS()
    {
        int randomNum = Random.Range(0, arrayCostRewardTypesSTATISTICS.Length);                          // Choose a random cost from the costs array
        string chosenSTATISTIC = arrayCostRewardTypesSTATISTICS[randomNum];
        return chosenSTATISTIC;
    }

    private string GenerateRandomCostCREATURES()
    {
        int randomNum = Random.Range(0, arrayCostRewardTypesCREATURES.Length);                          // Choose a random cost from the costs array
        string chosenCREATURE = arrayCostRewardTypesCREATURES[randomNum];
        return chosenCREATURE;
    }

    private string GenerateRandomCostSPELLS()
    {
        int randomNum = Random.Range(0, arrayCostRewardTypesSPELLS.Length);                          // Choose a random cost from the costs array
        string chosenSPELL = arrayCostRewardTypesSPELLS[randomNum];
        return chosenSPELL;
    }

    private string GenerateRandomCostPOTIONS()
    {
        int randomNum = Random.Range(0, arrayCostRewardTypesPOTIONS.Length);                          // Choose a random cost from the costs array
        string chosenPOTION = arrayCostRewardTypesPOTIONS[randomNum];
        return chosenPOTION;
    }

    private string GenerateRandomCostSUPPORTS()
    {
        int randomNum = Random.Range(0, arrayCostRewardTypesSUPPORTS.Length);                          // Choose a random cost from the costs array
        string chosenSUPPORT = arrayCostRewardTypesSUPPORTS[randomNum];
        return chosenSUPPORT;
    }

    private string GenerateRandomCostAFFINITIES()
    {
        int randomNum = Random.Range(0, arrayCostRewardTypesAFFINITIES.Length);                          // Choose a random cost from the costs array
        string chosenAFFINITY = arrayCostRewardTypesAFFINITIES[randomNum];
        return chosenAFFINITY;
    }

    private string GenerateRandomCostGOLD()
    {
        return "";
    }






    public void EncounterCycleCost(string costType)
    {

        // Generate random and unique cost for each slot

        arrayChoiceButtons[0].enabled = true;
        arrayChoiceButtons[2].enabled = true;
        arrayChoiceButtons[4].enabled = true;

        arrayChoiceButtons[0].interactable = true;
        arrayChoiceButtons[0].image.fillAmount = 1;
        arrayChoiceButtons[0].image.enabled = true;

        arrayChoiceButtons[2].interactable = true;
        arrayChoiceButtons[2].image.fillAmount = 1;
        arrayChoiceButtons[2].image.enabled = true;

        arrayChoiceButtons[4].interactable = true;
        arrayChoiceButtons[4].image.fillAmount = 1;
        arrayChoiceButtons[4].image.enabled = true;


        if (costType == "STATISTICS")
        {
            // If the type of cost for the chosen strip is STATISTICS
            // Generate a random type of statistic for each cost slot

            arrayGeneratedCostOrRewardTypes[0] = GenerateRandomCostOrRewardSTATISTICS();

            do
            {
                arrayGeneratedCostOrRewardTypes[1] = GenerateRandomCostOrRewardSTATISTICS();
            }
            while (arrayGeneratedCostOrRewardTypes[1] == arrayGeneratedCostOrRewardTypes[0]);

            do
            {
                arrayGeneratedCostOrRewardTypes[2] = GenerateRandomCostOrRewardSTATISTICS();
            }
            while ((arrayGeneratedCostOrRewardTypes[2] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[2] == arrayGeneratedCostOrRewardTypes[1]));

            do
            {
                arrayGeneratedCostOrRewardTypes[3] = GenerateRandomCostOrRewardSTATISTICS();
            }
            while ((arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[1]) || (arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[2]));

            do
            {
                arrayGeneratedCostOrRewardTypes[4] = GenerateRandomCostOrRewardSTATISTICS();
            }
            while ((arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[1]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[2]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[3]));


            // Then update the GUI icons with the chosen STATISTIC types

            for (int i = 0; i < 5; i++)
            {
                switch (arrayGeneratedCostOrRewardTypes[i])
                {
                    case "ATTACK_EARTH":
                        arrayChoiceButtons[i].image.sprite = spriteCostStatisticsATTACK_EARTH;
                        break;
                    case "ATTACK_METAL":
                        arrayChoiceButtons[i].image.sprite = spriteCostStatisticsATTACK_METAL;
                        break;
                    case "ATTACK_WATER":
                        arrayChoiceButtons[i].image.sprite = spriteCostStatisticsATTACK_WATER;
                        break;
                    case "ATTACK_WOOD":
                        arrayChoiceButtons[i].image.sprite = spriteCostStatisticsATTACK_WOOD;
                        break;
                    case "ATTACK_FIRE":
                        arrayChoiceButtons[i].image.sprite = spriteCostStatisticsATTACK_FIRE;
                        break;

                    case "SHIELD_EARTH":
                        arrayChoiceButtons[i].image.sprite = spriteCostStatisticsSHIELD_EARTH;
                        break;
                    case "SHIELD_METAL":
                        arrayChoiceButtons[i].image.sprite = spriteCostStatisticsSHIELD_METAL;
                        break;
                    case "SHIELD_WATER":
                        arrayChoiceButtons[i].image.sprite = spriteCostStatisticsSHIELD_WATER;
                        break;
                    case "SHIELD_WOOD":
                        arrayChoiceButtons[i].image.sprite = spriteCostStatisticsSHIELD_WOOD;
                        break;
                    case "SHIELD_FIRE":
                        arrayChoiceButtons[i].image.sprite = spriteCostStatisticsSHIELD_FIRE;
                        break;

                    case "SHIELD_EARTH_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostStatisticsSHIELD_EARTH_REGEN;
                        break;
                    case "SHIELD_METAL_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostStatisticsSHIELD_METAL_REGEN;
                        break;
                    case "SHIELD_WATER_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostStatisticsSHIELD_WATER_REGEN;
                        break;
                    case "SHIELD_WOOD_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostStatisticsSHIELD_WOOD_REGEN;
                        break;
                    case "SHIELD_FIRE_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostStatisticsSHIELD_FIRE_REGEN;
                        break;

                    case "DX":
                        arrayChoiceButtons[i].image.sprite = spriteCostStatisticsDX;
                        break;
                    case "STR":
                        arrayChoiceButtons[i].image.sprite = spriteCostStatisticsSTR;
                        break;
                    case "SPD":
                        arrayChoiceButtons[i].image.sprite = spriteCostStatisticsSPD;
                        break;
                    case "INT":
                        arrayChoiceButtons[i].image.sprite = spriteCostStatisticsINT;
                        break;

                    case "HP_MAX":
                        arrayChoiceButtons[i].image.sprite = spriteCostStatisticsHP_MAX;
                        break;
                    case "HP_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostStatisticsHP_REGEN;
                        break;
                    case "SP_MAX":
                        arrayChoiceButtons[i].image.sprite = spriteCostStatisticsSP_MAX;
                        break;
                    case "SP_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostStatisticsSP_REGEN;
                        break;
                    case "MP_MAX":
                        arrayChoiceButtons[i].image.sprite = spriteCostStatisticsMP_MAX;
                        break;
                    case "MP_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostStatisticsMP_REGEN;
                        break;

                }
            }

        }
        else if (costType == "CREATURES")
        {
            // If the type of cost for the chosen strip is CREATURES
            // Choose a random creature from commander's army for each cost slot

            arrayGeneratedCostOrRewardTypes[0] = GenerateRandomCostCREATURES();

            do
            {
                arrayGeneratedCostOrRewardTypes[1] = GenerateRandomCostCREATURES();
            }
            while (arrayGeneratedCostOrRewardTypes[1] == arrayGeneratedCostOrRewardTypes[0]);

            do
            {
                arrayGeneratedCostOrRewardTypes[2] = GenerateRandomCostCREATURES();
            }
            while ((arrayGeneratedCostOrRewardTypes[2] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[2] == arrayGeneratedCostOrRewardTypes[1]));

            do
            {
                arrayGeneratedCostOrRewardTypes[3] = GenerateRandomCostCREATURES();
            }
            while ((arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[1]) || (arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[2]));

            do
            {
                arrayGeneratedCostOrRewardTypes[4] = GenerateRandomCostCREATURES();
            }
            while ((arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[1]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[2]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[3]));



            // Then update the GUI icons with the chosen CREATURE types

            for (int i = 0; i < 5; i++)
            {
                switch (arrayGeneratedCostOrRewardTypes[i])
                {
                    case "YOUNG_MARSH_DRAKE":
                        arrayChoiceButtons[i].image.sprite = spriteCostCreaturesYOUNG_MARSH_DRAKE;
                        break;
                    case "GRIFFIN":
                        arrayChoiceButtons[i].image.sprite = spriteCostCreaturesGRIFFIN;
                        break;
                    case "SHADE":
                        arrayChoiceButtons[i].image.sprite = spriteCostCreaturesSHADE;
                        break;
                    case "THUNDER_BRUTE":
                        arrayChoiceButtons[i].image.sprite = spriteCostCreaturesTHUNDER_BRUTE;
                        break;
                    case "FROST_WORM":
                        arrayChoiceButtons[i].image.sprite = spriteCostCreaturesFROST_WORM;
                        break;
                    case "FIRE_ELEMENTAL":
                        arrayChoiceButtons[i].image.sprite = spriteCostCreaturesFIRE_ELEMENTAL;
                        break;
                    case "LIGHTNING_DRAGON":
                        arrayChoiceButtons[i].image.sprite = spriteCostCreaturesLIGHTNING_DRAGON;
                        break;
                    case "MIST_WEAVER":
                        arrayChoiceButtons[i].image.sprite = spriteCostCreaturesMIST_WEAVER;
                        break;
                    case "NIGHTCALLER":
                        arrayChoiceButtons[i].image.sprite = spriteCostCreaturesNIGHTCALLER;
                        break;
                    case "TAMORI":
                        arrayChoiceButtons[i].image.sprite = spriteCostCreaturesTAMORI;
                        break;
                }
            }
        }
        else if (costType == "SPELLS")
        {
            // If the type of cost for the chosen strip is SPELLS
            // Choose a random SPELL from commander's collection for each cost slot

            arrayGeneratedCostOrRewardTypes[0] = GenerateRandomCostSPELLS();

            do
            {
                arrayGeneratedCostOrRewardTypes[1] = GenerateRandomCostSPELLS();
            }
            while (arrayGeneratedCostOrRewardTypes[1] == arrayGeneratedCostOrRewardTypes[0]);

            do
            {
                arrayGeneratedCostOrRewardTypes[2] = GenerateRandomCostSPELLS();
            }
            while ((arrayGeneratedCostOrRewardTypes[2] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[2] == arrayGeneratedCostOrRewardTypes[1]));

            do
            {
                arrayGeneratedCostOrRewardTypes[3] = GenerateRandomCostSPELLS();
            }
            while ((arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[1]) || (arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[2]));

            do
            {
                arrayGeneratedCostOrRewardTypes[4] = GenerateRandomCostSPELLS();
            }
            while ((arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[1]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[2]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[3]));



            // Then update the GUI icons with the chosen SPELL types

            for (int i = 0; i < 5; i++)
            {
                switch (arrayGeneratedCostOrRewardTypes[i])
                {
                    case "ARCANE_BRILLIANCE":
                        arrayChoiceButtons[i].image.sprite = spriteCostSpellsARCANE_BRILLIANCE;
                        break;
                    case "BLESS":
                        arrayChoiceButtons[i].image.sprite = spriteCostSpellsBLESS;
                        break;
                    case "BLIND":
                        arrayChoiceButtons[i].image.sprite = spriteCostSpellsBLIND;
                        break;
                    case "ENRAGE":
                        arrayChoiceButtons[i].image.sprite = spriteCostSpellsENRAGE;
                        break;
                    case "GUARDIAN":
                        arrayChoiceButtons[i].image.sprite = spriteCostSpellsGUARDIAN;
                        break;
                    case "INFERNO":
                        arrayChoiceButtons[i].image.sprite = spriteCostSpellsINFERNO;
                        break;
                    case "MIRROR_IMAGE":
                        arrayChoiceButtons[i].image.sprite = spriteCostSpellsMIRROR_IMAGE;
                        break;
                    case "RAISE_SKELETON":
                        arrayChoiceButtons[i].image.sprite = spriteCostSpellsRAISE_SKELETON;
                        break;
                    case "SLAYER":
                        arrayChoiceButtons[i].image.sprite = spriteCostSpellsSLAYER;
                        break;
                    case "SNIPE":
                        arrayChoiceButtons[i].image.sprite = spriteCostSpellsSNIPE;
                        break;
                }
            }
        }
        else if (costType == "POTIONS")
        {
            // If the type of cost for the chosen strip is POTIONS
            // Choose a random POTION from commander's collection for each cost slot

            arrayGeneratedCostOrRewardTypes[0] = GenerateRandomCostPOTIONS();

            do
            {
                arrayGeneratedCostOrRewardTypes[1] = GenerateRandomCostPOTIONS();
            }
            while (arrayGeneratedCostOrRewardTypes[1] == arrayGeneratedCostOrRewardTypes[0]);

            do
            {
                arrayGeneratedCostOrRewardTypes[2] = GenerateRandomCostPOTIONS();
            }
            while ((arrayGeneratedCostOrRewardTypes[2] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[2] == arrayGeneratedCostOrRewardTypes[1]));

            do
            {
                arrayGeneratedCostOrRewardTypes[3] = GenerateRandomCostPOTIONS();
            }
            while ((arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[1]) || (arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[2]));

            do
            {
                arrayGeneratedCostOrRewardTypes[4] = GenerateRandomCostPOTIONS();
            }
            while ((arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[1]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[2]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[3]));



            // Then update the GUI icons with the chosen POTION types

            for (int i = 0; i < 5; i++)
            {
                switch (arrayGeneratedCostOrRewardTypes[i])
                {
                    case "POISON_ATTACK_EARTH":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_ATTACK_EARTH;
                        break;
                    case "POISON_ATTACK_METAL":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_ATTACK_METAL;
                        break;
                    case "POISON_ATTACK_WATER":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_ATTACK_WATER;
                        break;
                    case "POISON_ATTACK_WOOD":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_ATTACK_WOOD;
                        break;
                    case "POISON_ATTACK_FIRE":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_ATTACK_FIRE;
                        break;

                    case "POTION_ATTACK_EARTH":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_ATTACK_EARTH;
                        break;
                    case "POTION_ATTACK_METAL":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_ATTACK_METAL;
                        break;
                    case "POTION_ATTACK_WATER":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_ATTACK_WATER;
                        break;
                    case "POTION_ATTACK_WOOD":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_ATTACK_WOOD;
                        break;
                    case "POTION_ATTACK_FIRE":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_ATTACK_FIRE;
                        break;

                    case "POISON_SHIELD_EARTH":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_SHIELD_EARTH;
                        break;
                    case "POISON_SHIELD_METAL":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_SHIELD_METAL;
                        break;
                    case "POISON_SHIELD_WATER":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_SHIELD_WATER;
                        break;
                    case "POISON_SHIELD_WOOD":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_SHIELD_WOOD;
                        break;
                    case "POISON_SHIELD_FIRE":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_SHIELD_FIRE;
                        break;

                    case "POTION_SHIELD_EARTH":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_SHIELD_EARTH;
                        break;
                    case "POTION_SHIELD_METAL":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_SHIELD_METAL;
                        break;
                    case "POTION_SHIELD_WATER":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_SHIELD_WATER;
                        break;
                    case "POTION_SHIELD_WOOD":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_SHIELD_WOOD;
                        break;
                    case "POTION_SHIELD_FIRE":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_SHIELD_FIRE;
                        break;

                    case "POISON_SHIELD_EARTH_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_SHIELD_EARTH_REGEN;
                        break;
                    case "POISON_SHIELD_METAL_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_SHIELD_METAL_REGEN;
                        break;
                    case "POISON_SHIELD_WATER_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_SHIELD_WATER_REGEN;
                        break;
                    case "POISON_SHIELD_WOOD_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_SHIELD_WOOD_REGEN;
                        break;
                    case "POISON_SHIELD_FIRE_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_SHIELD_FIRE_REGEN;
                        break;

                    case "POTION_SHIELD_EARTH_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_SHIELD_EARTH_REGEN;
                        break;
                    case "POTION_SHIELD_METAL_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_SHIELD_METAL_REGEN;
                        break;
                    case "POTION_SHIELD_WATER_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_SHIELD_WATER_REGEN;
                        break;
                    case "POTION_SHIELD_WOOD_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_SHIELD_WOOD_REGEN;
                        break;
                    case "POTION_SHIELD_FIRE_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_SHIELD_FIRE_REGEN;
                        break;

                    case "POISON_DX":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_DX;
                        break;
                    case "POISON_STR":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_STR;
                        break;
                    case "POISON_SPD":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_SPD;
                        break;
                    case "POISON_INT":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_INT;
                        break;

                    case "POTION_DX":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_DX;
                        break;
                    case "POTION_STR":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_STR;
                        break;
                    case "POTION_SPD":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_SPD;
                        break;
                    case "POTION_INT":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_INT;
                        break;

                    case "POISON_HP_CURRENT":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_HP_CURRENT;
                        break;
                    case "POISON_HP_MAX":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_HP_MAX;
                        break;
                    case "POISON_HP_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_HP_REGEN;
                        break;
                    case "POISON_SP_CURRENT":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_SP_CURRENT;
                        break;
                    case "POISON_SP_MAX":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_SP_MAX;
                        break;
                    case "POISON_SP_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_SP_REGEN;
                        break;
                    case "POISON_MP_CURRENT":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_MP_CURRENT;
                        break;
                    case "POISON_MP_MAX":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_MP_MAX;
                        break;
                    case "POISON_MP_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_MP_REGEN;
                        break;

                    case "POTION_HP_CURRENT":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_HP_CURRENT;
                        break;
                    case "POTION_HP_MAX":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_HP_MAX;
                        break;
                    case "POTION_HP_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_HP_REGEN;
                        break;
                    case "POTION_SP_CURRENT":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_SP_CURRENT;
                        break;
                    case "POTION_SP_MAX":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_SP_MAX;
                        break;
                    case "POTION_SP_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_SP_REGEN;
                        break;
                    case "POTION_MP_CURRENT":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_MP_CURRENT;
                        break;
                    case "POTION_MP_MAX":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_MP_MAX;
                        break;
                    case "POTION_MP_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_MP_REGEN;
                        break;


                }
            }
        }
        else if (costType == "SUPPORTS")
        {
            // If the type of cost for the chosen strip is SUPPORTS
            // Choose a random SUPPORT from commander's collection for each cost slot

            arrayGeneratedCostOrRewardTypes[0] = GenerateRandomCostSUPPORTS();

            do
            {
                arrayGeneratedCostOrRewardTypes[1] = GenerateRandomCostSUPPORTS();
            }
            while (arrayGeneratedCostOrRewardTypes[1] == arrayGeneratedCostOrRewardTypes[0]);

            do
            {
                arrayGeneratedCostOrRewardTypes[2] = GenerateRandomCostSUPPORTS();
            }
            while ((arrayGeneratedCostOrRewardTypes[2] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[2] == arrayGeneratedCostOrRewardTypes[1]));

            do
            {
                arrayGeneratedCostOrRewardTypes[3] = GenerateRandomCostSUPPORTS();
            }
            while ((arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[1]) || (arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[2]));

            do
            {
                arrayGeneratedCostOrRewardTypes[4] = GenerateRandomCostSUPPORTS();
            }
            while ((arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[1]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[2]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[3]));



            // Then update the GUI icons with the chosen SUPPORT types

            for (int i = 0; i < 5; i++)
            {
                switch (arrayGeneratedCostOrRewardTypes[i])
                {
                    case "ANKH":
                        arrayChoiceButtons[i].image.sprite = spriteCostSupportsANKH;
                        break;
                    case "CURSED_URN":
                        arrayChoiceButtons[i].image.sprite = spriteCostSupportsCURSED_URN;
                        break;
                    case "DRAGON_RING":
                        arrayChoiceButtons[i].image.sprite = spriteCostSupportsDRAGON_RING;
                        break;
                    case "HEAVY_BALLISTA":
                        arrayChoiceButtons[i].image.sprite = spriteCostSupportsHEAVY_BALLISTA;
                        break;
                    case "LION_SHIELD":
                        arrayChoiceButtons[i].image.sprite = spriteCostSupportsLION_SHIELD;
                        break;
                    case "MAGMA_ORB":
                        arrayChoiceButtons[i].image.sprite = spriteCostSupportsMAGMA_ORB;
                        break;
                    case "PALADIN_SWORD":
                        arrayChoiceButtons[i].image.sprite = spriteCostSupportsPALADIN_SWORD;
                        break;
                    case "RUBY_GAUNTLET":
                        arrayChoiceButtons[i].image.sprite = spriteCostSupportsRUBY_GAUNTLET;
                        break;
                    case "SOUL_AMULET":
                        arrayChoiceButtons[i].image.sprite = spriteCostSupportsSOUL_AMULET;
                        break;
                    case "WIZARD_STAFF":
                        arrayChoiceButtons[i].image.sprite = spriteCostSupportsWIZARD_STAFF;
                        break;
                }
            }
        }
        else if (costType == "AFFINITIES")
        {
            // If the type of cost for the chosen strip is AFFINITIES
            // Choose a random AFFINITY from commander's collection for each cost slot

            arrayGeneratedCostOrRewardTypes[0] = GenerateRandomCostAFFINITIES();

            do
            {
                arrayGeneratedCostOrRewardTypes[1] = GenerateRandomCostAFFINITIES();
            }
            while (arrayGeneratedCostOrRewardTypes[1] == arrayGeneratedCostOrRewardTypes[0]);

            do
            {
                arrayGeneratedCostOrRewardTypes[2] = GenerateRandomCostAFFINITIES();
            }
            while ((arrayGeneratedCostOrRewardTypes[2] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[2] == arrayGeneratedCostOrRewardTypes[1]));

            do
            {
                arrayGeneratedCostOrRewardTypes[3] = GenerateRandomCostAFFINITIES();
            }
            while ((arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[1]) || (arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[2]));

            do
            {
                arrayGeneratedCostOrRewardTypes[4] = GenerateRandomCostAFFINITIES();
            }
            while ((arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[1]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[2]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[3]));



            // Then update the GUI icons with the chosen SUPPORT types

            for (int i = 0; i < 5; i++)
            {
                switch (arrayGeneratedCostOrRewardTypes[i])
                {
                    case "AMPHIBIAN":
                        arrayChoiceButtons[i].image.sprite = spriteCostAffinitiesAMPHIBIAN;
                        break;
                    case "AQUATIC":
                        arrayChoiceButtons[i].image.sprite = spriteCostAffinitiesAQUATIC;
                        break;
                    case "AVIAN":
                        arrayChoiceButtons[i].image.sprite = spriteCostAffinitiesAVIAN;
                        break;
                    case "BEAST":
                        arrayChoiceButtons[i].image.sprite = spriteCostAffinitiesBEAST;
                        break;
                    case "BRUTE":
                        arrayChoiceButtons[i].image.sprite = spriteCostAffinitiesBRUTE;
                        break;
                    case "BUG":
                        arrayChoiceButtons[i].image.sprite = spriteCostAffinitiesBUG;
                        break;
                    case "DEMON":
                        arrayChoiceButtons[i].image.sprite = spriteCostAffinitiesDEMON;
                        break;
                    case "DRAGON":
                        arrayChoiceButtons[i].image.sprite = spriteCostAffinitiesDRAGON;
                        break;
                    case "ELEMENTAL":
                        arrayChoiceButtons[i].image.sprite = spriteCostAffinitiesELEMENTAL;
                        break;
                    case "ETHEREAL":
                        arrayChoiceButtons[i].image.sprite = spriteCostAffinitiesETHEREAL;
                        break;
                    case "HUMAN":
                        arrayChoiceButtons[i].image.sprite = spriteCostAffinitiesHUMAN;
                        break;
                    case "PHYTO":
                        arrayChoiceButtons[i].image.sprite = spriteCostAffinitiesPHYTO;
                        break;
                    case "REPTILIAN":
                        arrayChoiceButtons[i].image.sprite = spriteCostAffinitiesREPTILIAN;
                        break;
                    case "UNDEAD":
                        arrayChoiceButtons[i].image.sprite = spriteCostAffinitiesUNDEAD;
                        break;
                }
            }
        }
        else if (costType == "GOLD")
        {
            arrayChoiceButtons[0].enabled = false;
            arrayChoiceButtons[0].interactable = false;
            arrayChoiceButtons[0].image.fillAmount = 0;
            arrayChoiceButtons[0].image.enabled = false;

            arrayChoiceButtons[1].image.sprite = spriteCostGoldAMOUNT;

            arrayChoiceButtons[2].enabled = false;
            arrayChoiceButtons[2].interactable = false;
            arrayChoiceButtons[2].image.fillAmount = 0;
            arrayChoiceButtons[2].image.enabled = false;

            arrayChoiceButtons[3].image.sprite = spriteCostGoldINCOME;

            arrayChoiceButtons[4].enabled = false;
            arrayChoiceButtons[4].interactable = false;
            arrayChoiceButtons[4].image.fillAmount = 0;
            arrayChoiceButtons[4].image.enabled = false;
        }

    }

    public void EncounterCycleReward(string rewardType)
    {

        // Generate random and unique reward for each slot

        arrayChoiceButtons[0].enabled = true;
        arrayChoiceButtons[2].enabled = true;
        arrayChoiceButtons[4].enabled = true;

        arrayChoiceButtons[0].interactable = true;
        arrayChoiceButtons[0].image.fillAmount = 1;
        arrayChoiceButtons[0].image.enabled = true;

        arrayChoiceButtons[2].interactable = true;
        arrayChoiceButtons[2].image.fillAmount = 1;
        arrayChoiceButtons[2].image.enabled = true;

        arrayChoiceButtons[4].interactable = true;
        arrayChoiceButtons[4].image.fillAmount = 1;
        arrayChoiceButtons[4].image.enabled = true;


        if (rewardType == "STATISTICS")
        {
            // If the type of reward for the chosen strip is STATISTICS
            // Generate a random type of statistic for each reward slot

            arrayGeneratedCostOrRewardTypes[0] = GenerateRandomCostOrRewardSTATISTICS();

            do
            {
                arrayGeneratedCostOrRewardTypes[1] = GenerateRandomCostOrRewardSTATISTICS();
            }
            while (arrayGeneratedCostOrRewardTypes[1] == arrayGeneratedCostOrRewardTypes[0]);

            do
            {
                arrayGeneratedCostOrRewardTypes[2] = GenerateRandomCostOrRewardSTATISTICS();
            }
            while ((arrayGeneratedCostOrRewardTypes[2] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[2] == arrayGeneratedCostOrRewardTypes[1]));

            do
            {
                arrayGeneratedCostOrRewardTypes[3] = GenerateRandomCostOrRewardSTATISTICS();
            }
            while ((arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[1]) || (arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[2]));

            do
            {
                arrayGeneratedCostOrRewardTypes[4] = GenerateRandomCostOrRewardSTATISTICS();
            }
            while ((arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[1]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[2]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[3]));


            // Then update the GUI icons with the chosen STATISTIC types

            for (int i = 0; i < 5; i++)
            {
                switch (arrayGeneratedCostOrRewardTypes[i])
                {
                    case "ATTACK_EARTH":
                        arrayChoiceButtons[i].image.sprite = spriteRewardStatisticsATTACK_EARTH;
                        break;
                    case "ATTACK_METAL":
                        arrayChoiceButtons[i].image.sprite = spriteRewardStatisticsATTACK_METAL;
                        break;
                    case "ATTACK_WATER":
                        arrayChoiceButtons[i].image.sprite = spriteRewardStatisticsATTACK_WATER;
                        break;
                    case "ATTACK_WOOD":
                        arrayChoiceButtons[i].image.sprite = spriteRewardStatisticsATTACK_WOOD;
                        break;
                    case "ATTACK_FIRE":
                        arrayChoiceButtons[i].image.sprite = spriteRewardStatisticsATTACK_FIRE;
                        break;

                    case "SHIELD_EARTH":
                        arrayChoiceButtons[i].image.sprite = spriteRewardStatisticsSHIELD_EARTH;
                        break;
                    case "SHIELD_METAL":
                        arrayChoiceButtons[i].image.sprite = spriteRewardStatisticsSHIELD_METAL;
                        break;
                    case "SHIELD_WATER":
                        arrayChoiceButtons[i].image.sprite = spriteRewardStatisticsSHIELD_WATER;
                        break;
                    case "SHIELD_WOOD":
                        arrayChoiceButtons[i].image.sprite = spriteRewardStatisticsSHIELD_WOOD;
                        break;
                    case "SHIELD_FIRE":
                        arrayChoiceButtons[i].image.sprite = spriteRewardStatisticsSHIELD_FIRE;
                        break;

                    case "SHIELD_EARTH_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteRewardStatisticsSHIELD_EARTH_REGEN;
                        break;
                    case "SHIELD_METAL_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteRewardStatisticsSHIELD_METAL_REGEN;
                        break;
                    case "SHIELD_WATER_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteRewardStatisticsSHIELD_WATER_REGEN;
                        break;
                    case "SHIELD_WOOD_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteRewardStatisticsSHIELD_WOOD_REGEN;
                        break;
                    case "SHIELD_FIRE_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteRewardStatisticsSHIELD_FIRE_REGEN;
                        break;

                    case "DX":
                        arrayChoiceButtons[i].image.sprite = spriteRewardStatisticsDX;
                        break;
                    case "STR":
                        arrayChoiceButtons[i].image.sprite = spriteRewardStatisticsSTR;
                        break;
                    case "SPD":
                        arrayChoiceButtons[i].image.sprite = spriteRewardStatisticsSPD;
                        break;
                    case "INT":
                        arrayChoiceButtons[i].image.sprite = spriteRewardStatisticsINT;
                        break;

                    case "HP_MAX":
                        arrayChoiceButtons[i].image.sprite = spriteRewardStatisticsHP_MAX;
                        break;
                    case "HP_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteRewardStatisticsHP_REGEN;
                        break;
                    case "SP_MAX":
                        arrayChoiceButtons[i].image.sprite = spriteRewardStatisticsSP_MAX;
                        break;
                    case "SP_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteRewardStatisticsSP_REGEN;
                        break;
                    case "MP_MAX":
                        arrayChoiceButtons[i].image.sprite = spriteRewardStatisticsMP_MAX;
                        break;
                    case "MP_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteRewardStatisticsMP_REGEN;
                        break;

                }
            }

        }
        else if (rewardType == "CREATURES")
        {
            // If the type of reward for the chosen strip is CREATURES
            // Choose a random creature for commander's army for each reward slot

            arrayGeneratedCostOrRewardTypes[0] = GenerateRandomCostCREATURES();

            do
            {
                arrayGeneratedCostOrRewardTypes[1] = GenerateRandomCostCREATURES();
            }
            while (arrayGeneratedCostOrRewardTypes[1] == arrayGeneratedCostOrRewardTypes[0]);

            do
            {
                arrayGeneratedCostOrRewardTypes[2] = GenerateRandomCostCREATURES();
            }
            while ((arrayGeneratedCostOrRewardTypes[2] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[2] == arrayGeneratedCostOrRewardTypes[1]));

            do
            {
                arrayGeneratedCostOrRewardTypes[3] = GenerateRandomCostCREATURES();
            }
            while ((arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[1]) || (arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[2]));

            do
            {
                arrayGeneratedCostOrRewardTypes[4] = GenerateRandomCostCREATURES();
            }
            while ((arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[1]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[2]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[3]));



            // Then update the GUI icons with the chosen CREATURE types

            for (int i = 0; i < 5; i++)
            {
                switch (arrayGeneratedCostOrRewardTypes[i])
                {
                    case "YOUNG_MARSH_DRAKE":
                        arrayChoiceButtons[i].image.sprite = spriteRewardCreaturesYOUNG_MARSH_DRAKE;
                        break;
                    case "GRIFFIN":
                        arrayChoiceButtons[i].image.sprite = spriteRewardCreaturesGRIFFIN;
                        break;
                    case "SHADE":
                        arrayChoiceButtons[i].image.sprite = spriteRewardCreaturesSHADE;
                        break;
                    case "THUNDER_BRUTE":
                        arrayChoiceButtons[i].image.sprite = spriteRewardCreaturesTHUNDER_BRUTE;
                        break;
                    case "FROST_WORM":
                        arrayChoiceButtons[i].image.sprite = spriteRewardCreaturesFROST_WORM;
                        break;
                    case "FIRE_ELEMENTAL":
                        arrayChoiceButtons[i].image.sprite = spriteRewardCreaturesFIRE_ELEMENTAL;
                        break;
                    case "LIGHTNING_DRAGON":
                        arrayChoiceButtons[i].image.sprite = spriteRewardCreaturesLIGHTNING_DRAGON;
                        break;
                    case "MIST_WEAVER":
                        arrayChoiceButtons[i].image.sprite = spriteRewardCreaturesMIST_WEAVER;
                        break;
                    case "NIGHTCALLER":
                        arrayChoiceButtons[i].image.sprite = spriteRewardCreaturesNIGHTCALLER;
                        break;
                    case "TAMORI":
                        arrayChoiceButtons[i].image.sprite = spriteRewardCreaturesTAMORI;
                        break;
                }
            }
        }
        else if (rewardType == "SPELLS")
        {
            // If the type of Reward for the chosen strip is SPELLS
            // Choose a random SPELL for commander's collection for each Reward slot

            arrayGeneratedCostOrRewardTypes[0] = GenerateRandomCostSPELLS();

            do
            {
                arrayGeneratedCostOrRewardTypes[1] = GenerateRandomCostSPELLS();
            }
            while (arrayGeneratedCostOrRewardTypes[1] == arrayGeneratedCostOrRewardTypes[0]);

            do
            {
                arrayGeneratedCostOrRewardTypes[2] = GenerateRandomCostSPELLS();
            }
            while ((arrayGeneratedCostOrRewardTypes[2] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[2] == arrayGeneratedCostOrRewardTypes[1]));

            do
            {
                arrayGeneratedCostOrRewardTypes[3] = GenerateRandomCostSPELLS();
            }
            while ((arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[1]) || (arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[2]));

            do
            {
                arrayGeneratedCostOrRewardTypes[4] = GenerateRandomCostSPELLS();
            }
            while ((arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[1]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[2]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[3]));



            // Then update the GUI icons with the chosen SPELL types

            for (int i = 0; i < 5; i++)
            {
                switch (arrayGeneratedCostOrRewardTypes[i])
                {
                    case "ARCANE_BRILLIANCE":
                        arrayChoiceButtons[i].image.sprite = spriteRewardSpellsARCANE_BRILLIANCE;
                        break;
                    case "BLESS":
                        arrayChoiceButtons[i].image.sprite = spriteRewardSpellsBLESS;
                        break;
                    case "BLIND":
                        arrayChoiceButtons[i].image.sprite = spriteRewardSpellsBLIND;
                        break;
                    case "ENRAGE":
                        arrayChoiceButtons[i].image.sprite = spriteRewardSpellsENRAGE;
                        break;
                    case "GUARDIAN":
                        arrayChoiceButtons[i].image.sprite = spriteRewardSpellsGUARDIAN;
                        break;
                    case "INFERNO":
                        arrayChoiceButtons[i].image.sprite = spriteRewardSpellsINFERNO;
                        break;
                    case "MIRROR_IMAGE":
                        arrayChoiceButtons[i].image.sprite = spriteRewardSpellsMIRROR_IMAGE;
                        break;
                    case "RAISE_SKELETON":
                        arrayChoiceButtons[i].image.sprite = spriteRewardSpellsRAISE_SKELETON;
                        break;
                    case "SLAYER":
                        arrayChoiceButtons[i].image.sprite = spriteRewardSpellsSLAYER;
                        break;
                    case "SNIPE":
                        arrayChoiceButtons[i].image.sprite = spriteRewardSpellsSNIPE;
                        break;
                }
            }
        }
        else if (rewardType == "POTIONS")
        {
            // If the type of Reward for the chosen strip is POTIONS
            // Choose a random POTION for commander's collection for each Reward slot

            arrayGeneratedCostOrRewardTypes[0] = GenerateRandomCostPOTIONS();

            do
            {
                arrayGeneratedCostOrRewardTypes[1] = GenerateRandomCostPOTIONS();
            }
            while (arrayGeneratedCostOrRewardTypes[1] == arrayGeneratedCostOrRewardTypes[0]);

            do
            {
                arrayGeneratedCostOrRewardTypes[2] = GenerateRandomCostPOTIONS();
            }
            while ((arrayGeneratedCostOrRewardTypes[2] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[2] == arrayGeneratedCostOrRewardTypes[1]));

            do
            {
                arrayGeneratedCostOrRewardTypes[3] = GenerateRandomCostPOTIONS();
            }
            while ((arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[1]) || (arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[2]));

            do
            {
                arrayGeneratedCostOrRewardTypes[4] = GenerateRandomCostPOTIONS();
            }
            while ((arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[1]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[2]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[3]));



            // Then update the GUI icons with the chosen POTION types

            for (int i = 0; i < 5; i++)
            {
                switch (arrayGeneratedCostOrRewardTypes[i])
                {
                    case "POISON_ATTACK_EARTH":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_ATTACK_EARTH;
                        break;
                    case "POISON_ATTACK_METAL":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_ATTACK_METAL;
                        break;
                    case "POISON_ATTACK_WATER":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_ATTACK_WATER;
                        break;
                    case "POISON_ATTACK_WOOD":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_ATTACK_WOOD;
                        break;
                    case "POISON_ATTACK_FIRE":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_ATTACK_FIRE;
                        break;

                    case "POTION_ATTACK_EARTH":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_ATTACK_EARTH;
                        break;
                    case "POTION_ATTACK_METAL":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_ATTACK_METAL;
                        break;
                    case "POTION_ATTACK_WATER":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_ATTACK_WATER;
                        break;
                    case "POTION_ATTACK_WOOD":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_ATTACK_WOOD;
                        break;
                    case "POTION_ATTACK_FIRE":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_ATTACK_FIRE;
                        break;

                    case "POISON_SHIELD_EARTH":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_SHIELD_EARTH;
                        break;
                    case "POISON_SHIELD_METAL":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_SHIELD_METAL;
                        break;
                    case "POISON_SHIELD_WATER":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_SHIELD_WATER;
                        break;
                    case "POISON_SHIELD_WOOD":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_SHIELD_WOOD;
                        break;
                    case "POISON_SHIELD_FIRE":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_SHIELD_FIRE;
                        break;

                    case "POTION_SHIELD_EARTH":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_SHIELD_EARTH;
                        break;
                    case "POTION_SHIELD_METAL":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_SHIELD_METAL;
                        break;
                    case "POTION_SHIELD_WATER":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_SHIELD_WATER;
                        break;
                    case "POTION_SHIELD_WOOD":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_SHIELD_WOOD;
                        break;
                    case "POTION_SHIELD_FIRE":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_SHIELD_FIRE;
                        break;

                    case "POISON_SHIELD_EARTH_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_SHIELD_EARTH_REGEN;
                        break;
                    case "POISON_SHIELD_METAL_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_SHIELD_METAL_REGEN;
                        break;
                    case "POISON_SHIELD_WATER_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_SHIELD_WATER_REGEN;
                        break;
                    case "POISON_SHIELD_WOOD_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_SHIELD_WOOD_REGEN;
                        break;
                    case "POISON_SHIELD_FIRE_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_SHIELD_FIRE_REGEN;
                        break;

                    case "POTION_SHIELD_EARTH_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_SHIELD_EARTH_REGEN;
                        break;
                    case "POTION_SHIELD_METAL_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_SHIELD_METAL_REGEN;
                        break;
                    case "POTION_SHIELD_WATER_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_SHIELD_WATER_REGEN;
                        break;
                    case "POTION_SHIELD_WOOD_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_SHIELD_WOOD_REGEN;
                        break;
                    case "POTION_SHIELD_FIRE_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_SHIELD_FIRE_REGEN;
                        break;

                    case "POISON_DX":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_DX;
                        break;
                    case "POISON_STR":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_STR;
                        break;
                    case "POISON_SPD":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_SPD;
                        break;
                    case "POISON_INT":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_INT;
                        break;

                    case "POTION_DX":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_DX;
                        break;
                    case "POTION_STR":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_STR;
                        break;
                    case "POTION_SPD":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_SPD;
                        break;
                    case "POTION_INT":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_INT;
                        break;

                    case "POISON_HP_CURRENT":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_HP_CURRENT;
                        break;
                    case "POISON_HP_MAX":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_HP_MAX;
                        break;
                    case "POISON_HP_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_HP_REGEN;
                        break;
                    case "POISON_SP_CURRENT":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_SP_CURRENT;
                        break;
                    case "POISON_SP_MAX":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_SP_MAX;
                        break;
                    case "POISON_SP_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_SP_REGEN;
                        break;
                    case "POISON_MP_CURRENT":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_MP_CURRENT;
                        break;
                    case "POISON_MP_MAX":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_MP_MAX;
                        break;
                    case "POISON_MP_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOISON_MP_REGEN;
                        break;

                    case "POTION_HP_CURRENT":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_HP_CURRENT;
                        break;
                    case "POTION_HP_MAX":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_HP_MAX;
                        break;
                    case "POTION_HP_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_HP_REGEN;
                        break;
                    case "POTION_SP_CURRENT":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_SP_CURRENT;
                        break;
                    case "POTION_SP_MAX":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_SP_MAX;
                        break;
                    case "POTION_SP_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_SP_REGEN;
                        break;
                    case "POTION_MP_CURRENT":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_MP_CURRENT;
                        break;
                    case "POTION_MP_MAX":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_MP_MAX;
                        break;
                    case "POTION_MP_REGEN":
                        arrayChoiceButtons[i].image.sprite = spriteCostPotionsPOTION_MP_REGEN;
                        break;


                }
            }
        }
        else if (rewardType == "SUPPORTS")
        {
            // If the type of Reward for the chosen strip is SUPPORTS
            // Choose a random SUPPORT for commander's collection for each Reward slot

            arrayGeneratedCostOrRewardTypes[0] = GenerateRandomCostSUPPORTS();

            do
            {
                arrayGeneratedCostOrRewardTypes[1] = GenerateRandomCostSUPPORTS();
            }
            while (arrayGeneratedCostOrRewardTypes[1] == arrayGeneratedCostOrRewardTypes[0]);

            do
            {
                arrayGeneratedCostOrRewardTypes[2] = GenerateRandomCostSUPPORTS();
            }
            while ((arrayGeneratedCostOrRewardTypes[2] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[2] == arrayGeneratedCostOrRewardTypes[1]));

            do
            {
                arrayGeneratedCostOrRewardTypes[3] = GenerateRandomCostSUPPORTS();
            }
            while ((arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[1]) || (arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[2]));

            do
            {
                arrayGeneratedCostOrRewardTypes[4] = GenerateRandomCostSUPPORTS();
            }
            while ((arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[1]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[2]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[3]));



            // Then update the GUI icons with the chosen SUPPORT types

            for (int i = 0; i < 5; i++)
            {
                switch (arrayGeneratedCostOrRewardTypes[i])
                {
                    case "ANKH":
                        arrayChoiceButtons[i].image.sprite = spriteRewardSupportsANKH;
                        break;
                    case "CURSED_URN":
                        arrayChoiceButtons[i].image.sprite = spriteRewardSupportsCURSED_URN;
                        break;
                    case "DRAGON_RING":
                        arrayChoiceButtons[i].image.sprite = spriteRewardSupportsDRAGON_RING;
                        break;
                    case "HEAVY_BALLISTA":
                        arrayChoiceButtons[i].image.sprite = spriteRewardSupportsHEAVY_BALLISTA;
                        break;
                    case "LION_SHIELD":
                        arrayChoiceButtons[i].image.sprite = spriteRewardSupportsLION_SHIELD;
                        break;
                    case "MAGMA_ORB":
                        arrayChoiceButtons[i].image.sprite = spriteRewardSupportsMAGMA_ORB;
                        break;
                    case "PALADIN_SWORD":
                        arrayChoiceButtons[i].image.sprite = spriteRewardSupportsPALADIN_SWORD;
                        break;
                    case "RUBY_GAUNTLET":
                        arrayChoiceButtons[i].image.sprite = spriteRewardSupportsRUBY_GAUNTLET;
                        break;
                    case "SOUL_AMULET":
                        arrayChoiceButtons[i].image.sprite = spriteRewardSupportsSOUL_AMULET;
                        break;
                    case "WIZARD_STAFF":
                        arrayChoiceButtons[i].image.sprite = spriteRewardSupportsWIZARD_STAFF;
                        break;
                }
            }
        }
        else if (rewardType == "AFFINITIES")
        {
            // If the type of Reward for the chosen strip is AFFINITIES
            // Choose a random AFFINITY for commander's collection for each Reward slot

            arrayGeneratedCostOrRewardTypes[0] = GenerateRandomCostAFFINITIES();

            do
            {
                arrayGeneratedCostOrRewardTypes[1] = GenerateRandomCostAFFINITIES();
            }
            while (arrayGeneratedCostOrRewardTypes[1] == arrayGeneratedCostOrRewardTypes[0]);

            do
            {
                arrayGeneratedCostOrRewardTypes[2] = GenerateRandomCostAFFINITIES();
            }
            while ((arrayGeneratedCostOrRewardTypes[2] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[2] == arrayGeneratedCostOrRewardTypes[1]));

            do
            {
                arrayGeneratedCostOrRewardTypes[3] = GenerateRandomCostAFFINITIES();
            }
            while ((arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[1]) || (arrayGeneratedCostOrRewardTypes[3] == arrayGeneratedCostOrRewardTypes[2]));

            do
            {
                arrayGeneratedCostOrRewardTypes[4] = GenerateRandomCostAFFINITIES();
            }
            while ((arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[0]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[1]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[2]) || (arrayGeneratedCostOrRewardTypes[4] == arrayGeneratedCostOrRewardTypes[3]));



            // Then update the GUI icons with the chosen AFFINITY types

            for (int i = 0; i < 5; i++)
            {
                switch (arrayGeneratedCostOrRewardTypes[i])
                {
                    case "AMPHIBIAN":
                        arrayChoiceButtons[i].image.sprite = spriteRewardAffinitiesAMPHIBIAN;
                        break;
                    case "AQUATIC":
                        arrayChoiceButtons[i].image.sprite = spriteRewardAffinitiesAQUATIC;
                        break;
                    case "AVIAN":
                        arrayChoiceButtons[i].image.sprite = spriteRewardAffinitiesAVIAN;
                        break;
                    case "BEAST":
                        arrayChoiceButtons[i].image.sprite = spriteRewardAffinitiesBEAST;
                        break;
                    case "BRUTE":
                        arrayChoiceButtons[i].image.sprite = spriteRewardAffinitiesBRUTE;
                        break;
                    case "BUG":
                        arrayChoiceButtons[i].image.sprite = spriteRewardAffinitiesBUG;
                        break;
                    case "DEMON":
                        arrayChoiceButtons[i].image.sprite = spriteRewardAffinitiesDEMON;
                        break;
                    case "DRAGON":
                        arrayChoiceButtons[i].image.sprite = spriteRewardAffinitiesDRAGON;
                        break;
                    case "ELEMENTAL":
                        arrayChoiceButtons[i].image.sprite = spriteRewardAffinitiesELEMENTAL;
                        break;
                    case "ETHEREAL":
                        arrayChoiceButtons[i].image.sprite = spriteRewardAffinitiesETHEREAL;
                        break;
                    case "HUMAN":
                        arrayChoiceButtons[i].image.sprite = spriteRewardAffinitiesHUMAN;
                        break;
                    case "PHYTO":
                        arrayChoiceButtons[i].image.sprite = spriteRewardAffinitiesPHYTO;
                        break;
                    case "REPTILIAN":
                        arrayChoiceButtons[i].image.sprite = spriteRewardAffinitiesREPTILIAN;
                        break;
                    case "UNDEAD":
                        arrayChoiceButtons[i].image.sprite = spriteRewardAffinitiesUNDEAD;
                        break;
                }
            }
        }
        else if (rewardType == "GOLD")
        {
            arrayChoiceButtons[0].enabled = false;
            arrayChoiceButtons[0].interactable = false;
            arrayChoiceButtons[0].image.fillAmount = 0;
            arrayChoiceButtons[0].image.enabled = false;

            arrayChoiceButtons[1].image.sprite = spriteRewardGoldAMOUNT;

            arrayChoiceButtons[2].enabled = false;
            arrayChoiceButtons[2].interactable = false;
            arrayChoiceButtons[2].image.fillAmount = 0;
            arrayChoiceButtons[2].image.enabled = false;

            arrayChoiceButtons[3].image.sprite = spriteRewardGoldINCOME; 

            arrayChoiceButtons[4].enabled = false;
            arrayChoiceButtons[4].interactable = false;
            arrayChoiceButtons[4].image.fillAmount = 0;
            arrayChoiceButtons[4].image.enabled = false;
        }

    }
}
