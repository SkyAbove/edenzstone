﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayGLowBallElement : StateMachineBehaviour
{
    public GameObject myCanvas;             // Get the game object that has the needed script attached
    public AdventureMain myScript;          // Instantiate an object with the exact name of that .cs fIle

    public GameObject panelAdventureMain;
    private Animator animatorPanelAdventureMain;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    //override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        panelAdventureMain = GameObject.Find("panelAdventureMain");
        animatorPanelAdventureMain = panelAdventureMain.GetComponent<Animator>();
        animatorPanelAdventureMain.Play("glowballElement", -1, 0.0f);

        myCanvas = GameObject.Find("Canvas");                           // Reference the canvas
        myScript = myCanvas.GetComponent<AdventureMain>();              // Get the needed script attached to the canvas

        myScript.HideAllIconsELEMENTS();                                // Changes the orbs to empty
        myScript.HideAllIconsFACTIONS();                                // Changes the orbs to empty
        myScript.HideAllIconsCOSTS();                                   // Changes the orbs to empty
    }

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
