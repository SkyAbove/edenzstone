﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdventureMain : MonoBehaviour
{
    // WARNING: Commenting out game objects destroys the Inspector's connection to the object
    // When un-commenting the object again, you have to re-link it in the Inspector

    public GameObject tabStatistics;                            // Tabs in the panelCommander
    public GameObject tabCreatures;
    public GameObject tabSpells;
    public GameObject tabPotions;
    public GameObject tabSupports;
    public GameObject tabAffinities;
    public GameObject tabGold;

    public GameObject panelAdventureMain;

    private Animator animatorStrips;

    public Button btnIconStats;                                 // REFERENCE: The icon buttons in panelCommander
    public Button btnIconCreatures;
    public Button btnIconSpells;
    public Button btnIconPotions;
    public Button btnIconSupports;
    public Button btnIconAffinities;
    public Button btnIconGold;

    public Button btnStory;

    public Button btnAdventureFinished;

    public Sprite btnIconStatsNormal;                           // REFERENCE: The sprites needed for each tab icon button 
    public Sprite btnIconStatsHover;
    public Sprite btnIconCreaturesNormal;
    public Sprite btnIconCreaturesHover;
    public Sprite btnIconSpellsNormal;
    public Sprite btnIconSpellsHover;
    public Sprite btnIconPotionsNormal;
    public Sprite btnIconPotionsHover;
    public Sprite btnIconSupportsNormal;
    public Sprite btnIconSupportsHover;
    public Sprite btnIconAffinitiesNormal;
    public Sprite btnIconAffinitiesHover;
    public Sprite btnIconGoldNormal;
    public Sprite btnIconGoldHover;

    public Sprite spritePortraitAmphibianAMPHORA;               // REFERENCE: Commander portrait sprites that will go into Image imgAdventureMainPortrait
    public Sprite spritePortraitAmphibianCROACK;
    public Sprite spritePortraitAmphibianGULLET;
    public Sprite spritePortraitAmphibianROG;
    public Sprite spritePortraitAmphibianSALLA;

    public Sprite spritePortraitAquaticBLUB;
    public Sprite spritePortraitAquaticMERA;
    public Sprite spritePortraitAquaticOCTUS;
    public Sprite spritePortraitAquaticSHAREK;
    public Sprite spritePortraitAquaticTHULLU;

    public Sprite spritePortraitAvianBILL;
    public Sprite spritePortraitAvianGIBBS;
    public Sprite spritePortraitAvianHARPETTE;
    public Sprite spritePortraitAvianIZORA;
    public Sprite spritePortraitAvianYRAG;

    public Sprite spritePortraitBeastBLORN;
    public Sprite spritePortraitBeastGARLAND;
    public Sprite spritePortraitBeastLIONEL;
    public Sprite spritePortraitBeastMONKLEY;
    public Sprite spritePortraitBeastPIGLET;

    public Sprite spritePortraitBruteBLOODBORN;
    public Sprite spritePortraitBruteCYCLON;
    public Sprite spritePortraitBruteMOLAK;
    public Sprite spritePortraitBruteMORCA;
    public Sprite spritePortraitBruteSHEOG;

    public Sprite spritePortraitBugANETTE;
    public Sprite spritePortraitBugBUTTERCUP;
    public Sprite spritePortraitBugHIVERA;
    public Sprite spritePortraitBugMANTEL;
    public Sprite spritePortraitBugSECTON;

    public Sprite spritePortraitDemonDAMIAN;
    public Sprite spritePortraitDemonKRAM;
    public Sprite spritePortraitDemonNETH;
    public Sprite spritePortraitDemonSEARAIN;
    public Sprite spritePortraitDemonSUCCUBAH;

    public Sprite spritePortraitDragonCHEN;
    public Sprite spritePortraitDragonISIS;
    public Sprite spritePortraitDragonIXEN;
    public Sprite spritePortraitDragonLOTH;
    public Sprite spritePortraitDragonSANG;

    public Sprite spritePortraitElementalARCLAN;
    public Sprite spritePortraitElementalHYDRANA;
    public Sprite spritePortraitElementalIGNUS;
    public Sprite spritePortraitElementalMAG;
    public Sprite spritePortraitElementalWYD;

    public Sprite spritePortraitEtherealANGELUS;
    public Sprite spritePortraitEtherealNYX;
    public Sprite spritePortraitEtherealRAEL;
    public Sprite spritePortraitEtherealTHANATOS;
    public Sprite spritePortraitEtherealTHONIOS;

    public Sprite spritePortraitHumanDACAS;
    public Sprite spritePortraitHumanFINEAS;
    public Sprite spritePortraitHumanFORLA;
    public Sprite spritePortraitHumanHUNTER;
    public Sprite spritePortraitHumanTAMAZ;

    public Sprite spritePortraitPhytoCALIX;
    public Sprite spritePortraitPhytoIRONBARK;
    public Sprite spritePortraitPhytoOAKLAN;
    public Sprite spritePortraitPhytoTHORNET;
    public Sprite spritePortraitPhytoTULA;

    public Sprite spritePortraitReptilianAGAMET;
    public Sprite spritePortraitReptilianAMMIT;
    public Sprite spritePortraitReptilianMEDUSEL;
    public Sprite spritePortraitReptilianNAGA;
    public Sprite spritePortraitReptilianTHWARK;

    public Sprite spritePortraitUndeadLAMIA;
    public Sprite spritePortraitUndeadLICHEN;
    public Sprite spritePortraitUndeadQUIETUS;
    public Sprite spritePortraitUndeadSWORT;
    public Sprite spritePortraitUndeadZOMM;

    public Sprite spriteElementalOrbEARTH;
    public Sprite spriteElementalOrbMETAL;
    public Sprite spriteElementalOrbWATER;
    public Sprite spriteElementalOrbWOOD;
    public Sprite spriteElementalOrbFIRE;

    public Sprite spriteRaceIconAMPHIBIAN;                      // REFERENCE: The race icons at the bottom of the panelCommander
    public Sprite spriteRaceIconAQUATIC;
    public Sprite spriteRaceIconAVIAN;
    public Sprite spriteRaceIconBEAST;
    public Sprite spriteRaceIconBRUTE;
    public Sprite spriteRaceIconBUG;
    public Sprite spriteRaceIconDEMON;
    public Sprite spriteRaceIconDRAGON;
    public Sprite spriteRaceIconELEMENTAL;
    public Sprite spriteRaceIconETHEREAL;
    public Sprite spriteRaceIconHUMAN;
    public Sprite spriteRaceIconPHYTO;
    public Sprite spriteRaceIconREPTILIAN;
    public Sprite spriteRaceIconUNDEAD;

    public Sprite spriteCostIconSTATISTICS;
    public Sprite spriteCostIconCREATURES;
    public Sprite spriteCostIconSPELLS;
    public Sprite spriteCostIconPOTIONS;
    public Sprite spriteCostIconSUPPORTS;
    public Sprite spriteCostIconAFFINITIES;
    public Sprite spriteCostIconGOLD;

    public Sprite spriteRewardIconSTATISTICS;
    public Sprite spriteRewardIconCREATURES;
    public Sprite spriteRewardIconSPELLS;
    public Sprite spriteRewardIconPOTIONS;
    public Sprite spriteRewardIconSUPPORTS;
    public Sprite spriteRewardIconAFFINITIES;
    public Sprite spriteRewardIconGOLD;

    public Sprite spriteOrbEmpty;

    public Sprite btnStoryOn;
    public Sprite btnStoryOff;

    public Sprite spriteSuccessNormal;
    public Sprite spriteSuccessGood;
    public Sprite spriteSuccessGreat;
    public Sprite spriteSuccessMighty;
    public Sprite spriteSuccessEpic;

    


    public Text txtPlayerName;                                 // REFERENCE: All textboxes
    public Text txtTabName;

    public Text txtAdventureDMGEarth;
    public Text txtAdventureDMGMetal;
    public Text txtAdventureDMGWater;
    public Text txtAdventureDMGWood;
    public Text txtAdventureDMGFire;

    public Text txtAdventureSHIELDEarth;
    public Text txtAdventureSHIELDMetal;
    public Text txtAdventureSHIELDWater;
    public Text txtAdventureSHIELDWood;
    public Text txtAdventureSHIELDFire;

    public Text txtAdventureSHIELDRegenEarth;
    public Text txtAdventureSHIELDRegenMetal;
    public Text txtAdventureSHIELDRegenWater;
    public Text txtAdventureSHIELDRegenWood;
    public Text txtAdventureSHIELDRegenFire;

    public Text txtAdventureHPCurrent;
    public Text txtAdventureSPCurrent;
    public Text txtAdventureMPCurrent;

    public Text txtAdventureHPMax;
    public Text txtAdventureSPMax;
    public Text txtAdventureMPMax;

    public Text txtAdventureHPRegen;
    public Text txtAdventureSPRegen;
    public Text txtAdventureMPRegen;

    public Text txtAdventureDX;
    public Text txtAdventureSTR;
    public Text txtAdventureSPD;
    public Text txtAdventureINT;

    public Text txtTabAffinitiesAMPHIBIAN;
    public Text txtTabAffinitiesAQUATIC;
    public Text txtTabAffinitiesAVIAN;
    public Text txtTabAffinitiesBEAST;
    public Text txtTabAffinitiesBRUTE;
    public Text txtTabAffinitiesBUG;
    public Text txtTabAffinitiesDEMON;
    public Text txtTabAffinitiesDRAGON;
    public Text txtTabAffinitiesELEMENTAL;
    public Text txtTabAffinitiesETHEREAL;
    public Text txtTabAffinitiesHUMAN;
    public Text txtTabAffinitiesPHYTO;
    public Text txtTabAffinitiesREPTILIAN;
    public Text txtTabAffinitiesUNDEAD;

    public Text txtAdventureMainChainElemental;
    public Text txtAdventureMainChainAffinity;
    public Text txtAdventureMainChainCost;
    public Text txtAdventureMainChainTotal;

    public Text txtTabGoldTOTAL;
    public Text txtTabGoldINCOME;
    public Text txtTabGoldCREATURES;

    public Text txtStory;

    public Text txtAdventureMainTurnNumberCurrent;
    public Text txtAdventureMainTurnNumberMax;

    public Text txtChoicesHeading;

    public Image imgAdventureMainPortrait;
    public Image imgElementalOrb;
    public Image imgCostIcon;
    public Image imgRaceIcon;
    public Image imgHPbar;
    public Image imgSPbar;
    public Image imgMPbar;

    public Image imgStrip1Element;
    public Image imgStrip1Faction;
    public Image imgStrip1Cost;
    public Image imgStrip1Reward1;
    public Image imgStrip1Reward2;

    public Image imgStrip2Element;
    public Image imgStrip2Faction;
    public Image imgStrip2Cost;
    public Image imgStrip2Reward1;
    public Image imgStrip2Reward2;

    public Image imgStrip3Element;
    public Image imgStrip3Faction;
    public Image imgStrip3Cost;
    public Image imgStrip3Reward1;
    public Image imgStrip3Reward2;

    public Image imgStrip4Element;
    public Image imgStrip4Faction;
    public Image imgStrip4Cost;
    public Image imgStrip4Reward1;
    public Image imgStrip4Reward2;

    public Image imgStrip5Element;
    public Image imgStrip5Faction;
    public Image imgStrip5Cost;
    public Image imgStrip5Reward1;
    public Image imgStrip5Reward2;

    public Image imgElementFromChosenStrip;         // Used to temporarily remember the Icons from the chosen strip, before they are changed to blank "spriteOrbEmpty"
    public Image imgFactionFromChosenStrip;
    public Image imgCostFromChosenStrip;
    public Image imgReward1FromChosenStrip;
    public Image imgReward2FromChosenStrip;

    public Image imgStoryBG;

    public Image imgSuccessType;

    public InputField inputTestHPcurrent;           // For testing purposes only 
    public InputField inputTestHPmax;
    public InputField inputTestSPcurrent;
    public InputField inputTestSPmax;
    public InputField inputTestMPcurrent;
    public InputField inputTestMPmax;
    public InputField inputTestDX;
    public InputField inputTestSTR;
    public InputField inputTestSPD;
    public InputField inputTestINT;
    public InputField inputTestHPregen;
    public InputField inputTestSPregen;
    public InputField inputTestMPregen;
    public InputField inputTestEARTHregen;
    public InputField inputTestMETALregen;
    public InputField inputTestWATERregen;
    public InputField inputTestWOODregen;
    public InputField inputTestFIREregen;
    public InputField inputTestDamageEARTH;
    public InputField inputTestDamageMETAL;
    public InputField inputTestDamageWATER;
    public InputField inputTestDamageWOOD;
    public InputField inputTestDamageFIRE;
    public InputField inputTestShieldEARTH;
    public InputField inputTestShieldMETAL;
    public InputField inputTestShieldWATER;
    public InputField inputTestShieldWOOD;
    public InputField inputTestShieldFIRE;

    private string[] arrayElements = new string[] { "EARTH", "METAL", "WATER", "WOOD", "FIRE" };
    private string[] arrayFactions = new string[] { "AMPHIBIAN", "AQUATIC", "AVIAN", "BEAST", "BRUTE", "BUG", "DEMON", "DRAGON", "ELEMENTAL", "ETHEREAL", "HUMAN", "PHYTO", "REPTILIAN", "UNDEAD" };
    private string[] arrayCosts = new string[] { "STATISTICS", "CREATURES", "SPELLS", "POTIONS", "SUPPORTS", "AFFINITIES", "GOLD" };
    private string[] arrayLuckLevels = new string[] { "NORMAL", "GOOD", "GREAT", "MIGHTY", "EPIC" };
    private string[] arrayGeneratedLuckTypes = new string[5];               // Holds the collection of randomly generated Lucks for the current turn


    private string strip1Element;
    private string strip1Faction;
    private string strip1Cost;
    private string strip1Reward1;
    private string strip1Reward2;

    private string strip2Element;
    private string strip2Faction;
    private string strip2Cost;
    private string strip2Reward1;
    private string strip2Reward2;

    private string strip3Element;
    private string strip3Faction;
    private string strip3Cost;
    private string strip3Reward1;
    private string strip3Reward2;

    private string strip4Element;
    private string strip4Faction;
    private string strip4Cost;
    private string strip4Reward1;
    private string strip4Reward2;

    private string strip5Element;
    private string strip5Faction;
    private string strip5Cost;
    private string strip5Reward1;
    private string strip5Reward2;

    private string chosenStripElement;
    private string chosenStripFaction;
    private string chosenStripCost;
    private string chosenStripReward1;
    private string chosenStripReward2;

    private string storyMessage;
    private string storyElementFaction;
    private string storyElementCost;
    private string storyElementReward1;
    private string storyElementReward2;

    private string slot1CostOrReward;
    private string slot2CostOrReward;
    private string slot3CostOrReward;
    private string slot4CostOrReward;
    private string slot5CostOrReward;

    private string chosenLuckType;                  // The number of the luck box the user has selected determines which element in the arrayGeneratedLuckTypes will go into this variable

    private int StripClicked;                       // Keeps track of which strip has been clicked, so we know which pictures to load in the Chain Icons
    private bool StoryButtonActive = false;         // Used to toggle btnStory 

    private int chainLengthElementalEARTH = 0;
    private int chainLengthElementalMETAL = 0;
    private int chainLengthElementalWATER = 0;
    private int chainLengthElementalWOOD = 0;
    private int chainLengthElementalFIRE = 0;

    private int chainLengthAffinityAMPHIBIAN = 0;
    private int chainLengthAffinityAQUATIC = 0;
    private int chainLengthAffinityAVIAN = 0;
    private int chainLengthAffinityBEAST = 0;
    private int chainLengthAffinityBRUTE = 0;
    private int chainLengthAffinityBUG = 0;
    private int chainLengthAffinityDEMON = 0;
    private int chainLengthAffinityDRAGON = 0;
    private int chainLengthAffinityELEMENTAL = 0;
    private int chainLengthAffinityETHEREAL = 0;
    private int chainLengthAffinityHUMAN = 0;
    private int chainLengthAffinityPHYTO = 0;
    private int chainLengthAffinityREPTILIAN = 0;
    private int chainLengthAffinityUNDEAD = 0;

    private int chainLengthCostSTATISTICS = 0;
    private int chainLengthCostCREATURES = 0;
    private int chainLengthCostSPELLS = 0;
    private int chainLengthCostPOTIONS = 0;
    private int chainLengthCostSUPPORTS = 0;
    private int chainLengthCostAFFINITIES = 0;
    private int chainLengthCostGOLD = 0;

    private int numberOfUnchosenEarths = 0;         // Used each turn to hold the totals for all the elements that were NOT chosen in that turn
    private int numberOfUnchosenMetals = 0;
    private int numberOfUnchosenWaters = 0;
    private int numberOfUnchosenWoods = 0;
    private int numberOfUnchosenFires = 0;

    private int turnNumber = 0;
    private int maxTurns = 30;

    private bool StripSelectedForFirstTime = false;

    public GameObject myCanvas;                     // Get the game object that has the needed script attached
    public AdventureMain2 myScript;                 // Instantiate an object with the exact name of that .cs fIle


    private int encounterCyclePosition = 1;

    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
    //      INITIALISATION METHODS
    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- //

    void Start()
    {
        Debug.Log("AdventureMain Start() method now running.");         // Can't use this to setup the screen as it runs at unpredictable times 
                                                                        // Panel set up manually with RefreshPanelCommander() / initialiseCommanderStats()
        animatorStrips = panelAdventureMain.GetComponent<Animator>();   // Get the animator component for panelAdventureMain so we can run the STRIP animations

        myCanvas = GameObject.Find("Canvas");                           // Reference the canvas
        myScript = myCanvas.GetComponent<AdventureMain2>();              // Get the needed script attached to the canvas
    }

    void Update()
    {


    }

    public void RefreshPanelCommander()                             // Initially called from OnStateEnter() attached to panelAdventureMain's SlideIn animation state
    {
        ShowTabStatistics();                                            // Display the STATISTICS tab by default
        initialiseCommanderStats();                                     // Set values for all known stats (from previous screen)
        SetAffinityColors();                                             // Make sure the texts for the Affinity scores are the right colors (red for negative, grey for positive)
        SetPortrait();                                                  // Change portrait to chosen commander
        GenerateRandomStartingElement();                                // Set up initial chains
        GenerateRandomStartingFaction();
        GenerateRandomStartingCost();

        GoToNextTurn();                                                 // Starts the fIrst turn cycle

        // GenerateStrips();                                               // Populate strips (generate random icons and change strip images to these)
        // StripsSlideIn();                                                // Summon the strips

        txtStory.enabled = false;                                       // Make sure story text and bg image are invisible by default
        imgStoryBG.enabled = false;

        txtAdventureMainTurnNumberMax.text = "/ " + maxTurns.ToString();
        txtAdventureMainChainTotal.text = MainCharacter.chainLengthTotal.ToString(); 
    }

    public void initialiseCommanderStats()
    {
        txtPlayerName.text = MainCharacter.name;                                // Update GUI with all known initial values from CharacterSelect and CharacterSetup
                                                                                // All values are reset to default by resetMainCharacter() / defaultAllPhysicalFields() from NewGameReset.cs when NEW GAME is pressed on MAIN MENU
        txtAdventureHPCurrent.text = MainCharacter.HPcurrent.ToString();
        txtAdventureSPCurrent.text = MainCharacter.SPcurrent.ToString();
        txtAdventureMPCurrent.text = MainCharacter.MPcurrent.ToString();

        txtAdventureHPMax.text = MainCharacter.HPmax.ToString();
        txtAdventureSPMax.text = MainCharacter.SPmax.ToString();
        txtAdventureMPMax.text = MainCharacter.MPmax.ToString();

        txtAdventureDX.text = MainCharacter.DX.ToString();
        txtAdventureSTR.text = MainCharacter.STR.ToString();
        txtAdventureSPD.text = MainCharacter.SPD.ToString();
        txtAdventureINT.text = MainCharacter.INT.ToString();

        imgHPbar.fillAmount = MainCharacter.HPcurrentPercent;
        imgSPbar.fillAmount = MainCharacter.SPcurrentPercent;
        imgMPbar.fillAmount = MainCharacter.MPcurrentPercent;

        txtTabAffinitiesAMPHIBIAN.text = MainCharacter.affinityAMPHIBIAN.ToString();
        txtTabAffinitiesAQUATIC.text = MainCharacter.affinityAQUATIC.ToString();
        txtTabAffinitiesAVIAN.text = MainCharacter.affinityAVIAN.ToString();
        txtTabAffinitiesBEAST.text = MainCharacter.affinityBEAST.ToString();
        txtTabAffinitiesBRUTE.text = MainCharacter.affinityBRUTE.ToString();
        txtTabAffinitiesBUG.text = MainCharacter.affinityBUG.ToString();
        txtTabAffinitiesDEMON.text = MainCharacter.affinityDEMON.ToString();
        txtTabAffinitiesDRAGON.text = MainCharacter.affinityDRAGON.ToString();
        txtTabAffinitiesELEMENTAL.text = MainCharacter.affinityELEMENTAL.ToString();
        txtTabAffinitiesETHEREAL.text = MainCharacter.affinityETHEREAL.ToString();
        txtTabAffinitiesHUMAN.text = MainCharacter.affinityHUMAN.ToString();
        txtTabAffinitiesPHYTO.text = MainCharacter.affinityPHYTO.ToString();
        txtTabAffinitiesREPTILIAN.text = MainCharacter.affinityREPTILIAN.ToString();
        txtTabAffinitiesUNDEAD.text = MainCharacter.affinityUNDEAD.ToString();

    }



    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
    //      TURN CONTROL HANDLERS
    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- //


    public void EncounterCycleCost(string cost)
    {
        encounterCyclePosition = 1;
        myScript.EncounterCycleCost(cost);
    }

    public void EncounterCycleNext()
    {
        if (encounterCyclePosition == 1)
        {
            if (chosenStripReward1 == "STATISTICS")
            {
                txtChoicesHeading.text = "CHOOSE A STATISTIC";
            }
            else if (chosenStripReward1 == "CREATURES")
            {
                txtChoicesHeading.text = "CHOOSE A CREATURE";
            }
            else if (chosenStripReward1 == "SPELLS")
            {
                txtChoicesHeading.text = "CHOOSE A SPELL";
            }
            else if (chosenStripReward1 == "POTIONS")
            {
                txtChoicesHeading.text = "CHOOSE A POTION";
            }
            else if (chosenStripReward1 == "SUPPORTS")
            {
                txtChoicesHeading.text = "CHOOSE A SUPPORT";
            }
            else if (chosenStripReward1 == "AFFINITIES")
            {
                txtChoicesHeading.text = "CHOOSE A FACTION";
            }
            else if (chosenStripReward1 == "GOLD")
            {
                txtChoicesHeading.text = "GOLD OR INCOME/TURN?";
            }

            encounterCyclePosition = 2;

            myScript.EncounterCycleReward(chosenStripReward1);

            animatorStrips.enabled = true;
            animatorStrips.Rebind();
            animatorStrips.Play("groupResultBackToChoice");

            
        }
        else if (encounterCyclePosition == 2)
        {

            if (chosenStripReward2 == "STATISTICS")
            {
                txtChoicesHeading.text = "CHOOSE A STATISTIC";
            }
            else if (chosenStripReward2 == "CREATURES")
            {
                txtChoicesHeading.text = "CHOOSE A CREATURE";
            }
            else if (chosenStripReward2 == "SPELLS")
            {
                txtChoicesHeading.text = "CHOOSE A SPELL";
            }
            else if (chosenStripReward2 == "POTIONS")
            {
                txtChoicesHeading.text = "CHOOSE A POTION";
            }
            else if (chosenStripReward2 == "SUPPORTS")
            {
                txtChoicesHeading.text = "CHOOSE A SUPPORT";
            }
            else if (chosenStripReward2 == "AFFINITIES")
            {
                txtChoicesHeading.text = "CHOOSE A FACTION";
            }
            else if (chosenStripReward2 == "GOLD")
            {
                txtChoicesHeading.text = "GOLD OR INCOME/TURN?";
            }

            encounterCyclePosition = 3;

            myScript.EncounterCycleReward(chosenStripReward2);

            animatorStrips.enabled = true;
            animatorStrips.Rebind();
            animatorStrips.Play("groupResultBackToChoice"); 

            
        }
        else
        {
            GoToNextTurn();
        }
    }   
    

    public void GoToNextTurn()                                      // Runs every time you click the CONTINUE button inside groupResult
    {        

        turnNumber += 1;

        if (turnNumber == 31)
        {
            btnAdventureFinished.Select();
        }
        else
        {
            txtAdventureMainTurnNumberCurrent.text = turnNumber.ToString();

            GenerateStrips();                                               // Populate strips (generate random icons and change strip images to these)
            StripsSlideIn();
        }



    }



    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
    //      ALL HANDLERS FOR LUCK BUTTONS IN:  panelSelection / groupLUCK
    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- //

    private void RandomiseLuckBoxes()                               // Assigns to each Luck Box a unique value from one of the 5 Luck Types - NORMAL / GOOD / GREAT / MIGHTY / EPIC
    {
        arrayGeneratedLuckTypes[0] = GenerateLuckType();

        do
        {
            arrayGeneratedLuckTypes[1] = GenerateLuckType();
        }
        while (arrayGeneratedLuckTypes[1] == arrayGeneratedLuckTypes[0]);

        do
        {
            arrayGeneratedLuckTypes[2] = GenerateLuckType();
        }
        while ((arrayGeneratedLuckTypes[2] == arrayGeneratedLuckTypes[0]) || (arrayGeneratedLuckTypes[2] == arrayGeneratedLuckTypes[1]));

        do
        {
            arrayGeneratedLuckTypes[3] = GenerateLuckType();
        }
        while ((arrayGeneratedLuckTypes[3] == arrayGeneratedLuckTypes[0]) || (arrayGeneratedLuckTypes[3] == arrayGeneratedLuckTypes[1]) || (arrayGeneratedLuckTypes[3] == arrayGeneratedLuckTypes[2]));

        do
        {
            arrayGeneratedLuckTypes[4] = GenerateLuckType();
        }
        while ((arrayGeneratedLuckTypes[4] == arrayGeneratedLuckTypes[0]) || (arrayGeneratedLuckTypes[4] == arrayGeneratedLuckTypes[1]) || (arrayGeneratedLuckTypes[4] == arrayGeneratedLuckTypes[2]) || (arrayGeneratedLuckTypes[4] == arrayGeneratedLuckTypes[3]));
    }

    private string GenerateLuckType()
    {
        int randomNum = Random.Range(0, 5);                         // Choose a random Luck Type from the Luck Levels array
        chosenLuckType = arrayLuckLevels[randomNum];
        return chosenLuckType;
    }


    public void Luck1Hover()
    {
        Debug.Log("Mouse is over Luck 1.");
    }

    public void Luck2Hover()
    {
        Debug.Log("Mouse is over Luck 2.");
    }

    public void Luck3Hover()
    {
        Debug.Log("Mouse is over Luck 3.");
    }

    public void Luck4Hover()
    {
        Debug.Log("Mouse is over Luck 4.");
    }

    public void Luck5Hover()
    {
        Debug.Log("Mouse is over Luck 5.");
    }



    public void Luck1Out()
    {
        //
    }

    public void Luck2Out()
    {
        //
    }

    public void Luck3Out()
    {
        //
    }

    public void Luck4Out()
    {
        //
    }

    public void Luck5Out()
    {
        //
    }



    public void Luck1Clicked()
    {
        chosenLuckType = arrayGeneratedLuckTypes[0];
        SetLuckResultImage();

        animatorStrips.enabled = true;
        animatorStrips.Rebind();
        animatorStrips.Play("groupLuckToResult");
    }

    public void Luck2Clicked()
    {
        chosenLuckType = arrayGeneratedLuckTypes[1];
        SetLuckResultImage();

        animatorStrips.enabled = true;
        animatorStrips.Rebind();
        animatorStrips.Play("groupLuckToResult");
    }

    public void Luck3Clicked()
    {
        chosenLuckType = arrayGeneratedLuckTypes[2];
        SetLuckResultImage();

        animatorStrips.enabled = true;
        animatorStrips.Rebind();
        animatorStrips.Play("groupLuckToResult");
    }

    public void Luck4Clicked()
    {
        chosenLuckType = arrayGeneratedLuckTypes[3];
        SetLuckResultImage();

        animatorStrips.enabled = true;
        animatorStrips.Rebind();
        animatorStrips.Play("groupLuckToResult");
    }

    public void Luck5Clicked()
    {
        chosenLuckType = arrayGeneratedLuckTypes[4];
        SetLuckResultImage();

        animatorStrips.enabled = true;
        animatorStrips.Rebind();
        animatorStrips.Play("groupLuckToResult"); 
    }


    private void SetLuckResultImage()
    {
        if (chosenLuckType == "NORMAL")
        {
            imgSuccessType.sprite = spriteSuccessNormal;
        }
        else if (chosenLuckType == "GOOD")
        {
            imgSuccessType.sprite = spriteSuccessGood;
        }
        else if (chosenLuckType == "GREAT")
        {
            imgSuccessType.sprite = spriteSuccessGreat;
        }
        else if (chosenLuckType == "MIGHTY")
        {
            imgSuccessType.sprite = spriteSuccessMighty;
        }
        else if (chosenLuckType == "EPIC")
        {
            imgSuccessType.sprite = spriteSuccessEpic;
        }
    }


    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
    //      ALL HANDLERS FOR CHOICE BUTTONS IN:  panelSelection / groupChoices
    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- //

    public void Choice1Hover()
    {
        Debug.Log("Mouse is over Choice 1.");
    }

    public void Choice2Hover()
    {
        Debug.Log("Mouse is over Choice 2.");
    }

    public void Choice3Hover()
    {
        Debug.Log("Mouse is over Choice 3.");
    }

    public void Choice4Hover()
    {
        Debug.Log("Mouse is over Choice 4.");
    }

    public void Choice5Hover()
    {
        Debug.Log("Mouse is over Choice 5.");
    }



    public void Choice1Out()
    {
        //
    }

    public void Choice2Out()
    {
        //
    }

    public void Choice3Out()
    {
        //
    }

    public void Choice4Out()
    {
        //
    }

    public void Choice5Out()
    {
        //
    }



    public void Choice1Clicked()
    {
        Debug.Log("Choice 1 has been clicked.");

        animatorStrips.enabled = true;
        animatorStrips.Rebind();
        animatorStrips.Play("groupChoicesToLuck");

        RandomiseLuckBoxes();
    }

    public void Choice2Clicked()
    {
        Debug.Log("Choice 2 has been clicked.");

        animatorStrips.enabled = true;
        animatorStrips.Rebind();
        animatorStrips.Play("groupChoicesToLuck");

        RandomiseLuckBoxes();
    }

    public void Choice3Clicked()
    {
        Debug.Log("Choice 3 has been clicked.");

        animatorStrips.enabled = true;
        animatorStrips.Rebind();
        animatorStrips.Play("groupChoicesToLuck");

        RandomiseLuckBoxes();
    }

    public void Choice4Clicked()
    {
        Debug.Log("Choice 4 has been clicked.");

        animatorStrips.enabled = true;
        animatorStrips.Rebind();
        animatorStrips.Play("groupChoicesToLuck");

        RandomiseLuckBoxes();
    }

    public void Choice5Clicked()
    {
        Debug.Log("Choice 5 has been clicked.");

        animatorStrips.enabled = true;
        animatorStrips.Rebind();
        animatorStrips.Play("groupChoicesToLuck");

        RandomiseLuckBoxes();
    }


    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
    //      METHODS THAT GENERATE THE "STORIES" FOR EACH ENCOUNTER - SHOWN WHEN HOVERING OVER THE SMALL "INFO" BUTTON
    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- //

    public void ButtonStoryHover()
    {
        txtStory.enabled = true;
        imgStoryBG.enabled = true;
        btnStory.image.sprite = btnStoryOn;
    }

    public void ButtonStoryOut()
    {
        if (StoryButtonActive == false)
        {
            btnStory.image.sprite = btnStoryOff;
            txtStory.enabled = false;
            imgStoryBG.enabled = false;
        }
        else
        {
            btnStory.image.sprite = btnStoryOn;
            txtStory.enabled = true;
            imgStoryBG.enabled = true;
        }
    }

    public void ButtonStoryActive()
    {
        if (StoryButtonActive == false)
        {
            StoryButtonActive = true;
        }
        else if (StoryButtonActive == true)
        {
            StoryButtonActive = false;
        }
    }

    public void ProvideStoryElements(string faction, string cost, string reward1, string reward2)       // Called by each Strip's onClick()
    {
        if (faction == "AMPHIBIAN")
        {
            string[] amphibianActors = new string[] {   "[TOAD WARRIORS] fending off an attack. \n \n",                        // Add as many as needed. One will be randomly selected
                                                        "[SALAMANDER MONKS] engaged in a difficult exorcism. \n \n",
                                                        "[JELLYCRAB MERCENARIES] looking for their next payday. \n \n",
                                                        "[FROG PRINCES] awaiting their true loves. \n \n",
                                                        "[MARSH CREEPERS] hiding from a predator. \n \n"
                                                    };

            int randomNum = Random.Range(0, amphibianActors.Length);         // Choose a random element from the array
            storyElementFaction = amphibianActors[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story

        }
        else if (faction == "AQUATIC")
        {
            string[] aquaticActors = new string[]   {   "[MERMAID HUNTERS] preparing for the next hunt. \n \n",                         // Add as many as needed. One will be randomly selected
                                                        "[PRIESTS OF CTHULHU] out spreading the Word. \n \n",
                                                        "[SIRENS] luring sailors to their doom. \n \n",
                                                        "[SEAHORSE HATCHERY GUARDS] watching over the unborn. \n \n",
                                                        "[SEA WITCHES] collecting potion ingredients. \n \n"
                                                    };

            int randomNum = Random.Range(0, aquaticActors.Length);         // Choose a random element from the array
            storyElementFaction = aquaticActors[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story
        }
        else if (faction == "AVIAN")
        {
            string[] avianActors = new string[]     {   "[VULTURES] looking for a carcass to pick. \n \n",                       // Add as many as needed. One will be randomly selected
                                                        "[GRIFFINS] defending their sanctuary. \n \n",
                                                        "[HARPIES] preparing for war. \n \n",
                                                        "[EAGLE WINDRIDERS] feeding their young. \n \n",
                                                        "[CROW ARCHERS] engaged in target practice. \n \n"
                                                    };

            int randomNum = Random.Range(0, avianActors.Length);         // Choose a random element from the array
            storyElementFaction = avianActors[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story
        }
        else if (faction == "BEAST")
        {
            string[] beastActors = new string[]     {   "[HYENAS] stalking a wounded mud boar. \n \n",                          // Add as many as needed. One will be randomly selected
                                                        "[CENTAURS] gathering supplies for the winter. \n \n",
                                                        "[GOLDEN UNICORNS] lifting a curse from an old forest. \n \n",
                                                        "[DIRE WOLVES] teaching their cubs to hunt. \n \n",
                                                        "[MAMMOTHS] searching for fresh grazing grounds. \n \n"
                                                    };

            int randomNum = Random.Range(0, beastActors.Length);         // Choose a random element from the array
            storyElementFaction = beastActors[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story

        }
        else if (faction == "BRUTE")
        {
            string[] bruteActors = new string[]     {   "[ORC RAIDERS] sneaking into a fortress at night. \n \n",                          // Add as many as needed. One will be randomly selected
                                                        "dumb [OGRES] looking for a clue. \n \n",
                                                        "[CYCLOPES] keeping their eyes on the prize. \n \n",
                                                        "thieving [GOBLINS] carting their spoils back to their lair. \n \n",
                                                        "[TROLLS] waiting for travellers under a bridge. \n \n"
                                                    };

            int randomNum = Random.Range(0, bruteActors.Length);         // Choose a random element from the array
            storyElementFaction = bruteActors[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story
        }
        else if (faction == "BUG")
        {
            string[] bugActors = new string[]       {   "[SCORPIONS] trying to cross a river. \n \n",                          // Add as many as needed. One will be randomly selected
                                                        "[GIANT WORMS] creating a tunnel network. \n \n",
                                                        "[ROACH MERMADINS] on a secret mission for the roach king. \n \n",
                                                        "[FROST SPIDERS] cleaning out their nest. \n \n",
                                                        "[WORKER BEES] preparing for hibernation. \n \n"
                                                    };

            int randomNum = Random.Range(0, bugActors.Length);         // Choose a random element from the array
            storyElementFaction = bugActors[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story
        }
        else if (faction == "DEMON")
        {
            string[] demonActors = new string[]     {   "[FALLENE ANGELS] searching for redemption. \n \n",           // Add as many as needed. One will be randomly selected
                                                        "[IMPS] tormenting a village. \n \n",
                                                        "[CERBERI] guarding a subterranean gate. \n \n",
                                                        "[SUCCUBI] hiding in a cave until nightfall. \n \n",
                                                        "[PIT LORDS] ushering souls into the abyss. \n \n"
                                                    };

            int randomNum = Random.Range(0, demonActors.Length);         // Choose a random element from the array
            storyElementFaction = demonActors[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story
        }
        else if (faction == "DRAGON")
        {
            string[] dragonActors = new string[]    {   "sleepy [SNOW DRAGONS] gathering food for winter. \n \n",                          // Add as many as needed. One will be randomly selected
                                                        "[MAGMA DRAGONS] roasting a flock of sheep. \n \n",
                                                        "[BLACK DRAGONS] guarding their treasure hoard. \n \n",
                                                        "[SWAMP DRAGONS] foraging in a muddy bog. \n \n",
                                                        "[CRYSTAL DRAGONS] polishing their scales. \n \n"
                                                    };

            int randomNum = Random.Range(0, dragonActors.Length);         // Choose a random element from the array
            storyElementFaction = dragonActors[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story
        }
        else if (faction == "ELEMENTAL")
        {
            string[] elementalActors = new string[]    {    "[AIR ELEMENTALS] gathering stormclouds. \n \n",                    // Add as many as needed. One will be randomly selected
                                                            "[RUST BEASTS] scavenging in a junk yard. \n \n",
                                                            "[SEISMIC ELEMENTALS] rearranging the landscape. \n \n",
                                                            "[FIRE COLOSSI] feeding on a magma vent. \n \n",
                                                            "[GAS GIANTS] clearing away the morning mist. \n \n"
                                                        };

            int randomNum = Random.Range(0, elementalActors.Length);         // Choose a random element from the array
            storyElementFaction = elementalActors[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story
        }
        else if (faction == "ETHEREAL")
        {
            string[] etherealActors = new string[]    {     "trapped [EFREET] warlords. \n \n",                    // Add as many as needed. One will be randomly selected
                                                            "[DRYADS] revelling in a forest glen. \n \n",
                                                            "[PIXIES] tending a rose garden. \n \n",
                                                            "[SPIRIT GUARDIANS] watching over a soul gate. \n \n",
                                                            "[RED ANGELS] ushering in an eclipse. \n \n"
                                                        };

            int randomNum = Random.Range(0, etherealActors.Length);         // Choose a random element from the array
            storyElementFaction = etherealActors[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story
        }
        else if (faction == "HUMAN")
        {
            string[] humanActors = new string[]     {   "[PIRATES] looting a small coastal village. \n \n",                          // Add as many as needed. One will be randomly selected
                                                        "[ASSASSINS] stalking their next kill. \n \n",
                                                        "[HEALERS] tending to the wounded. \n \n",
                                                        "[ARENA CHAMPIONS] training for their next battle. \n \n",
                                                        "[ROYAL GUARDS] protecting the palace. \n \n"
                                                    };

            int randomNum = Random.Range(0, humanActors.Length);         // Choose a random element from the array
            storyElementFaction = humanActors[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story
        }
        else if (faction == "PHYTO")
        {
            string[] phytoActors = new string[]    {    "[CURSED MUSHROOMS] searching for redemption. \n \n",                          // Add as many as needed. One will be randomly selected
                                                        "[MANDRAKES] petrifying intruders with their screams. \n \n",
                                                        "[SNAP DRAGONS] devouring an unlucky wanderer. \n \n",
                                                        "[ENTS] tending to young saplings. \n \n",
                                                        "[MYCONID TRAPPERS] catching bats in a dank cave. \n \n"
                                                    };

            int randomNum = Random.Range(0, phytoActors.Length);         // Choose a random element from the array
            storyElementFaction = phytoActors[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story
        }
        else if (faction == "REPTILIAN")
        {
            string[] reptilianActors = new string[]     {   "[KILLER CROCS] hunting villagers on a river bank. \n \n",                          // Add as many as needed. One will be randomly selected
                                                            "[ICE RAPTORS] traversing a treacherous glacier. \n \n",
                                                            "[BASILISKS] keeping rare gems hidden from adventurers. \n \n",
                                                            "[HYDRAS] haunting a ruined temple. \n \n",
                                                            "hungry [ANACONDAS]. \n \n"
                                                        };

            int randomNum = Random.Range(0, reptilianActors.Length);         // Choose a random element from the array
            storyElementFaction = reptilianActors[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story
        }
        else
        {
            string[] undeadActors = new string[]    {   "[BONE DRAGONS] watching over a crypt. \n \n",                          // Add as many as needed. One will be randomly selected
                                                        "[BARROW WIGHTS] haunting a cemetery. \n \n",
                                                        "[LICHES] raising the dead. \n \n",
                                                        "[ZOMBIE PIRATES] watching over their sunken treasure. \n \n",
                                                        "[VAMPIRES] hiding from the sunlight. \n \n"
                                                    };

            int randomNum = Random.Range(0, undeadActors.Length);         // Choose a random element from the array
            storyElementFaction = undeadActors[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story
        }


        if (cost == "STATISTICS")
        {
            string[] costsSTATISTICS = new string[]     {   "If you will pay the [BLOOD PRICE], \n",                   // Add as many as needed. One will be randomly selected
                                                            "If you will [HUMBLE YOURSELF] before them, \n",
                                                            "If you will [OFFER SOMETHING OF YOURSELF] to their gods, \n",
                                                            "In exchange for [PART OF YOUR SOUL], \n"
                                                        };

            int randomNum = Random.Range(0, costsSTATISTICS.Length);      // Choose a random element from the array
            storyElementCost = costsSTATISTICS[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story
        }
        else if (cost == "CREATURES")
        {
            string[] costsCREATURES = new string[]     {    "If you will hand over one of your [CREATURES] for their personal use, \n",        // Add as many as needed. One will be randomly selected
                                                            "If you will commit one of your [CREATURES] to their cause, \n",
                                                            "If you will promise to let one of your [CREATURES] go free, \n",
                                                            "If you will sacrifice one of your [CREATURES] to their gods, \n"
                                                        };

            int randomNum = Random.Range(0, costsCREATURES.Length);      // Choose a random element from the array
            storyElementCost = costsCREATURES[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story
        }
        else if (cost == "SPELLS")
        {
            string[] costsSPELLS = new string[]         {   "If you will hand over one of your [SPELLS] for their personal use, \n",        // Add as many as needed. One will be randomly selected
                                                            "If you will teach them one of your [SPELLS], \n",
                                                            "If you will amuse them by demonstrating one of your [SPELLS] \n",
                                                            "If you will sacrifice one of your [SPELLS] to their gods, \n",
                                                            "If you will use some of your [SPELL] power to help them, \n"
                                                        };

            int randomNum = Random.Range(0, costsSPELLS.Length);      // Choose a random element from the array
            storyElementCost = costsSPELLS[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story
        }
        else if (cost == "POTIONS")
        {
            string[] costsPOTIONS = new string[]        {   "If you will hand over one of your [POTIONS] for their personal use, \n",        // Add as many as needed. One will be randomly selected
                                                            "If you will use one of your [POTIONS] to teach them the alchemical arts, \n",
                                                            "If you will sacrifice one of your [POTIONS] to their gods, \n",
                                                        };

            int randomNum = Random.Range(0, costsPOTIONS.Length);      // Choose a random element from the array
            storyElementCost = costsPOTIONS[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story
        }
        else if (cost == "SUPPORTS")
        {
            string[] costsSUPPORTS = new string[]       {   "If you will give them one of your [SUPPORTS] for their personal use, \n",        // Add as many as needed. One will be randomly selected
                                                            "If you will sacrifice one of your [SUPPORTS] to their gods, \n",
                                                            "If you will commit your [SUPPORT] to the war effort of their people, \n"
                                                        };

            int randomNum = Random.Range(0, costsSUPPORTS.Length);      // Choose a random element from the array
            storyElementCost = costsSUPPORTS[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story
        }
        else if (cost == "AFFINITIES")
        {
            string[] costsAFFINITIES = new string[]     {   "If you agree to join them against their [ENEMIES], \n",        // Add as many as needed. One will be randomly selected
                                                            "If you renounce their [ENEMIES], \n"
                                                        };

            int randomNum = Random.Range(0, costsAFFINITIES.Length);      // Choose a random element from the array
            storyElementCost = costsAFFINITIES[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story
        }
        else
        {
            string[] costsGOLD = new string[]         {     "In exchange for a modest sum of [GOLD], \n",        // Add as many as needed. One will be randomly selected
                                                            "If you will pay the price in [GOLD], \n",
                                                            "If you have enough [GOLD] to spare, \n"
                                                        };

            int randomNum = Random.Range(0, costsGOLD.Length);      // Choose a random element from the array
            storyElementCost = costsGOLD[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story
        }


        if (reward1 == "STATISTICS")
        {
            string[] rewardSTATISTICS = new string[]    {   "they will give you the secrets of their martial arts",       // Add as many as needed. One will be randomly selected
                                                            "they will give you training to improve yourself",
                                                            "they will help you to become more powerful",
                                                            "they will give you combat training"
                                                        };

            int randomNum = Random.Range(0, rewardSTATISTICS.Length);         // Choose a random element from the array
            storyElementReward1 = rewardSTATISTICS[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story
        }
        else if (reward1 == "CREATURES")
        {
            string[] rewardCREATURES = new string[]    {    "they will provide you with reinforcements",        // Add as many as needed. One will be randomly selected
                                                            "they will lend their forces to your cause",
                                                            "they will give you one of their slaves to use as you see fit",
                                                            "they will provide you with mercenaries ready for battle"
                                                        };

            int randomNum = Random.Range(0, rewardCREATURES.Length);         // Choose a random element from the array
            storyElementReward1 = rewardCREATURES[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story
        }
        else if (reward1 == "SPELLS")
        {
            string[] rewardSPELLS = new string[]        {   "they will teach you some magic",        // Add as many as needed. One will be randomly selected
                                                            "they will give you a spell scroll",
                                                            "they will summon mystical forces to assist you",
                                                            "they will allow you to study the arcane tomes of their ancestors"
                                                        };

            int randomNum = Random.Range(0, rewardSPELLS.Length);         // Choose a random element from the array
            storyElementReward1 = rewardSPELLS[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story
        }
        else if (reward1 == "POTIONS")
        {
            string[] rewardPOTIONS = new string[]       {   "you may try one of their special brews",        // Add as many as needed. One will be randomly selected
                                                            "they will brew a concoction of your choice",
                                                            "they will give you one of their unused potions or poisons"
                                                        };

            int randomNum = Random.Range(0, rewardPOTIONS.Length);         // Choose a random element from the array
            storyElementReward1 = rewardPOTIONS[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story

        }
        else if (reward1 == "SUPPORTS")
        {
            string[] rewardSUPPORTS = new string[]      {   "they will give you something to aid you in your next battle",        // Add as many as needed. One will be randomly selected
                                                            "they will provide support for your next battle",
                                                            "they will give you a secret advantage you can use in battle"
                                                        };

            int randomNum = Random.Range(0, rewardSUPPORTS.Length);         // Choose a random element from the array
            storyElementReward1 = rewardSUPPORTS[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story
        }
        else if (reward1 == "AFFINITIES")
        {
            string[] rewardAFFINITIES = new string[]    {   "they will put in a good word for you with their allies",        // Add as many as needed. One will be randomly selected
                                                            "they will speak highly of you to their friends",
                                                            "they will spread word of your accomplishments",
                                                            "they will teach you what they know about the customs of others"
                                                        };

            int randomNum = Random.Range(0, rewardAFFINITIES.Length);         // Choose a random element from the array
            storyElementReward1 = rewardAFFINITIES[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story
        }
        else
        {
            string[] rewardGOLD = new string[]          {   "they will give you such treasures as they can spare",         // Add as many as needed. One will be randomly selected
                                                            "they will share with you some of their gold",
                                                            "they will pay you directly with cold, hard cash"
                                                        };

            int randomNum = Random.Range(0, rewardGOLD.Length);         // Choose a random element from the array
            storyElementReward1 = rewardGOLD[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story
        }


        if (reward2 == "STATISTICS")
        {
            if (reward2 == reward1)
            {
                storyElementReward2 = ".";
            }
            else
            {
                string[] rewardSTATISTICS = new string[]    {   ", \n and teach you their secret martial arts.",       // Add as many as needed. One will be randomly selected
                                                                ", \n and help you to improve yourself.",
                                                                ", \n and help you to become more powerful.",
                                                                ", \n and give you combat training."
                                                            };

                int randomNum = Random.Range(0, rewardSTATISTICS.Length);         // Choose a random element from the array
                storyElementReward2 = rewardSTATISTICS[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story
            }
        }
        else if (reward2 == "CREATURES")
        {
            if (reward2 == reward1)
            {
                storyElementReward2 = ".";
            }
            else
            {
                string[] rewardCREATURES = new string[] {   ", \n and provide you with reinforcements.",        // Add as many as needed. One will be randomly selected
                                                            ", \n and lend their forces to your cause.",
                                                            ", \n and give you one of their slaves to use as you see fit.",
                                                            ", \n and provide you with mercenaries ready for battle."
                                                        };

                int randomNum = Random.Range(0, rewardCREATURES.Length);         // Choose a random element from the array
                storyElementReward2 = rewardCREATURES[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story
            }
        }
        else if (reward2 == "SPELLS")
        {
            if (reward2 == reward1)
            {
                storyElementReward2 = ".";
            }
            else
            {
                string[] rewardSPELLS = new string[]    {   ", \n and teach you some magic.",        // Add as many as needed. One will be randomly selected
                                                            ", \n and give you a spell scroll.",
                                                            ", \n and summon mystical forces to assist you.",
                                                            ", \n and allow you to study the arcane tomes of their ancestors."
                                                        };

                int randomNum = Random.Range(0, rewardSPELLS.Length);         // Choose a random element from the array
                storyElementReward2 = rewardSPELLS[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story
            }
        }
        else if (reward2 == "POTIONS")
        {
            if (reward2 == reward1)
            {
                storyElementReward2 = ".";
            }
            else
            {
                string[] rewardPOTIONS = new string[]   {   ", \n and you may try one of their special brews.",        // Add as many as needed. One will be randomly selected
                                                            ", \n and they will brew a concoction of your choice.",
                                                            ", \n and give you one of their unused potions or poisons."
                                                        };

                int randomNum = Random.Range(0, rewardPOTIONS.Length);         // Choose a random element from the array
                storyElementReward2 = rewardPOTIONS[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story
            }

        }
        else if (reward2 == "SUPPORTS")
        {
            if (reward2 == reward1)
            {
                storyElementReward2 = ".";
            }
            else
            {
                string[] rewardSUPPORTS = new string[]  {   ", \n and they'll give you something to aid you in your next battle.",        // Add as many as needed. One will be randomly selected
                                                            ", \n and they'll provide a special support for your next battle.",
                                                            ", \n and give you a secret advantage you can use in battle."
                                                        };

                int randomNum = Random.Range(0, rewardSUPPORTS.Length);         // Choose a random element from the array
                storyElementReward2 = rewardSUPPORTS[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story
            }
        }
        else if (reward2 == "AFFINITIES")
        {
            if (reward2 == reward1)
            {
                storyElementReward2 = ".";
            }
            else
            {
                string[] rewardAFFINITIES = new string[]{   ", \n and they will put in a good word for you with their allies.",        // Add as many as needed. One will be randomly selected
                                                            ", \n and they will speak highly of you to their friends.",
                                                            ", \n and they will spread word of your accomplishments.",
                                                            ", \n and they will teach you what they know about the customs of others."
                                                        };

                int randomNum = Random.Range(0, rewardAFFINITIES.Length);         // Choose a random element from the array
                storyElementReward2 = rewardAFFINITIES[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story
            }
        }
        else
        {
            if (reward2 == reward1)
            {
                storyElementReward2 = ".";
            }
            else
            {
                string[] rewardGOLD = new string[]      {   ", \n and they will give you such treasures as they can spare.",         // Add as many as needed. One will be randomly selected
                                                            ", \n and they will share with you some of their gold ",
                                                            ", \n and they will pay you directly with cold, hard cash."
                                                        };

                int randomNum = Random.Range(0, rewardGOLD.Length);         // Choose a random element from the array
                storyElementReward2 = rewardGOLD[randomNum];                // Chosen element will be used by GenerateStoryMessage to construct the fInal story
            }
        }


    }

    public void GenerateStoryMessage()
    {
        storyMessage = "You come across a group of ";
        storyMessage += storyElementFaction;
        storyMessage += storyElementCost;
        storyMessage += storyElementReward1;
        storyMessage += storyElementReward2;


        txtStory.text = storyMessage;
        Debug.Log(storyMessage);
    }


    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
    //      HIDING THE TOP 3 ORBS ON A SELECTED STRIP
    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- //

    public void HideAllIconsELEMENTS()                  // Method is redundant, in future, this can all be done inside the animation state
    {
        imgStrip1Element.sprite = spriteOrbEmpty;
        imgStrip2Element.sprite = spriteOrbEmpty;
        imgStrip3Element.sprite = spriteOrbEmpty;
        imgStrip4Element.sprite = spriteOrbEmpty;
        imgStrip5Element.sprite = spriteOrbEmpty;
    }

    public void HideAllIconsFACTIONS()
    {
        imgStrip1Faction.sprite = spriteOrbEmpty;                           // Upon changing the images from Faction Icon to empty orb, the orb is shrunk to the size of the icon
        imgStrip1Faction.SetNativeSize();                                   // Therefore need to set orb to native size
        imgStrip2Faction.sprite = spriteOrbEmpty;
        imgStrip2Faction.SetNativeSize();
        imgStrip3Faction.sprite = spriteOrbEmpty;
        imgStrip3Faction.SetNativeSize();
        imgStrip4Faction.sprite = spriteOrbEmpty;
        imgStrip4Faction.SetNativeSize();
        imgStrip5Faction.sprite = spriteOrbEmpty;
        imgStrip5Faction.SetNativeSize();
    }               // Method is redundant, in future, this can all be done inside the animation state

    public void HideAllIconsCOSTS()
    {
        imgStrip1Cost.sprite = spriteOrbEmpty;
        imgStrip1Cost.SetNativeSize();
        imgStrip2Cost.sprite = spriteOrbEmpty;
        imgStrip2Cost.SetNativeSize();
        imgStrip3Cost.sprite = spriteOrbEmpty;
        imgStrip3Cost.SetNativeSize();
        imgStrip4Cost.sprite = spriteOrbEmpty;
        imgStrip4Cost.SetNativeSize();
        imgStrip5Cost.sprite = spriteOrbEmpty;
        imgStrip5Cost.SetNativeSize();
    }                  // Method is redundant, in future, this can all be done inside the animation state



    public void UpdateChainIcons()                      // Changes the Chain icons to the icons from the selected Strip
    {                                                                           // Called from EventFunctionCaller.cs attached to object panelAdventureMain, during animation glowballElement trigger
        Debug.Log("Chain Icons transferred from strip to Commander Panel.");

        imgElementalOrb.sprite = imgElementFromChosenStrip.sprite;
        imgRaceIcon.sprite = imgFactionFromChosenStrip.sprite;
        imgCostIcon.sprite = imgCostFromChosenStrip.sprite;


    }



    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
    //      ANIMATION CALLERS
    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- //

    public void StripsSlideIn()                                             // Play animation for strips sliding in (called from btnSummonStrips, RefreshCommanderPanel(), GoToNextTurn())
    {
        animatorStrips.enabled = true;
        animatorStrips.Rebind();
        animatorStrips.Play("groupStrips_SlideIn");
    }

    public void StripsSlideOut()                                            // Play animation for strips sliding out (use at end of turn) (currently only called from btnUnsummonStrips)
    {
        animatorStrips.enabled = true;
        animatorStrips.Rebind();
        animatorStrips.Play("groupStrips_SlideOut");
    }




    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
    //      CREATION OF THE STRIPS
    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- //

    public void GenerateStrips()
    {
        StripXGenerateElements(ref strip1Element);                          // Generate random ELEMENTS for all 5 strips
        StripXGenerateElements(ref strip2Element);
        StripXGenerateElements(ref strip3Element);
        StripXGenerateElements(ref strip4Element);
        StripXGenerateElements(ref strip5Element);

        StripXSetElementImages(ref strip1Element, imgStrip1Element);        // For all strips, change the ELEMENT images to the randomly generated ELEMENTS
        StripXSetElementImages(ref strip2Element, imgStrip2Element);
        StripXSetElementImages(ref strip3Element, imgStrip3Element);
        StripXSetElementImages(ref strip4Element, imgStrip4Element);
        StripXSetElementImages(ref strip5Element, imgStrip5Element);

        StripXGenerateFactions(ref strip1Faction);                          // Generate random FACTIONS for all 5 strips
        StripXGenerateFactions(ref strip2Faction);
        StripXGenerateFactions(ref strip3Faction);
        StripXGenerateFactions(ref strip4Faction);
        StripXGenerateFactions(ref strip5Faction);

        StripXSetFactionImages(ref strip1Faction, imgStrip1Faction);        // For all strips, change the FACTION images to the randomly generated FACTIONS
        StripXSetFactionImages(ref strip2Faction, imgStrip2Faction);
        StripXSetFactionImages(ref strip3Faction, imgStrip3Faction);
        StripXSetFactionImages(ref strip4Faction, imgStrip4Faction);
        StripXSetFactionImages(ref strip5Faction, imgStrip5Faction);

        StripXGenerateCosts(ref strip1Cost);                                // Generate random COSTS for all 5 strips
        StripXGenerateCosts(ref strip2Cost);
        StripXGenerateCosts(ref strip3Cost);
        StripXGenerateCosts(ref strip4Cost);
        StripXGenerateCosts(ref strip5Cost);

        StripXSetCostImages(ref strip1Cost, imgStrip1Cost);                 // For all strips, change the COST images to the randomly generated COSTS
        StripXSetCostImages(ref strip2Cost, imgStrip2Cost);
        StripXSetCostImages(ref strip3Cost, imgStrip3Cost);
        StripXSetCostImages(ref strip4Cost, imgStrip4Cost);
        StripXSetCostImages(ref strip5Cost, imgStrip5Cost);

        StripXGenerateReward1s(ref strip1Reward1, ref strip1Cost);          // Generate random REWARD 1s for all 5 strips
        StripXGenerateReward1s(ref strip2Reward1, ref strip2Cost);
        StripXGenerateReward1s(ref strip3Reward1, ref strip3Cost);
        StripXGenerateReward1s(ref strip4Reward1, ref strip4Cost);
        StripXGenerateReward1s(ref strip5Reward1, ref strip5Cost);

        StripXSetReward1Images(ref strip1Reward1, imgStrip1Reward1);        // For all strips, change the REWARD 1 images to the randomly generated REWARD 1s
        StripXSetReward1Images(ref strip2Reward1, imgStrip2Reward1);
        StripXSetReward1Images(ref strip3Reward1, imgStrip3Reward1);
        StripXSetReward1Images(ref strip4Reward1, imgStrip4Reward1);
        StripXSetReward1Images(ref strip5Reward1, imgStrip5Reward1);

        StripXGenerateReward2s(ref strip1Reward2, ref strip1Cost);          // Generate random REWARD 2s for all 5 strips
        StripXGenerateReward2s(ref strip2Reward2, ref strip2Cost);
        StripXGenerateReward2s(ref strip3Reward2, ref strip3Cost);
        StripXGenerateReward2s(ref strip4Reward2, ref strip4Cost);
        StripXGenerateReward2s(ref strip5Reward2, ref strip5Cost);

        StripXSetReward2Images(ref strip1Reward2, imgStrip1Reward2);        // For all strips, change the REWARD 2 images to the randomly generated REWARD 2s
        StripXSetReward2Images(ref strip2Reward2, imgStrip2Reward2);
        StripXSetReward2Images(ref strip3Reward2, imgStrip3Reward2);
        StripXSetReward2Images(ref strip4Reward2, imgStrip4Reward2);
        StripXSetReward2Images(ref strip5Reward2, imgStrip5Reward2);
    }


    public void StripXGenerateElements(ref string stripXElement)
    {
        stripXElement = GenerateRandomElement();
    }

    public void StripXSetElementImages(ref string stripXElement, Image imgStripXElement)
    {
        if (stripXElement == "EARTH")
        {
            imgStripXElement.sprite = spriteElementalOrbEARTH;
        }
        else if (stripXElement == "METAL")
        {
            imgStripXElement.sprite = spriteElementalOrbMETAL;
        }
        else if (stripXElement == "WATER")
        {
            imgStripXElement.sprite = spriteElementalOrbWATER;
        }
        else if (stripXElement == "WOOD")
        {
            imgStripXElement.sprite = spriteElementalOrbWOOD;
        }
        else
        {
            imgStripXElement.sprite = spriteElementalOrbFIRE;
        }
    }


    public void StripXGenerateFactions(ref string stripXFaction)
    {
        stripXFaction = GenerateRandomFaction();
    }

    public void StripXSetFactionImages(ref string stripXFaction, Image imgStripXFaction)
    {
        if (stripXFaction == "AMPHIBIAN")                                // Change the element icon to the new element
        {
            imgStripXFaction.sprite = spriteRaceIconAMPHIBIAN;
        }
        else if (stripXFaction == "AQUATIC")
        {
            imgStripXFaction.sprite = spriteRaceIconAQUATIC;
        }
        else if (stripXFaction == "AVIAN")
        {
            imgStripXFaction.sprite = spriteRaceIconAVIAN;
        }
        else if (stripXFaction == "BEAST")
        {
            imgStripXFaction.sprite = spriteRaceIconBEAST;
        }
        else if (stripXFaction == "BRUTE")
        {
            imgStripXFaction.sprite = spriteRaceIconBRUTE;
        }
        else if (stripXFaction == "BUG")
        {
            imgStripXFaction.sprite = spriteRaceIconBUG;
        }
        else if (stripXFaction == "DEMON")
        {
            imgStripXFaction.sprite = spriteRaceIconDEMON;
        }
        else if (stripXFaction == "DRAGON")
        {
            imgStripXFaction.sprite = spriteRaceIconDRAGON;
        }
        else if (stripXFaction == "ELEMENTAL")
        {
            imgStripXFaction.sprite = spriteRaceIconELEMENTAL;
        }
        else if (stripXFaction == "ETHEREAL")
        {
            imgStripXFaction.sprite = spriteRaceIconETHEREAL;
        }
        else if (stripXFaction == "HUMAN")
        {
            imgStripXFaction.sprite = spriteRaceIconHUMAN;
        }
        else if (stripXFaction == "PHYTO")
        {
            imgStripXFaction.sprite = spriteRaceIconPHYTO;
        }
        else if (stripXFaction == "REPTILIAN")
        {
            imgStripXFaction.sprite = spriteRaceIconREPTILIAN;
        }
        else
        {
            imgStripXFaction.sprite = spriteRaceIconUNDEAD;
        }
    }


    public void StripXGenerateCosts(ref string stripXCost)
    {
        stripXCost = GenerateRandomCostOrReward();
    }

    public void StripXSetCostImages(ref string stripXCost, Image imgStripXCost)
    {
        if (stripXCost == "STATISTICS")
        {
            imgStripXCost.sprite = spriteCostIconSTATISTICS;
        }
        else if (stripXCost == "CREATURES")
        {
            imgStripXCost.sprite = spriteCostIconCREATURES;
        }
        else if (stripXCost == "SPELLS")
        {
            imgStripXCost.sprite = spriteCostIconSPELLS;
        }
        else if (stripXCost == "POTIONS")
        {
            imgStripXCost.sprite = spriteCostIconPOTIONS;
        }
        else if (stripXCost == "SUPPORTS")
        {
            imgStripXCost.sprite = spriteCostIconSUPPORTS;
        }
        else if (stripXCost == "AFFINITIES")
        {
            imgStripXCost.sprite = spriteCostIconAFFINITIES;
        }
        else
        {
            imgStripXCost.sprite = spriteCostIconGOLD;
        }
    }


    public void StripXGenerateReward1s(ref string stripXReward1, ref string stripXCost)
    {
        do
        {
            stripXReward1 = GenerateRandomCostOrReward();
        }                                                                   // If REWARD 1 same as COST, then generate a different REWARD 1
        while (stripXReward1 == stripXCost);

    }

    public void StripXSetReward1Images(ref string stripXReward1, Image imgStripXReward1)
    {
        if (stripXReward1 == "STATISTICS")
        {
            imgStripXReward1.sprite = spriteRewardIconSTATISTICS;
        }
        else if (stripXReward1 == "CREATURES")
        {
            imgStripXReward1.sprite = spriteRewardIconCREATURES;
        }
        else if (stripXReward1 == "SPELLS")
        {
            imgStripXReward1.sprite = spriteRewardIconSPELLS;
        }
        else if (stripXReward1 == "POTIONS")
        {
            imgStripXReward1.sprite = spriteRewardIconPOTIONS;
        }
        else if (stripXReward1 == "SUPPORTS")
        {
            imgStripXReward1.sprite = spriteRewardIconSUPPORTS;
        }
        else if (stripXReward1 == "AFFINITIES")
        {
            imgStripXReward1.sprite = spriteRewardIconAFFINITIES;
        }
        else
        {
            imgStripXReward1.sprite = spriteRewardIconGOLD;
        }
    }


    public void StripXGenerateReward2s(ref string stripXReward2, ref string stripXCost)
    {
        do
        {
            stripXReward2 = GenerateRandomCostOrReward();
        }                                                                   // If REWARD 1 same as COST, then generate a different REWARD 1
        while (stripXReward2 == stripXCost);
    }

    public void StripXSetReward2Images(ref string stripXReward2, Image imgStripXReward2)
    {
        if (stripXReward2 == "STATISTICS")
        {
            imgStripXReward2.sprite = spriteRewardIconSTATISTICS;
        }
        else if (stripXReward2 == "CREATURES")
        {
            imgStripXReward2.sprite = spriteRewardIconCREATURES;
        }
        else if (stripXReward2 == "SPELLS")
        {
            imgStripXReward2.sprite = spriteRewardIconSPELLS;
        }
        else if (stripXReward2 == "POTIONS")
        {
            imgStripXReward2.sprite = spriteRewardIconPOTIONS;
        }
        else if (stripXReward2 == "SUPPORTS")
        {
            imgStripXReward2.sprite = spriteRewardIconSUPPORTS;
        }
        else if (stripXReward2 == "AFFINITIES")
        {
            imgStripXReward2.sprite = spriteRewardIconAFFINITIES;
        }
        else
        {
            imgStripXReward2.sprite = spriteRewardIconGOLD;
        }
    }


    /*
    public void ShowAllIconsELEMENTS()
    {
        StripXSetElementImages(ref strip1Element, imgStrip1Element);        // For all strips, change the ELEMENT images to the randomly generated ELEMENTS
        StripXSetElementImages(ref strip2Element, imgStrip2Element);
        StripXSetElementImages(ref strip3Element, imgStrip3Element);
        StripXSetElementImages(ref strip4Element, imgStrip4Element);
        StripXSetElementImages(ref strip5Element, imgStrip5Element);
    }

    public void ShowAllIconsFACTIONS()
    {
        StripXSetFactionImages(ref strip1Faction, imgStrip1Faction);        // For all strips, change the ELEMENT images to the randomly generated ELEMENTS
        StripXSetFactionImages(ref strip2Faction, imgStrip2Faction);
        StripXSetFactionImages(ref strip3Faction, imgStrip3Faction);
        StripXSetFactionImages(ref strip4Faction, imgStrip4Faction);
        StripXSetFactionImages(ref strip5Faction, imgStrip5Faction);
    }

    public void ShowAllIconsCOSTS()
    {
        StripXSetCostImages(ref strip1Cost, imgStrip1Cost);                 // For all strips, change the COST images to the randomly generated COSTS
        StripXSetCostImages(ref strip2Cost, imgStrip2Cost);
        StripXSetCostImages(ref strip3Cost, imgStrip3Cost);
        StripXSetCostImages(ref strip4Cost, imgStrip4Cost);
        StripXSetCostImages(ref strip5Cost, imgStrip5Cost);
    }

    */


    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
    //      RANDOM GENERATORS
    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- //

    private string GenerateRandomElement()
    {
        int randomNum = Random.Range(0, 5);                         // Choose a random element from the elements array
        string chosenElement = arrayElements[randomNum];
        return chosenElement;
    }

    private string GenerateRandomFaction()
    {
        int randomNum = Random.Range(0, 14);                         // Choose a random faction from the factions array
        string chosenFaction = arrayFactions[randomNum];
        return chosenFaction;
    }

    private string GenerateRandomCostOrReward()
    {
        int randomNum = Random.Range(0, 7);                          // Choose a random cost from the costs array
        string chosenCost = arrayCosts[randomNum];
        return chosenCost;
    }


    


    public void GenerateRandomStartingElement()
    {
        string chosenElement = GenerateRandomElement();

        if (chosenElement == "EARTH")                                // Change the element icon to the new element
        {
            imgElementalOrb.sprite = spriteElementalOrbEARTH;
            chainLengthElementalEARTH += 2;
            txtAdventureMainChainElemental.text = chainLengthElementalEARTH.ToString();
        }
        else if (chosenElement == "METAL")
        {
            imgElementalOrb.sprite = spriteElementalOrbMETAL;
            chainLengthElementalMETAL += 2;
            txtAdventureMainChainElemental.text = chainLengthElementalMETAL.ToString();
        }
        else if (chosenElement == "WATER")
        {
            imgElementalOrb.sprite = spriteElementalOrbWATER;
            chainLengthElementalWATER += 2;
            txtAdventureMainChainElemental.text = chainLengthElementalWATER.ToString();
        }
        else if (chosenElement == "WOOD")
        {
            imgElementalOrb.sprite = spriteElementalOrbWOOD;
            chainLengthElementalWOOD += 2;
            txtAdventureMainChainElemental.text = chainLengthElementalWOOD.ToString();
        }
        else
        {
            imgElementalOrb.sprite = spriteElementalOrbFIRE;
            chainLengthElementalFIRE += 2;
            txtAdventureMainChainElemental.text = chainLengthElementalFIRE.ToString();
        }
    }

    public void GenerateRandomStartingFaction()
    {
        string chosenFaction = GenerateRandomFaction();

        if (chosenFaction == "AMPHIBIAN")                            // Change the faction icon to the new faction
        {
            imgRaceIcon.sprite = spriteRaceIconAMPHIBIAN;
            chainLengthAffinityAMPHIBIAN += 2;
            txtAdventureMainChainAffinity.text = chainLengthAffinityAMPHIBIAN.ToString();
        }
        else if (chosenFaction == "AQUATIC")
        {
            imgRaceIcon.sprite = spriteRaceIconAQUATIC;
            chainLengthAffinityAQUATIC += 2;
            txtAdventureMainChainAffinity.text = chainLengthAffinityAQUATIC.ToString();
        }
        else if (chosenFaction == "AVIAN")
        {
            imgRaceIcon.sprite = spriteRaceIconAVIAN;
            chainLengthAffinityAVIAN += 2;
            txtAdventureMainChainAffinity.text = chainLengthAffinityAVIAN.ToString();
        }
        else if (chosenFaction == "BEAST")
        {
            imgRaceIcon.sprite = spriteRaceIconBEAST;
            chainLengthAffinityBEAST += 2;
            txtAdventureMainChainAffinity.text = chainLengthAffinityBEAST.ToString();
        }
        else if (chosenFaction == "BRUTE")
        {
            imgRaceIcon.sprite = spriteRaceIconBRUTE;
            chainLengthAffinityBRUTE += 2;
            txtAdventureMainChainAffinity.text = chainLengthAffinityBRUTE.ToString();
        }
        else if (chosenFaction == "BUG")
        {
            imgRaceIcon.sprite = spriteRaceIconBUG;
            chainLengthAffinityBUG += 2;
            txtAdventureMainChainAffinity.text = chainLengthAffinityBUG.ToString();
        }
        else if (chosenFaction == "DEMON")
        {
            imgRaceIcon.sprite = spriteRaceIconDEMON;
            chainLengthAffinityDEMON += 2;
            txtAdventureMainChainAffinity.text = chainLengthAffinityDEMON.ToString();
        }
        else if (chosenFaction == "DRAGON")
        {
            imgRaceIcon.sprite = spriteRaceIconDRAGON;
            chainLengthAffinityDRAGON += 2;
            txtAdventureMainChainAffinity.text = chainLengthAffinityDRAGON.ToString();
        }
        else if (chosenFaction == "ELEMENTAL")
        {
            imgRaceIcon.sprite = spriteRaceIconELEMENTAL;
            chainLengthAffinityELEMENTAL += 2;
            txtAdventureMainChainAffinity.text = chainLengthAffinityELEMENTAL.ToString();
        }
        else if (chosenFaction == "ETHEREAL")
        {
            imgRaceIcon.sprite = spriteRaceIconETHEREAL;
            chainLengthAffinityETHEREAL += 2;
            txtAdventureMainChainAffinity.text = chainLengthAffinityETHEREAL.ToString();
        }
        else if (chosenFaction == "HUMAN")
        {
            imgRaceIcon.sprite = spriteRaceIconHUMAN;
            chainLengthAffinityHUMAN += 2;
            txtAdventureMainChainAffinity.text = chainLengthAffinityHUMAN.ToString();
        }
        else if (chosenFaction == "PHYTO")
        {
            imgRaceIcon.sprite = spriteRaceIconPHYTO;
            chainLengthAffinityPHYTO += 2;
            txtAdventureMainChainAffinity.text = chainLengthAffinityPHYTO.ToString();
        }
        else if (chosenFaction == "REPTILIAN")
        {
            imgRaceIcon.sprite = spriteRaceIconREPTILIAN;
            chainLengthAffinityREPTILIAN += 2;
            txtAdventureMainChainAffinity.text = chainLengthAffinityREPTILIAN.ToString();
        }
        else
        {
            imgRaceIcon.sprite = spriteRaceIconUNDEAD;
            chainLengthAffinityUNDEAD += 2;
            txtAdventureMainChainAffinity.text = chainLengthAffinityUNDEAD.ToString();
        }
    }

    public void GenerateRandomStartingCost()
    {
        string chosenCost = GenerateRandomCostOrReward();

        if (chosenCost == "STATISTICS")                              // Change the cost icon to the new cost
        {
            imgCostIcon.sprite = spriteCostIconSTATISTICS;
            chainLengthCostSTATISTICS += 2;
            txtAdventureMainChainCost.text = chainLengthCostSTATISTICS.ToString();
        }
        else if (chosenCost == "CREATURES")
        {
            imgCostIcon.sprite = spriteCostIconCREATURES;
            chainLengthCostCREATURES += 2;
            txtAdventureMainChainCost.text = chainLengthCostCREATURES.ToString();
        }
        else if (chosenCost == "SPELLS")
        {
            imgCostIcon.sprite = spriteCostIconSPELLS;
            chainLengthCostSPELLS += 2;
            txtAdventureMainChainCost.text = chainLengthCostSPELLS.ToString();
        }
        else if (chosenCost == "POTIONS")
        {
            imgCostIcon.sprite = spriteCostIconPOTIONS;
            chainLengthCostPOTIONS += 2;
            txtAdventureMainChainCost.text = chainLengthCostPOTIONS.ToString();
        }
        else if (chosenCost == "SUPPORTS")
        {
            imgCostIcon.sprite = spriteCostIconSUPPORTS;
            chainLengthCostSUPPORTS += 2;
            txtAdventureMainChainCost.text = chainLengthCostSUPPORTS.ToString();
        }
        else if (chosenCost == "AFFINITIES")
        {
            imgCostIcon.sprite = spriteCostIconAFFINITIES;
            chainLengthCostAFFINITIES += 2;
            txtAdventureMainChainCost.text = chainLengthCostAFFINITIES.ToString();
        }
        else
        {
            imgCostIcon.sprite = spriteCostIconGOLD;
            chainLengthCostGOLD += 2;
            txtAdventureMainChainCost.text = chainLengthCostGOLD.ToString();
        }
    }


    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
    //      SETTER METHODS FOR STATS
    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- //

    public void UpdateHP(int HPc, int HPm)
    {

        MainCharacter.HPcurrent += HPc;                                         // Update CURRENT HP value

        if (MainCharacter.HPcurrent < 0)                                         // If CURRENT HP falls below 0, return to 0 (can never be less than 0)
        {
            MainCharacter.HPcurrent = 0;
        }

        MainCharacter.HPmax += HPm;                                             // Update MAX HP value

        if (MainCharacter.HPmax < 1)                                            // If MAX HP falls below 1, return to 1 (can never be less than 1)
        {
            MainCharacter.HPmax = 1;
        }

        if (MainCharacter.HPcurrent > MainCharacter.HPmax)                      // Current HP can never exceed max HP
        {
            MainCharacter.HPcurrent = MainCharacter.HPmax;
        }

        txtAdventureHPCurrent.text = MainCharacter.HPcurrent.ToString();        // Update GUI
        txtAdventureHPMax.text = MainCharacter.HPmax.ToString();

        MainCharacter.HPcurrentPercent = (float)MainCharacter.HPcurrent / (float)MainCharacter.HPmax;       // Update HP %
        imgHPbar.fillAmount = MainCharacter.HPcurrentPercent;                                              // Adjust HP bar
    }

    public void UpdateSP(int SPc, int SPm)
    {
        MainCharacter.SPcurrent += SPc;                                          // Update CURRENT SP value

        if (MainCharacter.SPcurrent < 0)                                         // If CURRENT SP falls below 0, return to 0 (can never be less than 0)
        {
            MainCharacter.SPcurrent = 0;
        }

        MainCharacter.SPmax += SPm;                                             // Update MAX SP value

        if (MainCharacter.SPmax < 1)                                            // If MAX SP falls below 1, return to 1 (can never be less than 1)
        {
            MainCharacter.SPmax = 1;
        }

        if (MainCharacter.SPcurrent > MainCharacter.SPmax)                      // Current SP can never exceed max SP
        {
            MainCharacter.SPcurrent = MainCharacter.SPmax;
        }

        txtAdventureSPCurrent.text = MainCharacter.SPcurrent.ToString();        // Update GUI
        txtAdventureSPMax.text = MainCharacter.SPmax.ToString();

        MainCharacter.SPcurrentPercent = (float)MainCharacter.SPcurrent / (float)MainCharacter.SPmax;       // Update SP %
        imgSPbar.fillAmount = MainCharacter.SPcurrentPercent;                                              // Adjust SP bar
    }

    public void UpdateMP(int MPc, int MPm)
    {
        MainCharacter.MPcurrent += MPc;                                          // Update CURRENT MP value

        if (MainCharacter.MPcurrent < 0)                                         // If CURRENT MP falls below 0, return to 0 (can never be less than 0)
        {
            MainCharacter.MPcurrent = 0;
        }

        MainCharacter.MPmax += MPm;                                             // Update MAX MP value 

        if (MainCharacter.MPmax < 1)                                            // If MAX MP falls below 1, return to 1 (can never be less than 1)
        {
            MainCharacter.MPmax = 1;
        }

        if (MainCharacter.MPcurrent > MainCharacter.MPmax)                      // Current MP can never exceed max MP
        {
            MainCharacter.MPcurrent = MainCharacter.MPmax;
        }

        txtAdventureMPCurrent.text = MainCharacter.MPcurrent.ToString();        // Update GUI
        txtAdventureMPMax.text = MainCharacter.MPmax.ToString();

        MainCharacter.MPcurrentPercent = (float)MainCharacter.MPcurrent / (float)MainCharacter.MPmax;       // Update MP %
        imgMPbar.fillAmount = MainCharacter.MPcurrentPercent;                                              // Adjust MP bar
    }


    public void UpdateDX(int dx)
    {
        MainCharacter.DX += dx;
        txtAdventureDX.text = MainCharacter.DX.ToString();
    }

    public void UpdateSTR(int str)
    {
        MainCharacter.STR += str;
        txtAdventureSTR.text = MainCharacter.STR.ToString();
    }

    public void UpdateSPD(int spd)
    {
        MainCharacter.SPD += spd;
        txtAdventureSPD.text = MainCharacter.SPD.ToString();
    }

    public void UpdateINT(int intel)
    {
        MainCharacter.INT += intel;
        txtAdventureINT.text = MainCharacter.INT.ToString();
    }


    public void UpdateHPregen(int regen)
    {
        MainCharacter.HPregen += regen;

        if (MainCharacter.HPregen > 0)
        {
            txtAdventureHPRegen.text = "+" + MainCharacter.HPregen.ToString();
        }
        else
        {
            txtAdventureHPRegen.text = MainCharacter.HPregen.ToString();
        }
    }

    public void UpdateSPregen(int regen)
    {
        MainCharacter.SPregen += regen;

        if (MainCharacter.SPregen > 0)
        {
            txtAdventureSPRegen.text = "+" + MainCharacter.SPregen.ToString();
        }
        else
        {
            txtAdventureSPRegen.text = MainCharacter.SPregen.ToString();
        }
    }

    public void UpdateMPregen(int regen)
    {
        MainCharacter.MPregen += regen;

        if (MainCharacter.MPregen > 0)
        {
            txtAdventureMPRegen.text = "+" + MainCharacter.MPregen.ToString();
        }
        else
        {
            txtAdventureMPRegen.text = MainCharacter.MPregen.ToString();
        }
    }


    public void UpdateDamageEARTH(int amount)
    {
        MainCharacter.damageEARTH += amount;

        if (MainCharacter.damageEARTH < 0)           // If EARTH DMG falls below 0, reset it to 0 (can never be less than 0)
        {
            MainCharacter.damageEARTH = 0;
        }

        txtAdventureDMGEarth.text = MainCharacter.damageEARTH.ToString();
    }

    public void UpdateDamageMETAL(int amount)
    {
        MainCharacter.damageMETAL += amount;

        if (MainCharacter.damageMETAL < 0)           // If EARTH DMG falls below 0, reset it to 0 (can never be less than 0)
        {
            MainCharacter.damageMETAL = 0;
        }

        txtAdventureDMGMetal.text = MainCharacter.damageMETAL.ToString();
    }

    public void UpdateDamageWATER(int amount)
    {
        MainCharacter.damageWATER += amount;

        if (MainCharacter.damageWATER < 0)           // If EARTH DMG falls below 0, reset it to 0 (can never be less than 0)
        {
            MainCharacter.damageWATER = 0;
        }

        txtAdventureDMGWater.text = MainCharacter.damageWATER.ToString();
    }

    public void UpdateDamageWOOD(int amount)
    {
        MainCharacter.damageWOOD += amount;

        if (MainCharacter.damageWOOD < 0)           // If EARTH DMG falls below 0, reset it to 0 (can never be less than 0)
        {
            MainCharacter.damageWOOD = 0;
        }

        txtAdventureDMGWood.text = MainCharacter.damageWOOD.ToString();
    }

    public void UpdateDamageFIRE(int amount)
    {
        MainCharacter.damageFIRE += amount;

        if (MainCharacter.damageFIRE < 0)           // If EARTH DMG falls below 0, reset it to 0 (can never be less than 0)
        {
            MainCharacter.damageFIRE = 0;
        }

        txtAdventureDMGFire.text = MainCharacter.damageFIRE.ToString();
    }


    public void UpdateShieldEARTH(int amount)
    {
        MainCharacter.shieldEARTH += amount;

        if (MainCharacter.shieldEARTH < 0)           // If EARTH DMG falls below 0, reset it to 0 (can never be less than 0)
        {
            MainCharacter.shieldEARTH = 0;
        }

        txtAdventureSHIELDEarth.text = MainCharacter.shieldEARTH.ToString();
    }

    public void UpdateShieldMETAL(int amount)
    {
        MainCharacter.shieldMETAL += amount;

        if (MainCharacter.shieldMETAL < 0)           // If EARTH DMG falls below 0, reset it to 0 (can never be less than 0)
        {
            MainCharacter.shieldMETAL = 0;
        }

        txtAdventureSHIELDMetal.text = MainCharacter.shieldMETAL.ToString();
    }

    public void UpdateShieldWATER(int amount)
    {
        MainCharacter.shieldWATER += amount;

        if (MainCharacter.shieldWATER < 0)           // If EARTH DMG falls below 0, reset it to 0 (can never be less than 0)
        {
            MainCharacter.shieldWATER = 0;
        }

        txtAdventureSHIELDWater.text = MainCharacter.shieldWATER.ToString();
    }

    public void UpdateShieldWOOD(int amount)
    {
        MainCharacter.shieldWOOD += amount;

        if (MainCharacter.shieldWOOD < 0)           // If EARTH DMG falls below 0, reset it to 0 (can never be less than 0)
        {
            MainCharacter.shieldWOOD = 0;
        }

        txtAdventureSHIELDWood.text = MainCharacter.shieldWOOD.ToString();
    }

    public void UpdateShieldFIRE(int amount)
    {
        MainCharacter.shieldFIRE += amount;

        if (MainCharacter.shieldFIRE < 0)           // If EARTH DMG falls below 0, reset it to 0 (can never be less than 0)
        {
            MainCharacter.shieldFIRE = 0;
        }

        txtAdventureSHIELDFire.text = MainCharacter.shieldFIRE.ToString();
    }


    public void UpdateShieldEARTHRegen(int regen)
    {
        MainCharacter.shieldEARTHregen += regen;

        if (MainCharacter.shieldEARTHregen > 0)                                                     // If regen is bigger than 0, it needs a "+" appended to the front
        {
            txtAdventureSHIELDRegenEarth.text = "+" + MainCharacter.shieldEARTHregen.ToString();
        }
        else
        {
            txtAdventureSHIELDRegenEarth.text = MainCharacter.shieldEARTHregen.ToString();
        }
    }

    public void UpdateShieldMETALRegen(int regen)
    {
        MainCharacter.shieldMETALregen += regen;

        if (MainCharacter.shieldMETALregen > 0)                                                     // If regen is bigger than 0, it needs a "+" appended to the front
        {
            txtAdventureSHIELDRegenMetal.text = "+" + MainCharacter.shieldMETALregen.ToString();
        }
        else
        {
            txtAdventureSHIELDRegenMetal.text = MainCharacter.shieldMETALregen.ToString();
        }
    }

    public void UpdateShieldWATERRegen(int regen)
    {
        MainCharacter.shieldWATERregen += regen;

        if (MainCharacter.shieldWATERregen > 0)                                                     // If regen is bigger than 0, it needs a "+" appended to the front
        {
            txtAdventureSHIELDRegenWater.text = "+" + MainCharacter.shieldWATERregen.ToString();
        }
        else
        {
            txtAdventureSHIELDRegenWater.text = MainCharacter.shieldWATERregen.ToString();
        }
    }

    public void UpdateShieldWOODRegen(int regen)
    {
        MainCharacter.shieldWOODregen += regen;

        if (MainCharacter.shieldWOODregen > 0)                                                      // If regen is bigger than 0, it needs a "+" appended to the front
        {
            txtAdventureSHIELDRegenWood.text = "+" + MainCharacter.shieldWOODregen.ToString();
        }
        else
        {
            txtAdventureSHIELDRegenWood.text = MainCharacter.shieldWOODregen.ToString();
        }
    }

    public void UpdateShieldFIRERegen(int regen)
    {
        MainCharacter.shieldFIREregen += regen;

        if (MainCharacter.shieldFIREregen > 0)                                                      // If regen is bigger than 0, it needs a "+" appended to the front
        {
            txtAdventureSHIELDRegenFire.text = "+" + MainCharacter.shieldFIREregen.ToString();
        }
        else
        {
            txtAdventureSHIELDRegenFire.text = MainCharacter.shieldFIREregen.ToString();
        }
    }


    public void UpdateAffinityAMPHIBIAN(int amount)
    {
        MainCharacter.affinityAMPHIBIAN += amount;
        txtTabAffinitiesAMPHIBIAN.text = MainCharacter.affinityAMPHIBIAN.ToString();
        SetAffinityColors();
    }

    public void UpdateAffinityAQUATIC(int amount)
    {
        MainCharacter.affinityAQUATIC += amount;
        txtTabAffinitiesAQUATIC.text = MainCharacter.affinityAQUATIC.ToString();
        SetAffinityColors();
    }

    public void UpdateAffinityAVIAN(int amount)
    {
        MainCharacter.affinityAVIAN += amount;
        txtTabAffinitiesAVIAN.text = MainCharacter.affinityAVIAN.ToString();
        SetAffinityColors();
    }

    public void UpdateAffinityBEAST(int amount)
    {
        MainCharacter.affinityBEAST += amount;
        txtTabAffinitiesBEAST.text = MainCharacter.affinityBEAST.ToString();
        SetAffinityColors();
    }

    public void UpdateAffinityBRUTE(int amount)
    {
        MainCharacter.affinityBRUTE += amount;
        txtTabAffinitiesBRUTE.text = MainCharacter.affinityBRUTE.ToString();
        SetAffinityColors();
    }

    public void UpdateAffinityBUG(int amount)
    {
        MainCharacter.affinityBUG += amount;
        txtTabAffinitiesBUG.text = MainCharacter.affinityBUG.ToString();
        SetAffinityColors();
    }

    public void UpdateAffinityDEMON(int amount)
    {
        MainCharacter.affinityDEMON += amount;
        txtTabAffinitiesDEMON.text = MainCharacter.affinityDEMON.ToString();
        SetAffinityColors();
    }

    public void UpdateAffinityDRAGON(int amount)
    {
        MainCharacter.affinityDRAGON += amount;
        txtTabAffinitiesDRAGON.text = MainCharacter.affinityDRAGON.ToString();
        SetAffinityColors();
    }

    public void UpdateAffinityELEMENTAL(int amount)
    {
        MainCharacter.affinityELEMENTAL += amount;
        txtTabAffinitiesELEMENTAL.text = MainCharacter.affinityELEMENTAL.ToString();
        SetAffinityColors();
    }

    public void UpdateAffinityETHEREAL(int amount)
    {
        MainCharacter.affinityETHEREAL += amount;
        txtTabAffinitiesETHEREAL.text = MainCharacter.affinityETHEREAL.ToString();
        SetAffinityColors();
    }

    public void UpdateAffinityHUMAN(int amount)
    {
        MainCharacter.affinityHUMAN += amount;
        txtTabAffinitiesHUMAN.text = MainCharacter.affinityHUMAN.ToString();
        SetAffinityColors();
    }

    public void UpdateAffinityPHYTO(int amount)
    {
        MainCharacter.affinityPHYTO += amount;
        txtTabAffinitiesPHYTO.text = MainCharacter.affinityPHYTO.ToString();
        SetAffinityColors();
    }

    public void UpdateAffinityREPTILIAN(int amount)
    {
        MainCharacter.affinityREPTILIAN += amount;
        txtTabAffinitiesREPTILIAN.text = MainCharacter.affinityREPTILIAN.ToString();
        SetAffinityColors();
    }

    public void UpdateAffinityUNDEAD(int amount)
    {
        MainCharacter.affinityUNDEAD += amount;
        txtTabAffinitiesUNDEAD.text = MainCharacter.affinityUNDEAD.ToString();
        SetAffinityColors();
    }

    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
    //      STRIP CLICK HANDLERS
    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- //

    public void Strip1Clicked()                                             // Everything that must happen when Strip 1 is clicked                    
    {
        StripSelectedForFirstTime = true;                                   // Prevents TOTAL CHAINS from resetting to 0 on the first round before any strip has been clicked

        StripClicked = 1;                                                   // Remember which Strip was clicked

        imgElementFromChosenStrip.sprite = imgStrip1Element.sprite;         // Remember what the Strip's current Icons are, before they are changed to spriteOrbEmpty
        imgFactionFromChosenStrip.sprite = imgStrip1Faction.sprite;
        imgCostFromChosenStrip.sprite = imgStrip1Cost.sprite;
        imgReward1FromChosenStrip.sprite = imgStrip1Reward1.sprite;
        imgReward2FromChosenStrip.sprite = imgStrip1Reward2.sprite;

        animatorStrips.enabled = true;
        animatorStrips.Rebind();
        animatorStrips.Play("strip1_selected");

        // Has OnStateExit() that runs the following methods from this script:
        // HideAllIconsELEMENTS();
        // HideAllIconsFACTIONS();
        // HideAllIconsCOSTS();

        // And plays animation "glowballElement"
        // Contains Animation Event that plays method UpdateChainIcons()
        // Then has onStateExit() that fades the chain labels back in with new values, and runs these methods from this script:
        // UpdateChainElemental();
        // UpdateChainFaction();
        // UpdateChainCost();


        ProvideStoryElements(strip1Faction, strip1Cost, strip1Reward1, strip1Reward2);
        GenerateStoryMessage();

        //myScript.EncounterCycleCost(strip1Cost, strip1Reward1, strip1Reward2);

        // GoNextEncounterCycle(strip1Cost, strip1Reward1, strip1Reward2);

        EncounterCycleCost(strip1Cost);
    }

    public void Strip2Clicked()
    {
        StripSelectedForFirstTime = true;                                   // Prevents TOTAL CHAINS from resetting to 0 on the first round before any strip has been clicked

        StripClicked = 2;                                               // Remember which Strip was clicked

        imgElementFromChosenStrip.sprite = imgStrip2Element.sprite;     // Remember what the Strip's current Icons are, before they are changed to spriteOrbEmpty
        imgFactionFromChosenStrip.sprite = imgStrip2Faction.sprite;
        imgCostFromChosenStrip.sprite = imgStrip2Cost.sprite;
        imgReward1FromChosenStrip.sprite = imgStrip2Reward1.sprite;
        imgReward2FromChosenStrip.sprite = imgStrip2Reward2.sprite;

        animatorStrips.enabled = true;
        animatorStrips.Rebind();
        animatorStrips.Play("strip2_selected");

        // Has OnStateExit() that runs the following methods from this script:
        // HideAllIconsELEMENTS();
        // HideAllIconsFACTIONS();
        // HideAllIconsCOSTS();

        // And plays animation "glowballElement"
        // Contains Animation Event that plays method UpdateChainIcons()
        // Then has onStateExit() that fades the chain labels back in with new values, and runs these methods from this script:
        // UpdateChainElemental();
        // UpdateChainFaction();
        // UpdateChainCost();

        ProvideStoryElements(strip2Faction, strip2Cost, strip2Reward1, strip2Reward2);
        GenerateStoryMessage();

        // myScript.EncounterCycle(strip2Cost, strip2Reward1, strip2Reward2);

        //GoNextEncounterCycle(strip2Cost, strip2Reward1, strip2Reward2);

        EncounterCycleCost(strip2Cost);
    }

    public void Strip3Clicked()
    {
        StripSelectedForFirstTime = true;                                   // Prevents TOTAL CHAINS from resetting to 0 on the first round before any strip has been clicked

        StripClicked = 3;                                               // Remember which Strip was clicked

        imgElementFromChosenStrip.sprite = imgStrip3Element.sprite;     // Remember what the Strip's current Icons are, before they are changed to spriteOrbEmpty
        imgFactionFromChosenStrip.sprite = imgStrip3Faction.sprite;
        imgCostFromChosenStrip.sprite = imgStrip3Cost.sprite;
        imgReward1FromChosenStrip.sprite = imgStrip3Reward1.sprite;
        imgReward2FromChosenStrip.sprite = imgStrip3Reward2.sprite;

        animatorStrips.enabled = true;
        animatorStrips.Rebind();
        animatorStrips.Play("strip3_selected");

        // Has OnStateExit() that runs the following methods from this script:
        // HideAllIconsELEMENTS();
        // HideAllIconsFACTIONS();
        // HideAllIconsCOSTS();

        // And plays animation "glowballElement"
        // Contains Animation Event that plays method UpdateChainIcons()
        // Then has onStateExit() that fades the chain labels back in with new values, and runs these methods from this script:
        // UpdateChainElemental();
        // UpdateChainFaction();
        // UpdateChainCost();

        ProvideStoryElements(strip3Faction, strip3Cost, strip3Reward1, strip3Reward2);
        GenerateStoryMessage();

        // myScript.EncounterCycle(strip3Cost, strip3Reward1, strip3Reward2);

        //GoNextEncounterCycle(strip3Cost, strip3Reward1, strip3Reward2);

        EncounterCycleCost(strip3Cost);
    }

    public void Strip4Clicked()
    {
        StripSelectedForFirstTime = true;                                   // Prevents TOTAL CHAINS from resetting to 0 on the first round before any strip has been clicked

        StripClicked = 4;                                               // Remember which Strip was clicked

        imgElementFromChosenStrip.sprite = imgStrip4Element.sprite;     // Remember what the Strip's current Icons are, before they are changed to spriteOrbEmpty
        imgFactionFromChosenStrip.sprite = imgStrip4Faction.sprite;
        imgCostFromChosenStrip.sprite = imgStrip4Cost.sprite;
        imgReward1FromChosenStrip.sprite = imgStrip4Reward1.sprite;
        imgReward2FromChosenStrip.sprite = imgStrip4Reward2.sprite;

        animatorStrips.enabled = true;
        animatorStrips.Rebind();
        animatorStrips.Play("strip4_selected");

        // Has OnStateExit() that runs the following methods from this script:
        // HideAllIconsELEMENTS();
        // HideAllIconsFACTIONS();
        // HideAllIconsCOSTS();

        // And plays animation "glowballElement"
        // Contains Animation Event that plays method UpdateChainIcons()
        // Then has onStateExit() that fades the chain labels back in with new values, and runs these methods from this script:
        // UpdateChainElemental();
        // UpdateChainFaction();
        // UpdateChainCost();

        ProvideStoryElements(strip4Faction, strip4Cost, strip4Reward1, strip4Reward2);
        GenerateStoryMessage();

        // myScript.EncounterCycle(strip4Cost, strip4Reward1, strip4Reward2);

        // GoNextEncounterCycle(strip4Cost, strip4Reward1, strip4Reward2);

        EncounterCycleCost(strip4Cost);
    }

    public void Strip5Clicked()
    {
        StripSelectedForFirstTime = true;                                   // Prevents TOTAL CHAINS from resetting to 0 on the first round before any strip has been clicked

        StripClicked = 5;                                               // Remember which Strip was clicked

        imgElementFromChosenStrip.sprite = imgStrip5Element.sprite;     // Remember what the Strip's current Icons are, before they are changed to spriteOrbEmpty
        imgFactionFromChosenStrip.sprite = imgStrip5Faction.sprite;
        imgCostFromChosenStrip.sprite = imgStrip5Cost.sprite;
        imgReward1FromChosenStrip.sprite = imgStrip5Reward1.sprite;
        imgReward2FromChosenStrip.sprite = imgStrip5Reward2.sprite;

        animatorStrips.enabled = true;
        animatorStrips.Rebind();
        animatorStrips.Play("strip5_selected");

        // Has OnStateExit() that runs the following methods from this script:
        // HideAllIconsELEMENTS();
        // HideAllIconsFACTIONS();
        // HideAllIconsCOSTS();

        // And plays animation "glowballElement"
        // Contains Animation Event that plays method UpdateChainIcons()
        // Then has onStateExit() that fades the chain labels back in with new values, and runs these methods from this script:
        // UpdateChainElemental();
        // UpdateChainFaction();
        // UpdateChainCost();

        ProvideStoryElements(strip5Faction, strip5Cost, strip5Reward1, strip5Reward2);
        GenerateStoryMessage();

        // myScript.EncounterCycleCost(strip5Cost, strip5Reward1, strip5Reward2);

        // GoNextEncounterCycle(strip5Cost, strip5Reward1, strip5Reward2);

        EncounterCycleCost(strip5Cost);
    }


    

    public void GetSelectedStripItems(int stripNumber)              // When a STRIP is clicked (it sends its own number as an argument) - Attach this to every Strip
    {                                                               // Get all ELEMENTS / FACTION / COST / REWARD 1 / REWARD 2 of all strips

        numberOfUnchosenEarths = 0;                                 // Reset the number of unchosen elements for this round
        numberOfUnchosenMetals = 0;
        numberOfUnchosenWaters = 0;
        numberOfUnchosenWoods = 0;
        numberOfUnchosenFires = 0;

        switch (stripNumber)                                        // These values are already saved into strip1Element etc. when the strips are generated
        {                                                           // Now we just need to know which ones are CURRENTLY SELECTED

            // REMEMBER WHAT ALL THE ITEMS ARE FROM THE CHOSEN STRIP
            case 1:

                chosenStripElement = strip1Element;                 // strip1Element has already been generated by GenerateStrips(), now we are recording if this was the chosen Element             
                chosenStripFaction = strip1Faction;                 // strip1Faction has already been generated by GenerateStrips(), now we record if this was the chosen Faction
                chosenStripCost = strip1Cost;                       // strip1Cost    has already been generated by GenerateStrips(), now we record if this was the chosen Cost
                chosenStripReward1 = strip1Reward1;                 // strip1Reward1 has already been generated by GenerateStrips(), now we record if this was the chosen Reward1
                chosenStripReward2 = strip1Reward2;                 // strip1Reward2 has already been generated by GenerateStrips(), now we record if this was the chosen Reward2    

                Debug.Log("strip1Element = " + strip1Element);
                Debug.Log("strip1Faction = " + strip1Faction);
                Debug.Log("strip1Cost = " + strip1Cost);
                Debug.Log("strip1Reward1 = " + strip1Reward1);
                Debug.Log("strip1Reward2 = " + strip1Reward2);

                // ALSO COUNT HOW MANY NON-SELECTED ELEMENTS ARE DIFFERENT FROM THE SELECTED ELEMENT

                if (strip2Element != strip1Element)                 // Check the element of all other strips
                {                                                   // If same as this element, ignore
                    switch (strip2Element)                          // If not same
                    {
                        case "EARTH":                               // Check what that element is
                            numberOfUnchosenEarths += 1;            // Add 1 to unchosenELement for that element
                            break;
                        case "METAL":
                            numberOfUnchosenMetals += 1;
                            break;
                        case "WATER":
                            numberOfUnchosenWaters += 1;
                            break;
                        case "WOOD":
                            numberOfUnchosenWoods += 1;
                            break;
                        case "FIRE":
                            numberOfUnchosenFires += 1;
                            break;
                    }
                }

                if (strip3Element != strip1Element)
                {
                    switch (strip3Element)
                    {
                        case "EARTH":
                            numberOfUnchosenEarths += 1;
                            break;
                        case "METAL":
                            numberOfUnchosenMetals += 1;
                            break;
                        case "WATER":
                            numberOfUnchosenWaters += 1;
                            break;
                        case "WOOD":
                            numberOfUnchosenWoods += 1;
                            break;
                        case "FIRE":
                            numberOfUnchosenFires += 1;
                            break;
                    }
                }

                if (strip4Element != strip1Element)
                {
                    switch (strip4Element)
                    {
                        case "EARTH":
                            numberOfUnchosenEarths += 1;
                            break;
                        case "METAL":
                            numberOfUnchosenMetals += 1;
                            break;
                        case "WATER":
                            numberOfUnchosenWaters += 1;
                            break;
                        case "WOOD":
                            numberOfUnchosenWoods += 1;
                            break;
                        case "FIRE":
                            numberOfUnchosenFires += 1;
                            break;
                    }
                }

                if (strip5Element != strip1Element)
                {
                    switch (strip5Element)
                    {
                        case "EARTH":
                            numberOfUnchosenEarths += 1;
                            break;
                        case "METAL":
                            numberOfUnchosenMetals += 1;
                            break;
                        case "WATER":
                            numberOfUnchosenWaters += 1;
                            break;
                        case "WOOD":
                            numberOfUnchosenWoods += 1;
                            break;
                        case "FIRE":
                            numberOfUnchosenFires += 1;
                            break;
                    }
                }

                break;

            case 2:

                chosenStripElement = strip2Element;
                chosenStripFaction = strip2Faction;
                chosenStripCost = strip2Cost;
                chosenStripReward1 = strip2Reward1;
                chosenStripReward2 = strip2Reward2;

                // ALSO COUNT HOW MANY NON-SELECTED ELEMENTS ARE DIFFERENT FROM THE SELECTED ELEMENT
                if (strip1Element != strip2Element)                  // Check the element of all other strips
                {                                                   // If same as this element, ignore
                    switch (strip1Element)                          // If not same
                    {
                        case "EARTH":                               // Check what that element is
                            numberOfUnchosenEarths += 1;            // Add 1 to unchosenELement for that element
                            break;
                        case "METAL":
                            numberOfUnchosenMetals += 1;
                            break;
                        case "WATER":
                            numberOfUnchosenWaters += 1;
                            break;
                        case "WOOD":
                            numberOfUnchosenWoods += 1;
                            break;
                        case "FIRE":
                            numberOfUnchosenFires += 1;
                            break;
                    }
                }

                if (strip3Element != strip2Element)
                {
                    switch (strip3Element)
                    {
                        case "EARTH":
                            numberOfUnchosenEarths += 1;
                            break;
                        case "METAL":
                            numberOfUnchosenMetals += 1;
                            break;
                        case "WATER":
                            numberOfUnchosenWaters += 1;
                            break;
                        case "WOOD":
                            numberOfUnchosenWoods += 1;
                            break;
                        case "FIRE":
                            numberOfUnchosenFires += 1;
                            break;
                    }
                }

                if (strip4Element != strip2Element)
                {
                    switch (strip4Element)
                    {
                        case "EARTH":
                            numberOfUnchosenEarths += 1;
                            break;
                        case "METAL":
                            numberOfUnchosenMetals += 1;
                            break;
                        case "WATER":
                            numberOfUnchosenWaters += 1;
                            break;
                        case "WOOD":
                            numberOfUnchosenWoods += 1;
                            break;
                        case "FIRE":
                            numberOfUnchosenFires += 1;
                            break;
                    }
                }

                if (strip5Element != strip2Element)
                {
                    switch (strip5Element)
                    {
                        case "EARTH":
                            numberOfUnchosenEarths += 1;
                            break;
                        case "METAL":
                            numberOfUnchosenMetals += 1;
                            break;
                        case "WATER":
                            numberOfUnchosenWaters += 1;
                            break;
                        case "WOOD":
                            numberOfUnchosenWoods += 1;
                            break;
                        case "FIRE":
                            numberOfUnchosenFires += 1;
                            break;
                    }
                }

                break;

            case 3:

                chosenStripElement = strip3Element;
                chosenStripFaction = strip3Faction;
                chosenStripCost = strip3Cost;
                chosenStripReward1 = strip3Reward1;
                chosenStripReward2 = strip3Reward2;

                // ALSO COUNT HOW MANY NON-SELECTED ELEMENTS ARE DIFFERENT FROM THE SELECTED ELEMENT
                if (strip1Element != strip3Element)                  // Check the element of all other strips
                {                                                   // If same as this element, ignore
                    switch (strip1Element)                          // If not same
                    {
                        case "EARTH":                               // Check what that element is
                            numberOfUnchosenEarths += 1;            // Add 1 to unchosenELement for that element
                            break;
                        case "METAL":
                            numberOfUnchosenMetals += 1;
                            break;
                        case "WATER":
                            numberOfUnchosenWaters += 1;
                            break;
                        case "WOOD":
                            numberOfUnchosenWoods += 1;
                            break;
                        case "FIRE":
                            numberOfUnchosenFires += 1;
                            break;
                    }
                }

                if (strip2Element != strip3Element)
                {
                    switch (strip2Element)
                    {
                        case "EARTH":
                            numberOfUnchosenEarths += 1;
                            break;
                        case "METAL":
                            numberOfUnchosenMetals += 1;
                            break;
                        case "WATER":
                            numberOfUnchosenWaters += 1;
                            break;
                        case "WOOD":
                            numberOfUnchosenWoods += 1;
                            break;
                        case "FIRE":
                            numberOfUnchosenFires += 1;
                            break;
                    }
                }

                if (strip4Element != strip3Element)
                {
                    switch (strip4Element)
                    {
                        case "EARTH":
                            numberOfUnchosenEarths += 1;
                            break;
                        case "METAL":
                            numberOfUnchosenMetals += 1;
                            break;
                        case "WATER":
                            numberOfUnchosenWaters += 1;
                            break;
                        case "WOOD":
                            numberOfUnchosenWoods += 1;
                            break;
                        case "FIRE":
                            numberOfUnchosenFires += 1;
                            break;
                    }
                }

                if (strip5Element != strip3Element)
                {
                    switch (strip5Element)
                    {
                        case "EARTH":
                            numberOfUnchosenEarths += 1;
                            break;
                        case "METAL":
                            numberOfUnchosenMetals += 1;
                            break;
                        case "WATER":
                            numberOfUnchosenWaters += 1;
                            break;
                        case "WOOD":
                            numberOfUnchosenWoods += 1;
                            break;
                        case "FIRE":
                            numberOfUnchosenFires += 1;
                            break;
                    }
                }

                break;

            case 4:

                chosenStripElement = strip4Element;
                chosenStripFaction = strip4Faction;
                chosenStripCost = strip4Cost;
                chosenStripReward1 = strip4Reward1;
                chosenStripReward2 = strip4Reward2;

                // ALSO COUNT HOW MANY NON-SELECTED ELEMENTS ARE DIFFERENT FROM THE SELECTED ELEMENT
                if (strip1Element != strip4Element)                  // Check the element of all other strips
                {                                                   // If same as this element, ignore
                    switch (strip1Element)                          // If not same
                    {
                        case "EARTH":                               // Check what that element is
                            numberOfUnchosenEarths += 1;            // Add 1 to unchosenELement for that element
                            break;
                        case "METAL":
                            numberOfUnchosenMetals += 1;
                            break;
                        case "WATER":
                            numberOfUnchosenWaters += 1;
                            break;
                        case "WOOD":
                            numberOfUnchosenWoods += 1;
                            break;
                        case "FIRE":
                            numberOfUnchosenFires += 1;
                            break;
                    }
                }

                if (strip2Element != strip4Element)
                {
                    switch (strip2Element)
                    {
                        case "EARTH":
                            numberOfUnchosenEarths += 1;
                            break;
                        case "METAL":
                            numberOfUnchosenMetals += 1;
                            break;
                        case "WATER":
                            numberOfUnchosenWaters += 1;
                            break;
                        case "WOOD":
                            numberOfUnchosenWoods += 1;
                            break;
                        case "FIRE":
                            numberOfUnchosenFires += 1;
                            break;
                    }
                }

                if (strip3Element != strip4Element)
                {
                    switch (strip3Element)
                    {
                        case "EARTH":
                            numberOfUnchosenEarths += 1;
                            break;
                        case "METAL":
                            numberOfUnchosenMetals += 1;
                            break;
                        case "WATER":
                            numberOfUnchosenWaters += 1;
                            break;
                        case "WOOD":
                            numberOfUnchosenWoods += 1;
                            break;
                        case "FIRE":
                            numberOfUnchosenFires += 1;
                            break;
                    }
                }

                if (strip5Element != strip4Element)
                {
                    switch (strip5Element)
                    {
                        case "EARTH":
                            numberOfUnchosenEarths += 1;
                            break;
                        case "METAL":
                            numberOfUnchosenMetals += 1;
                            break;
                        case "WATER":
                            numberOfUnchosenWaters += 1;
                            break;
                        case "WOOD":
                            numberOfUnchosenWoods += 1;
                            break;
                        case "FIRE":
                            numberOfUnchosenFires += 1;
                            break;
                    }
                }

                break;

            case 5:

                chosenStripElement = strip5Element;
                chosenStripFaction = strip5Faction;
                chosenStripCost = strip5Cost;
                chosenStripReward1 = strip5Reward1;
                chosenStripReward2 = strip5Reward2;

                // ALSO COUNT HOW MANY NON-SELECTED ELEMENTS ARE DIFFERENT FROM THE SELECTED ELEMENT
                if (strip1Element != strip5Element)                 // Check the element of all other strips
                {                                                   // If same as this element, ignore
                    switch (strip1Element)                          // If not same
                    {
                        case "EARTH":                               // Check what that element is
                            numberOfUnchosenEarths += 1;            // Add 1 to unchosenELement for that element
                            break;
                        case "METAL":
                            numberOfUnchosenMetals += 1;
                            break;
                        case "WATER":
                            numberOfUnchosenWaters += 1;
                            break;
                        case "WOOD":
                            numberOfUnchosenWoods += 1;
                            break;
                        case "FIRE":
                            numberOfUnchosenFires += 1;
                            break;
                    }
                }

                if (strip2Element != strip5Element)
                {
                    switch (strip2Element)
                    {
                        case "EARTH":
                            numberOfUnchosenEarths += 1;
                            break;
                        case "METAL":
                            numberOfUnchosenMetals += 1;
                            break;
                        case "WATER":
                            numberOfUnchosenWaters += 1;
                            break;
                        case "WOOD":
                            numberOfUnchosenWoods += 1;
                            break;
                        case "FIRE":
                            numberOfUnchosenFires += 1;
                            break;
                    }
                }

                if (strip3Element != strip5Element)
                {
                    switch (strip3Element)
                    {
                        case "EARTH":
                            numberOfUnchosenEarths += 1;
                            break;
                        case "METAL":
                            numberOfUnchosenMetals += 1;
                            break;
                        case "WATER":
                            numberOfUnchosenWaters += 1;
                            break;
                        case "WOOD":
                            numberOfUnchosenWoods += 1;
                            break;
                        case "FIRE":
                            numberOfUnchosenFires += 1;
                            break;
                    }
                }

                if (strip4Element != strip5Element)
                {
                    switch (strip4Element)
                    {
                        case "EARTH":
                            numberOfUnchosenEarths += 1;
                            break;
                        case "METAL":
                            numberOfUnchosenMetals += 1;
                            break;
                        case "WATER":
                            numberOfUnchosenWaters += 1;
                            break;
                        case "WOOD":
                            numberOfUnchosenWoods += 1;
                            break;
                        case "FIRE":
                            numberOfUnchosenFires += 1;
                            break;
                    }
                }

                break;
        }


        if (chosenStripCost == "STATISTICS")
        {
            txtChoicesHeading.text = "REDUCE WHICH STATISTIC?";
        }
        else if (chosenStripCost == "CREATURES")
        {
            txtChoicesHeading.text = "WEAKEN WHICH CREATURE?";
        }
        else if (chosenStripCost == "SPELLS")
        {
            txtChoicesHeading.text = "DISPEL WHICH SPELL?";
        }
        else if (chosenStripCost == "POTIONS")
        {
            txtChoicesHeading.text = "DILUTE WHICH POTION?";
        }
        else if (chosenStripCost == "SUPPORTS")
        {
            txtChoicesHeading.text = "DEPLETE WHICH SUPPORTS?";
        }
        else if (chosenStripCost == "AFFINITIES")
        {
            txtChoicesHeading.text = "OFFEND WHICH FACTION?";
        }
        else if (chosenStripCost == "GOLD")
        {
            txtChoicesHeading.text = "LOSE GOLD OR INCOME/TURN?";
        }

    }


    // For each of the current elements, if it's NOT the same as the chosen element
    // Add +1 to Elemental Attack for that element
    // Update UI 


    public void CalculateChosenElements()                           // All ELEMENTAL changes that need to happen when a strip is selected       
    {
        Debug.Log("CalculateChosenElements() now running.");        // Call this from UpdateChains() in EventFunctionCaller.cs (the values will change while the text is briefly invisible)


        int numberOfEarths = 0;         // Count total number of each element there are on the board
        int numberOfMetals = 0;         // Needed to calculate shields and shield regens below
        int numberOfWaters = 0;
        int numberOfWoods = 0;
        int numberOfFires = 0;

        
        switch (strip1Element)
        {
            case "EARTH":
                numberOfEarths += 1;
                break;
            case "METAL":
                numberOfMetals += 1;
                break;
            case "WATER":
                numberOfWaters += 1;
                break;
            case "WOOD":
                numberOfWoods += 1;
                break;
            case "FIRE":
                numberOfFires += 1;
                break;
        }

        switch (strip2Element)
        {
            case "EARTH":
                numberOfEarths += 1;
                break;
            case "METAL":
                numberOfMetals += 1;
                break;
            case "WATER":
                numberOfWaters += 1;
                break;
            case "WOOD":
                numberOfWoods += 1;
                break;
            case "FIRE":
                numberOfFires += 1;
                break;
        }

        switch (strip3Element)
        {
            case "EARTH":
                numberOfEarths += 1;
                break;
            case "METAL":
                numberOfMetals += 1;
                break;
            case "WATER":
                numberOfWaters += 1;
                break;
            case "WOOD":
                numberOfWoods += 1;
                break;
            case "FIRE":
                numberOfFires += 1;
                break;
        }

        switch (strip4Element)
        {
            case "EARTH":
                numberOfEarths += 1;
                break;
            case "METAL":
                numberOfMetals += 1;
                break;
            case "WATER":
                numberOfWaters += 1;
                break;
            case "WOOD":
                numberOfWoods += 1;
                break;
            case "FIRE":
                numberOfFires += 1;
                break;
        }

        switch (strip5Element)
        {
            case "EARTH":
                numberOfEarths += 1;
                break;
            case "METAL":
                numberOfMetals += 1;
                break;
            case "WATER":
                numberOfWaters += 1;
                break;
            case "WOOD":
                numberOfWoods += 1;
                break;
            case "FIRE":
                numberOfFires += 1;
                break;
        }


        /*
        int numberOfSameElements;       

        switch (chosenStripElement)
        {
            case "EARTH":
                numberOfSameElements = numberOfEarths;
                break;
            case "METAL":
                numberOfSameElements = numberOfMetals;
                break;
            case "WATER":
                numberOfSameElements = numberOfWaters;
                break;
            case "WOOD":
                numberOfSameElements = numberOfWoods;
                break;
            case "FIRE":
                numberOfSameElements = numberOfFires;
                break;
        }
        */


        // Reset TotalChainLength

        if (StripSelectedForFirstTime)
        {
            MainCharacter.chainLengthTotal = 0;
        }

        // +1 Elemental Chain to chosen element
        // +10 Elemental Shield for total number of elements that are same as chosen element
        // +1 Elemental Shield Regen for total number of elements that are same as chosen element
        // +1 To elemental attacks for each of the UNCHOSEN elements

        if (chosenStripElement == "EARTH")
        {
            chainLengthElementalEARTH += 1;                                         // Update ELEMENTAL CHAIN
            MainCharacter.chainLengthTotal += chainLengthElementalEARTH;            // Update TOTAL CHAIN for current turn

            UpdateShieldEARTH((numberOfEarths * 10));
            UpdateShieldEARTHRegen(numberOfEarths);                                 // Update shield regens and update GUI

            UpdateDamageMETAL(numberOfUnchosenMetals);
            UpdateDamageWATER(numberOfUnchosenWaters);
            UpdateDamageWOOD(numberOfUnchosenWoods);
            UpdateDamageFIRE(numberOfUnchosenFires);
        }
        else if (chosenStripElement == "METAL")
        {
            chainLengthElementalMETAL += 1;                                         // Update ELEMENTAL CHAIN
            MainCharacter.chainLengthTotal += chainLengthElementalMETAL;            // Update TOTAL CHAIN for current turn

            UpdateShieldMETAL((numberOfMetals * 10));
            UpdateShieldMETALRegen(numberOfMetals);                                 // Update shield regens and update GUI

            UpdateDamageEARTH(numberOfUnchosenEarths);
            UpdateDamageWATER(numberOfUnchosenWaters);
            UpdateDamageWOOD(numberOfUnchosenWoods);
            UpdateDamageFIRE(numberOfUnchosenFires);
        }
        else if (chosenStripElement == "WATER")
        {
            chainLengthElementalWATER += 1;                                         // Update ELEMENTAL CHAIN  
            MainCharacter.chainLengthTotal += chainLengthElementalWATER;            // Update TOTAL CHAIN for current turn

            UpdateShieldWATER((numberOfWaters * 10));
            UpdateShieldWATERRegen(numberOfWaters);                                 // Update shield regens and update GUI

            UpdateDamageEARTH(numberOfUnchosenEarths);
            UpdateDamageMETAL(numberOfUnchosenMetals);           
            UpdateDamageWOOD(numberOfUnchosenWoods);
            UpdateDamageFIRE(numberOfUnchosenFires);
        }
        else if (chosenStripElement == "WOOD")
        {
            chainLengthElementalWOOD += 1;                                          // Update ELEMENTAL CHAIN
            MainCharacter.chainLengthTotal += chainLengthElementalWOOD;             // Update TOTAL CHAIN for current turn

            UpdateShieldWOOD((numberOfWoods * 10));
            UpdateShieldWOODRegen(numberOfWoods);                                   // Update shield regens and update GUI

            UpdateDamageEARTH(numberOfUnchosenEarths);
            UpdateDamageMETAL(numberOfUnchosenMetals);
            UpdateDamageWATER(numberOfUnchosenWaters);
            UpdateDamageFIRE(numberOfUnchosenFires);
        }
        else
        {
            chainLengthElementalFIRE += 1;                                          // Update ELEMENTAL CHAIN
            MainCharacter.chainLengthTotal += chainLengthElementalFIRE;             // Update TOTAL CHAIN for current turn

            UpdateShieldFIRE((numberOfFires * 10));
            UpdateShieldFIRERegen(numberOfFires);                                   // Update shield regens and update GUI

            UpdateDamageEARTH(numberOfUnchosenEarths);
            UpdateDamageMETAL(numberOfUnchosenMetals);
            UpdateDamageWATER(numberOfUnchosenWaters);
            UpdateDamageWOOD(numberOfUnchosenWoods);
        }


        // Update the GUI with the Elemental Chain score for the chosen element

        if (chosenStripElement == "EARTH")
        {
            txtAdventureMainChainElemental.text = chainLengthElementalEARTH.ToString();
            // banana
        }
        else if (chosenStripElement == "METAL")
        {
            txtAdventureMainChainElemental.text = chainLengthElementalMETAL.ToString();
        }
        else if (chosenStripElement == "WATER")
        {
            txtAdventureMainChainElemental.text = chainLengthElementalWATER.ToString();
        }
        else if (chosenStripElement == "WOOD")
        {
            txtAdventureMainChainElemental.text = chainLengthElementalWOOD.ToString();
        }
        else if (chosenStripElement == "FIRE")
        {
            txtAdventureMainChainElemental.text = chainLengthElementalFIRE.ToString();
        }


        // Update the GUI with the TOTAL CHAIN SCORE for all chains

        UpdateGUIChainLengthTotal();        

    }

    public void CalculateChosenFactions()                           // All FACTIONAL changes that need to happen when a strip is selected  
    {
        Debug.Log("CalculateChosenFactions() now running.");        // Call this from UpdateChains() in EventFunctionCaller.cs (the values will change while the text is briefly invisible)

        int numberOfAmphibians = 0;                                 // Count total number of each faction there are on the board
        int numberOfAquatics = 0;                                   // Needed to calculate total faction affinity gains
        int numberOfAvians = 0;
        int numberOfBeasts = 0;
        int numberOfBrutes = 0;
        int numberOfBugs = 0;
        int numberOfDemons = 0;
        int numberOfDragons = 0;
        int numberOfElementals = 0;
        int numberOfEthereals = 0;
        int numberOfHumans = 0;
        int numberOfPhytos = 0;
        int numberOfReptilians = 0;
        int numberOfUndeads = 0;

        switch (strip1Faction)                                      
        {
            case "AMPHIBIAN":
                numberOfAmphibians += 1;
                break;
            case "AQUATIC":
                numberOfAquatics += 1;
                break;
            case "AVIAN":
                numberOfAvians += 1;
                break;
            case "BEAST":
                numberOfBeasts += 1;
                break;
            case "BRUTE":
                numberOfBrutes += 1;
                break;
            case "BUG":
                numberOfBugs += 1;
                break;
            case "DEMON":
                numberOfDemons += 1;
                break;
            case "DRAGON":
                numberOfDragons += 1;
                break;
            case "ELEMENTAL":
                numberOfElementals += 1;
                break;
            case "ETHEREAL":
                numberOfEthereals += 1;
                break;
            case "HUMAN":
                numberOfHumans += 1;
                break;
            case "PHYTO":
                numberOfPhytos += 1;
                break;
            case "REPTILIAN":
                numberOfReptilians += 1;
                break;
            case "UNDEAD":
                numberOfUndeads += 1;
                break;
        }

        switch (strip2Faction)
        {
            case "AMPHIBIAN":
                numberOfAmphibians += 1;
                break;
            case "AQUATIC":
                numberOfAquatics += 1;
                break;
            case "AVIAN":
                numberOfAvians += 1;
                break;
            case "BEAST":
                numberOfBeasts += 1;
                break;
            case "BRUTE":
                numberOfBrutes += 1;
                break;
            case "BUG":
                numberOfBugs += 1;
                break;
            case "DEMON":
                numberOfDemons += 1;
                break;
            case "DRAGON":
                numberOfDragons += 1;
                break;
            case "ELEMENTAL":
                numberOfElementals += 1;
                break;
            case "ETHEREAL":
                numberOfEthereals += 1;
                break;
            case "HUMAN":
                numberOfHumans += 1;
                break;
            case "PHYTO":
                numberOfPhytos += 1;
                break;
            case "REPTILIAN":
                numberOfReptilians += 1;
                break;
            case "UNDEAD":
                numberOfUndeads += 1;
                break;
        }

        switch (strip3Faction)
        {
            case "AMPHIBIAN":
                numberOfAmphibians += 1;
                break;
            case "AQUATIC":
                numberOfAquatics += 1;
                break;
            case "AVIAN":
                numberOfAvians += 1;
                break;
            case "BEAST":
                numberOfBeasts += 1;
                break;
            case "BRUTE":
                numberOfBrutes += 1;
                break;
            case "BUG":
                numberOfBugs += 1;
                break;
            case "DEMON":
                numberOfDemons += 1;
                break;
            case "DRAGON":
                numberOfDragons += 1;
                break;
            case "ELEMENTAL":
                numberOfElementals += 1;
                break;
            case "ETHEREAL":
                numberOfEthereals += 1;
                break;
            case "HUMAN":
                numberOfHumans += 1;
                break;
            case "PHYTO":
                numberOfPhytos += 1;
                break;
            case "REPTILIAN":
                numberOfReptilians += 1;
                break;
            case "UNDEAD":
                numberOfUndeads += 1;
                break;
        }

        switch (strip4Faction)
        {
            case "AMPHIBIAN":
                numberOfAmphibians += 1;
                break;
            case "AQUATIC":
                numberOfAquatics += 1;
                break;
            case "AVIAN":
                numberOfAvians += 1;
                break;
            case "BEAST":
                numberOfBeasts += 1;
                break;
            case "BRUTE":
                numberOfBrutes += 1;
                break;
            case "BUG":
                numberOfBugs += 1;
                break;
            case "DEMON":
                numberOfDemons += 1;
                break;
            case "DRAGON":
                numberOfDragons += 1;
                break;
            case "ELEMENTAL":
                numberOfElementals += 1;
                break;
            case "ETHEREAL":
                numberOfEthereals += 1;
                break;
            case "HUMAN":
                numberOfHumans += 1;
                break;
            case "PHYTO":
                numberOfPhytos += 1;
                break;
            case "REPTILIAN":
                numberOfReptilians += 1;
                break;
            case "UNDEAD":
                numberOfUndeads += 1;
                break;
        }

        switch (strip5Faction)
        {
            case "AMPHIBIAN":
                numberOfAmphibians += 1;
                break;
            case "AQUATIC":
                numberOfAquatics += 1;
                break;
            case "AVIAN":
                numberOfAvians += 1;
                break;
            case "BEAST":
                numberOfBeasts += 1;
                break;
            case "BRUTE":
                numberOfBrutes += 1;
                break;
            case "BUG":
                numberOfBugs += 1;
                break;
            case "DEMON":
                numberOfDemons += 1;
                break;
            case "DRAGON":
                numberOfDragons += 1;
                break;
            case "ELEMENTAL":
                numberOfElementals += 1;
                break;
            case "ETHEREAL":
                numberOfEthereals += 1;
                break;
            case "HUMAN":
                numberOfHumans += 1;
                break;
            case "PHYTO":
                numberOfPhytos += 1;
                break;
            case "REPTILIAN":
                numberOfReptilians += 1;
                break;
            case "UNDEAD":
                numberOfUndeads += 1;
                break;
        }


        // +1 Faction Chain to chosen faction
        // +10 Faction Affinity for total number of factions that are same as chosen faction

        if (chosenStripFaction == "AMPHIBIAN") 
        {
            chainLengthAffinityAMPHIBIAN += 1;                                      // Update FACTION CHAIN length
            MainCharacter.chainLengthTotal += chainLengthAffinityAMPHIBIAN;         // Update TOTAL CHAIN for current turn

            UpdateAffinityAMPHIBIAN((numberOfAmphibians * 10));                     // Update Faction Affinity
            // MainCharacter.affinityAMPHIBIAN += (numberOfAmphibians * 10);           
        }
        else if (chosenStripFaction == "AQUATIC")
        {
            chainLengthAffinityAQUATIC += 1;                                        // Update FACTION CHAIN length
            MainCharacter.chainLengthTotal += chainLengthAffinityAQUATIC;           // Update TOTAL CHAIN for current turn

            UpdateAffinityAQUATIC((numberOfAquatics * 10));                        // Update Faction Affinity
            //MainCharacter.affinityAQUATIC += (numberOfAquatics * 10);             // Update Faction Affinity
        }
        else if (chosenStripFaction == "AVIAN")
        {
            chainLengthAffinityAVIAN += 1;                                          // Update FACTION CHAIN length
            MainCharacter.chainLengthTotal += chainLengthAffinityAVIAN;             // Update TOTAL CHAIN for current turn

            UpdateAffinityAVIAN((numberOfAvians * 10));                             // Update Faction Affinity
            //MainCharacter.affinityAVIAN += (numberOfAvians * 10);                 // Update Faction Affinity
        }
        else if (chosenStripFaction == "BEAST")
        {
            chainLengthAffinityBEAST += 1;                                          // Update FACTION CHAIN length
            MainCharacter.chainLengthTotal += chainLengthAffinityBEAST;            // Update TOTAL CHAIN for current turn

            UpdateAffinityBEAST((numberOfBeasts * 10));                             // Update Faction Affinity
            //MainCharacter.affinityBEAST += (numberOfBeasts * 10);                   // Update Faction Affinity
        }
        else if (chosenStripFaction == "BRUTE")
        {
            chainLengthAffinityBRUTE += 1;                                          // Update FACTION CHAIN length
            MainCharacter.chainLengthTotal += chainLengthAffinityBRUTE;            // Update TOTAL CHAIN for current turn

            UpdateAffinityBRUTE((numberOfBrutes * 10));                             // Update Faction Affinity
            //MainCharacter.affinityBRUTE += (numberOfBrutes * 10);                   // Update Faction Affinity
        }
        else if (chosenStripFaction == "BUG")
        {
            chainLengthAffinityBUG += 1;                                          // Update FACTION CHAIN length
            MainCharacter.chainLengthTotal += chainLengthAffinityBUG;            // Update TOTAL CHAIN for current turn

            UpdateAffinityBUG((numberOfBugs * 10));                             // Update Faction Affinity
            //MainCharacter.affinityBUG += (numberOfBugs * 10);                   // Update Faction Affinity
        }
        else if (chosenStripFaction == "DEMON")
        {
            chainLengthAffinityDEMON += 1;                                          // Update FACTION CHAIN length
            MainCharacter.chainLengthTotal += chainLengthAffinityDEMON;            // Update TOTAL CHAIN for current turn

            UpdateAffinityDEMON((numberOfDemons * 10));                             // Update Faction Affinity
            //MainCharacter.affinityDEMON += (numberOfDemons * 10);                   // Update Faction Affinity
        }
        else if (chosenStripFaction == "DRAGON")
        {
            chainLengthAffinityDRAGON += 1;                                          // Update FACTION CHAIN length
            MainCharacter.chainLengthTotal += chainLengthAffinityDRAGON;           // Update TOTAL CHAIN for current turn

            UpdateAffinityDRAGON((numberOfDragons * 10));                             // Update Faction Affinity
            //MainCharacter.affinityDRAGON += (numberOfDragons * 10);                   // Update Faction Affinity
        }
        else if (chosenStripFaction == "ELEMENTAL")
        {
            chainLengthAffinityELEMENTAL += 1;                                          // Update FACTION CHAIN length
            MainCharacter.chainLengthTotal += chainLengthAffinityELEMENTAL;            // Update TOTAL CHAIN for current turn

            UpdateAffinityELEMENTAL((numberOfElementals * 10));                             // Update Faction Affinity
            MainCharacter.affinityELEMENTAL += (numberOfElementals * 10);                   // Update Faction Affinity
        }
        else if (chosenStripFaction == "ETHEREAL")
        {
            chainLengthAffinityETHEREAL += 1;                                          // Update FACTION CHAIN length
            MainCharacter.chainLengthTotal += chainLengthAffinityETHEREAL;            // Update TOTAL CHAIN for current turn

            UpdateAffinityETHEREAL((numberOfEthereals * 10));                             // Update Faction Affinity
            //MainCharacter.affinityETHEREAL += (numberOfEthereals * 10);                   // Update Faction Affinity
        }
        else if (chosenStripFaction == "HUMAN")
        {
            chainLengthAffinityHUMAN += 1;                                          // Update FACTION CHAIN length
            MainCharacter.chainLengthTotal += chainLengthAffinityHUMAN;            // Update TOTAL CHAIN for current turn

            UpdateAffinityHUMAN((numberOfHumans * 10));                             // Update Faction Affinity
            //MainCharacter.affinityHUMAN += (numberOfHumans * 10);                   // Update Faction Affinity
        }
        else if (chosenStripFaction == "PHYTO")
        {
            chainLengthAffinityPHYTO += 1;                                          // Update FACTION CHAIN length
            MainCharacter.chainLengthTotal += chainLengthAffinityPHYTO;            // Update TOTAL CHAIN for current turn

            UpdateAffinityPHYTO((numberOfPhytos * 10));                             // Update Faction Affinity
            //MainCharacter.affinityPHYTO += (numberOfPhytos * 10);                   // Update Faction Affinity
        }
        else if (chosenStripFaction == "REPTILIAN")
        {
            chainLengthAffinityREPTILIAN += 1;                                      // Update FACTION CHAIN length
            MainCharacter.chainLengthTotal += chainLengthAffinityREPTILIAN;        // Update TOTAL CHAIN for current turn

            UpdateAffinityREPTILIAN((numberOfReptilians * 10));                             // Update Faction Affinity
            //MainCharacter.affinityREPTILIAN += (numberOfReptilians * 10);           // Update Faction Affinity
        }
        else if (chosenStripFaction == "UNDEAD")
        {
            chainLengthAffinityUNDEAD += 1;                                          // Update FACTION CHAIN length
            MainCharacter.chainLengthTotal += chainLengthAffinityUNDEAD;            // Update TOTAL CHAIN for current turn

            UpdateAffinityUNDEAD((numberOfUndeads * 10));                             // Update Faction Affinity
            //MainCharacter.affinityUNDEAD += (numberOfUndeads * 10);                   // Update Faction Affinity
        }


        // Update the GUI with the FACTION CHAIN score for the chosen element

        if (chosenStripFaction == "AMPHIBIAN")
        {
            txtAdventureMainChainAffinity.text = chainLengthAffinityAMPHIBIAN.ToString();
        }
        if (chosenStripFaction == "AQUATIC")
        {
            txtAdventureMainChainAffinity.text = chainLengthAffinityAQUATIC.ToString();
        }
        if (chosenStripFaction == "AVIAN")
        {
            txtAdventureMainChainAffinity.text = chainLengthAffinityAVIAN.ToString();
        }
        if (chosenStripFaction == "BEAST")
        {
            txtAdventureMainChainAffinity.text = chainLengthAffinityBEAST.ToString();
        }
        if (chosenStripFaction == "BRUTE")
        {
            txtAdventureMainChainAffinity.text = chainLengthAffinityBRUTE.ToString();
        }
        if (chosenStripFaction == "BUG")
        {
            txtAdventureMainChainAffinity.text = chainLengthAffinityBUG.ToString();
        }
        if (chosenStripFaction == "DEMON")
        {
            txtAdventureMainChainAffinity.text = chainLengthAffinityDEMON.ToString();
        }
        if (chosenStripFaction == "DRAGON")
        {
            txtAdventureMainChainAffinity.text = chainLengthAffinityDRAGON.ToString();
        }
        if (chosenStripFaction == "ELEMENTAL")
        {
            txtAdventureMainChainAffinity.text = chainLengthAffinityELEMENTAL.ToString();
        }
        if (chosenStripFaction == "ETHEREAL")
        {
            txtAdventureMainChainAffinity.text = chainLengthAffinityETHEREAL.ToString();
        }
        if (chosenStripFaction == "HUMAN")
        {
            txtAdventureMainChainAffinity.text = chainLengthAffinityHUMAN.ToString();
        }
        if (chosenStripFaction == "PHYTO")
        {
            txtAdventureMainChainAffinity.text = chainLengthAffinityPHYTO.ToString();
        }
        if (chosenStripFaction == "REPTILIAN")
        {
            txtAdventureMainChainAffinity.text = chainLengthAffinityREPTILIAN.ToString();
        }
        if (chosenStripFaction == "UNDEAD")
        {
            txtAdventureMainChainAffinity.text = chainLengthAffinityUNDEAD.ToString();
        }

        // Update the GUI with the TOTAL CHAIN SCORE for all chains

        UpdateGUIChainLengthTotal();

    }

    public void CalculateChosenCosts()
    {
        Debug.Log("CalculateChosenCosts() now running.");       // Call this from UpdateChains() in EventFunctionCaller.cs (the values will change while the text is briefly invisible)


        // +1 Cost Chain to chosen cost

        if (chosenStripCost == "STATISTICS")
        {
            chainLengthCostSTATISTICS += 1;                                     // Update COST CHAIN length
            MainCharacter.chainLengthTotal += chainLengthCostSTATISTICS;        // Update TOTAL CHAIN for current turn
        }
        else if (chosenStripCost == "CREATURES")
        {
            chainLengthCostCREATURES += 1;                                     // Update COST CHAIN length
            MainCharacter.chainLengthTotal += chainLengthCostCREATURES;        // Update TOTAL CHAIN for current turn
        }
        else if (chosenStripCost == "SPELLS")
        {
            chainLengthCostSPELLS += 1;                                     // Update COST CHAIN length
            MainCharacter.chainLengthTotal += chainLengthCostSPELLS;        // Update TOTAL CHAIN for current turn
        }
        else if (chosenStripCost == "POTIONS")
        {
            chainLengthCostPOTIONS += 1;                                     // Update COST CHAIN length
            MainCharacter.chainLengthTotal += chainLengthCostPOTIONS;        // Update TOTAL CHAIN for current turn
        }
        else if (chosenStripCost == "SUPPORTS")
        {
            chainLengthCostSUPPORTS += 1;                                     // Update COST CHAIN length
            MainCharacter.chainLengthTotal += chainLengthCostSUPPORTS;        // Update TOTAL CHAIN for current turn
        }
        else if (chosenStripCost == "AFFINITIES")
        {
            chainLengthCostAFFINITIES += 1;                                     // Update COST CHAIN length
            MainCharacter.chainLengthTotal += chainLengthCostAFFINITIES;        // Update TOTAL CHAIN for current turn
        }
        else if (chosenStripCost == "GOLD")
        {
            chainLengthCostGOLD += 1;                                     // Update COST CHAIN length
            MainCharacter.chainLengthTotal += chainLengthCostGOLD;        // Update TOTAL CHAIN for current turn
        }



        // Update the GUI with the COST CHAIN score for the chosen cost

        if (chosenStripCost == "STATISTICS")
        {
            txtAdventureMainChainCost.text = chainLengthCostSTATISTICS.ToString();
        }
        else if (chosenStripCost == "CREATURES")
        {
            txtAdventureMainChainCost.text = chainLengthCostCREATURES.ToString();
        }
        else if (chosenStripCost == "SPELLS")
        {
            txtAdventureMainChainCost.text = chainLengthCostSPELLS.ToString();
        }
        else if (chosenStripCost == "POTIONS")
        {
            txtAdventureMainChainCost.text = chainLengthCostPOTIONS.ToString();
        }
        else if (chosenStripCost == "SUPPORTS")
        {
            txtAdventureMainChainCost.text = chainLengthCostSUPPORTS.ToString();
        }
        else if (chosenStripCost == "AFFINITIES")
        {
            txtAdventureMainChainCost.text = chainLengthCostAFFINITIES.ToString();
        }
        else if (chosenStripCost == "GOLD")
        {
            txtAdventureMainChainCost.text = chainLengthCostGOLD.ToString();
        }


        // Update the GUI with the TOTAL CHAIN SCORE for all chains

        UpdateGUIChainLengthTotal();

    }


    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
    //      METHODS FOR UPDATING GUI AFTER CHAIN LENGTH DATA IS KNOWN
    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- //

    /*
    public void UpdateChainLengthElemental(int x)                       // Update ELEMENTAL CHAIN LENGTH
    {
        if(x != 0)                                                      // Argument must be 1 if current encounter race is same as previous, otherwise 0
        {
            MainCharacter.chainLengthElemental += x;
        }
        else
        {
            MainCharacter.chainLengthElemental = 0;
        }

        txtAdventureMainChainElemental.text = MainCharacter.chainLengthElemental.ToString();    // Update GUI
        UpdateChainLengthTotal();                                                               // Update TOTAL CHAIN LENGTH
    }

  

    public void UpdateChainLengthAffinity(int x)                        // Update AFFINITY CHAIN LENGTH
    {
        if (x != 0)                                                    // Argument must be 1 if current encounter race is same as previous, otherwise 0
        {
            MainCharacter.chainLengthRacial += x;
        }
        else
        {
            MainCharacter.chainLengthRacial = 0;
        }

        txtAdventureMainChainAffinity.text = MainCharacter.chainLengthRacial.ToString();        // Update GUI
        UpdateChainLengthTotal();                                                              // Update TOTAL CHAIN LENGTH
    }

    public void UpdateChainLengthCost(int x)                        // Update COST CHAIN LENGTH
    {
        if (x != 0)                                                 // Argument must be 1 if current encounter race is same as previous, otherwise 0
        {
            MainCharacter.chainLengthCost += x;
        }
        else
        {
            MainCharacter.chainLengthCost = 0;
        }

        txtAdventureMainChainCost.text = MainCharacter.chainLengthCost.ToString();              // Update GUI
        UpdateChainLengthTotal();                                                               // Update TOTAL CHAIN LENGTH
    }

    */

    public void UpdateGUIChainLengthElemental()                            // Write the current Elemental Chain values to the GUI
    {

        /*
        if (chosenStripElement == "EARTH")
        {
            txtAdventureMainChainElemental.text = chainLengthElementalEARTH.ToString();
        }
        else if (chosenStripElement == "METAL")
        {
            txtAdventureMainChainElemental.text = chainLengthElementalMETAL.ToString();
        }
        else if (chosenStripElement == "WATER")
        {
            txtAdventureMainChainElemental.text = chainLengthElementalWATER.ToString();
        }
        else if (chosenStripElement == "WOOD")
        {
            txtAdventureMainChainElemental.text = chainLengthElementalWOOD.ToString();
        }
        else if (chosenStripElement == "FIRE")
        {
            txtAdventureMainChainElemental.text = chainLengthElementalFIRE.ToString();
        }
        */

    }

    public void UpdateGUIChainLengthAffinity()                            // Write the current Affinity Chain values to the GUI
    {


    }

    public void UpdateGUIChainLengthCost()                              // Write the current Affinity Chain values to the GUI
    {


    }

    public void UpdateGUIChainLengthTotal()                            // Write the current Affinity Chain values to the GUI
    {

        // MainCharacter.chainLengthTotal = MainCharacter.chainLengthElemental + MainCharacter.chainLengthRacial + MainCharacter.chainLengthCost;
        txtAdventureMainChainTotal.text = MainCharacter.chainLengthTotal.ToString();
    }




    public void SetAffinityColors()                             // Check all affinity scores. If any are negative, change them to RED, otherwise GREY
    {
        if (MainCharacter.affinityAMPHIBIAN < 0)
        {
            txtTabAffinitiesAMPHIBIAN.color = new Color(209.0f / 255.0f, 58.0f / 255.0f, 58.0f / 255.0f);
        }
        else
        {
            txtTabAffinitiesAMPHIBIAN.color = new Color(118.0f / 255.0f, 118.0f / 255.0f, 118.0f / 255.0f);
        }

        if (MainCharacter.affinityAQUATIC < 0)
        {
            txtTabAffinitiesAQUATIC.color = new Color(209.0f / 255.0f, 58.0f / 255.0f, 58.0f / 255.0f);
        }
        else
        {
            txtTabAffinitiesAQUATIC.color = new Color(118.0f / 255.0f, 118.0f / 255.0f, 118.0f / 255.0f);
        }

        if (MainCharacter.affinityAVIAN < 0)
        {
            txtTabAffinitiesAVIAN.color = new Color(209.0f / 255.0f, 58.0f / 255.0f, 58.0f / 255.0f);
        }
        else
        {
            txtTabAffinitiesAVIAN.color = new Color(118.0f / 255.0f, 118.0f / 255.0f, 118.0f / 255.0f);
        }

        if (MainCharacter.affinityBEAST < 0)
        {
            txtTabAffinitiesBEAST.color = new Color(209.0f / 255.0f, 58.0f / 255.0f, 58.0f / 255.0f);
        }
        else
        {
            txtTabAffinitiesBEAST.color = new Color(118.0f / 255.0f, 118.0f / 255.0f, 118.0f / 255.0f);
        }

        if (MainCharacter.affinityBRUTE < 0)
        {
            txtTabAffinitiesBRUTE.color = new Color(209.0f / 255.0f, 58.0f / 255.0f, 58.0f / 255.0f);
        }
        else
        {
            txtTabAffinitiesBRUTE.color = new Color(118.0f / 255.0f, 118.0f / 255.0f, 118.0f / 255.0f);
        }

        if (MainCharacter.affinityBUG < 0)
        {
            txtTabAffinitiesBUG.color = new Color(209.0f / 255.0f, 58.0f / 255.0f, 58.0f / 255.0f);
        }
        else
        {
            txtTabAffinitiesBUG.color = new Color(118.0f / 255.0f, 118.0f / 255.0f, 118.0f / 255.0f);
        }

        if (MainCharacter.affinityDEMON < 0)
        {
            txtTabAffinitiesDEMON.color = new Color(209.0f / 255.0f, 58.0f / 255.0f, 58.0f / 255.0f);
        }
        else
        {
            txtTabAffinitiesDEMON.color = new Color(118.0f / 255.0f, 118.0f / 255.0f, 118.0f / 255.0f);
        }

        if (MainCharacter.affinityDRAGON < 0)
        {
            txtTabAffinitiesDRAGON.color = new Color(209.0f / 255.0f, 58.0f / 255.0f, 58.0f / 255.0f);
        }
        else
        {
            txtTabAffinitiesDRAGON.color = new Color(118.0f / 255.0f, 118.0f / 255.0f, 118.0f / 255.0f);
        }

        if (MainCharacter.affinityELEMENTAL < 0)
        {
            txtTabAffinitiesELEMENTAL.color = new Color(209.0f / 255.0f, 58.0f / 255.0f, 58.0f / 255.0f);
        }
        else
        {
            txtTabAffinitiesELEMENTAL.color = new Color(118.0f / 255.0f, 118.0f / 255.0f, 118.0f / 255.0f);
        }

        if (MainCharacter.affinityETHEREAL < 0)
        {
            txtTabAffinitiesETHEREAL.color = new Color(209.0f / 255.0f, 58.0f / 255.0f, 58.0f / 255.0f);
        }
        else
        {
            txtTabAffinitiesETHEREAL.color = new Color(118.0f / 255.0f, 118.0f / 255.0f, 118.0f / 255.0f);
        }

        if (MainCharacter.affinityHUMAN < 0)
        {
            txtTabAffinitiesHUMAN.color = new Color(209.0f / 255.0f, 58.0f / 255.0f, 58.0f / 255.0f);
        }
        else
        {
            txtTabAffinitiesHUMAN.color = new Color(118.0f / 255.0f, 118.0f / 255.0f, 118.0f / 255.0f);
        }

        if (MainCharacter.affinityPHYTO < 0)
        {
            txtTabAffinitiesPHYTO.color = new Color(209.0f / 255.0f, 58.0f / 255.0f, 58.0f / 255.0f);
        }
        else
        {
            txtTabAffinitiesPHYTO.color = new Color(118.0f / 255.0f, 118.0f / 255.0f, 118.0f / 255.0f);
        }

        if (MainCharacter.affinityREPTILIAN < 0)
        {
            txtTabAffinitiesREPTILIAN.color = new Color(209.0f / 255.0f, 58.0f / 255.0f, 58.0f / 255.0f);
        }
        else
        {
            txtTabAffinitiesREPTILIAN.color = new Color(118.0f / 255.0f, 118.0f / 255.0f, 118.0f / 255.0f);
        }

        if (MainCharacter.affinityUNDEAD < 0)
        {
            txtTabAffinitiesUNDEAD.color = new Color(209.0f / 255.0f, 58.0f / 255.0f, 58.0f / 255.0f);
        }
        else
        {
            txtTabAffinitiesUNDEAD.color = new Color(118.0f / 255.0f, 118.0f / 255.0f, 118.0f / 255.0f);
        }
    }


    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
    //      TAB CONTROL + INITIALISATION METHODS
    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- //

    public void SetAllTabIconsToNormal()
    {
        btnIconStats.image.sprite = btnIconStatsNormal;                 // De-highlights all icons
        btnIconCreatures.image.sprite = btnIconCreaturesNormal;
        btnIconSpells.image.sprite = btnIconSpellsNormal;
        btnIconPotions.image.sprite = btnIconPotionsNormal;
        btnIconSupports.image.sprite = btnIconSupportsNormal;
        btnIconAffinities.image.sprite = btnIconAffinitiesNormal;
        btnIconGold.image.sprite = btnIconGoldNormal;
    }

    public void HideAllTabs()
    {
        tabStatistics.SetActive(false);                              // Hide all tabs (before unhiding the chosen one)
        tabCreatures.SetActive(false);
        tabSpells.SetActive(false);
        tabPotions.SetActive(false);
        tabSupports.SetActive(false);
        tabAffinities.SetActive(false);
        tabGold.SetActive(false);
    }

    public void ShowTabStatistics()
    {
        SetAllTabIconsToNormal();                                   // De-highlight all icons
        btnIconStats.image.sprite = btnIconStatsHover;              // Set the STATISTICS icon to highlighted

        HideAllTabs();                                              // Hide all tabs
        tabStatistics.SetActive(true);                              // Show the tab for STATISTICS

        txtTabName.text = "Personal Statistics";                    // Update the displayed tab name (next to commander portrait)
    }

    public void ShowTabCreatures()
    {
        SetAllTabIconsToNormal();                                   // De-highlight all icons
        btnIconCreatures.image.sprite = btnIconCreaturesHover;      // Set the STATISTICS icon to highlighted

        HideAllTabs();
        tabCreatures.SetActive(true);                               // Show the tab for CREATURES

        txtTabName.text = "Creatures";                              // Update the displayed tab name (next to commander portrait)
    }

    public void ShowTabSpells()
    {
        SetAllTabIconsToNormal();                                   // De-highlight all icons
        btnIconSpells.image.sprite = btnIconSpellsHover;            // Set the STATISTICS icon to highlighted

        HideAllTabs();
        tabSpells.SetActive(true);                                  // Show the tab for CREATURES

        txtTabName.text = "Spells";                                 // Update the displayed tab name (next to commander portrait)
    }

    public void ShowTabPotions()
    {
        SetAllTabIconsToNormal();                                   // De-highlight all icons
        btnIconPotions.image.sprite = btnIconPotionsHover;          // Set the STATISTICS icon to highlighted

        HideAllTabs();
        tabPotions.SetActive(true);                                 // Show the tab for CREATURES

        txtTabName.text = "Potions & Poisons";                      // Update the displayed tab name (next to commander portrait)
    }

    public void ShowTabSupports()
    {
        SetAllTabIconsToNormal();                                   // De-highlight all icons
        btnIconSupports.image.sprite = btnIconSupportsHover;        // Set the STATISTICS icon to highlighted

        HideAllTabs();
        tabSupports.SetActive(true);                                // Show the tab for CREATURES

        txtTabName.text = "Supports";                               // Update the displayed tab name (next to commander portrait)
    }

    public void ShowTabAffinities()
    {
        SetAllTabIconsToNormal();                                   // De-highlight all icons
        btnIconAffinities.image.sprite = btnIconAffinitiesHover;      // Set the STATISTICS icon to highlighted

        HideAllTabs();
        tabAffinities.SetActive(true);                               // Show the tab for CREATURES

        txtTabName.text = "Affinities";                              // Update the displayed tab name (next to commander portrait)
    }

    public void ShowTabGold()
    {
        SetAllTabIconsToNormal();                                   // De-highlight all icons
        btnIconGold.image.sprite = btnIconGoldHover;                // Set the STATISTICS icon to highlighted

        HideAllTabs();
        tabGold.SetActive(true);                                    // Show the tab for CREATURES

        txtTabName.text = "Gold";                                   // Update the displayed tab name (next to commander portrait)
    }

    public void SetPortrait()                                                               // NOTE: This method is called during the Adventure Main panel's OnStateEnter() behaviour for its SlideIn animation state (see the script SetPortrait_PanelAdventureMain.cs)
    {
        if (MainCharacter.chosenCommander == "AMPHORA")
        {
            imgAdventureMainPortrait.sprite = spritePortraitAmphibianAMPHORA;
        }
        else if (MainCharacter.chosenCommander == "CROACK")
        {
            imgAdventureMainPortrait.sprite = spritePortraitAmphibianCROACK;
        }
        else if (MainCharacter.chosenCommander == "GULLET")
        {
            imgAdventureMainPortrait.sprite = spritePortraitAmphibianGULLET;
        }
        else if (MainCharacter.chosenCommander == "ROG")
        {
            imgAdventureMainPortrait.sprite = spritePortraitAmphibianROG;
        }
        else if (MainCharacter.chosenCommander == "SALLA")
        {
            imgAdventureMainPortrait.sprite = spritePortraitAmphibianSALLA;
        }
        else if (MainCharacter.chosenCommander == "BLUB")
        {
            imgAdventureMainPortrait.sprite = spritePortraitAquaticBLUB;
        }
        else if (MainCharacter.chosenCommander == "MERA")
        {
            imgAdventureMainPortrait.sprite = spritePortraitAquaticMERA;
        }
        else if (MainCharacter.chosenCommander == "OCTUS")
        {
            imgAdventureMainPortrait.sprite = spritePortraitAquaticOCTUS;
        }
        else if (MainCharacter.chosenCommander == "SHAREK")
        {
            imgAdventureMainPortrait.sprite = spritePortraitAquaticSHAREK;
        }
        else if (MainCharacter.chosenCommander == "THULLU")
        {
            imgAdventureMainPortrait.sprite = spritePortraitAquaticTHULLU;
        }
        else if (MainCharacter.chosenCommander == "BILL")
        {
            imgAdventureMainPortrait.sprite = spritePortraitAvianBILL;
        }
        else if (MainCharacter.chosenCommander == "GIBBS")
        {
            imgAdventureMainPortrait.sprite = spritePortraitAvianGIBBS;
        }
        else if (MainCharacter.chosenCommander == "HARPETTE")
        {
            imgAdventureMainPortrait.sprite = spritePortraitAvianHARPETTE;
        }
        else if (MainCharacter.chosenCommander == "IZORA")
        {
            imgAdventureMainPortrait.sprite = spritePortraitAvianIZORA;
        }
        else if (MainCharacter.chosenCommander == "YRAG")
        {
            imgAdventureMainPortrait.sprite = spritePortraitAvianYRAG;
        }
        else if (MainCharacter.chosenCommander == "BLORN")
        {
            imgAdventureMainPortrait.sprite = spritePortraitBeastBLORN;
        }
        else if (MainCharacter.chosenCommander == "GARLAND")
        {
            imgAdventureMainPortrait.sprite = spritePortraitBeastGARLAND;
        }
        else if (MainCharacter.chosenCommander == "LIONEL")
        {
            imgAdventureMainPortrait.sprite = spritePortraitBeastLIONEL;
        }
        else if (MainCharacter.chosenCommander == "MONKLEY")
        {
            imgAdventureMainPortrait.sprite = spritePortraitBeastMONKLEY;
        }
        else if (MainCharacter.chosenCommander == "PIGLET")
        {
            imgAdventureMainPortrait.sprite = spritePortraitBeastPIGLET;
        }
        else if (MainCharacter.chosenCommander == "BLOODBORN")
        {
            imgAdventureMainPortrait.sprite = spritePortraitBruteBLOODBORN;
        }
        else if (MainCharacter.chosenCommander == "CYCLON")
        {
            imgAdventureMainPortrait.sprite = spritePortraitBruteCYCLON;
        }
        else if (MainCharacter.chosenCommander == "MOLAK")
        {
            imgAdventureMainPortrait.sprite = spritePortraitBruteMOLAK;
        }
        else if (MainCharacter.chosenCommander == "MORCA")
        {
            imgAdventureMainPortrait.sprite = spritePortraitBruteMORCA;
        }
        else if (MainCharacter.chosenCommander == "SHEOG")
        {
            imgAdventureMainPortrait.sprite = spritePortraitBruteSHEOG;
        }
        else if (MainCharacter.chosenCommander == "ANETTE")
        {
            imgAdventureMainPortrait.sprite = spritePortraitBugANETTE;
        }
        else if (MainCharacter.chosenCommander == "BUTTERCUP")
        {
            imgAdventureMainPortrait.sprite = spritePortraitBugBUTTERCUP;
        }
        else if (MainCharacter.chosenCommander == "HIVERA")
        {
            imgAdventureMainPortrait.sprite = spritePortraitBugHIVERA;
        }
        else if (MainCharacter.chosenCommander == "MANTEL")
        {
            imgAdventureMainPortrait.sprite = spritePortraitBugMANTEL;
        }
        else if (MainCharacter.chosenCommander == "SECTON")
        {
            imgAdventureMainPortrait.sprite = spritePortraitBugSECTON;
        }
        else if (MainCharacter.chosenCommander == "DAMIAN")
        {
            imgAdventureMainPortrait.sprite = spritePortraitDemonDAMIAN;
        }
        else if (MainCharacter.chosenCommander == "KRAM")
        {
            imgAdventureMainPortrait.sprite = spritePortraitDemonKRAM;
        }
        else if (MainCharacter.chosenCommander == "NETH")
        {
            imgAdventureMainPortrait.sprite = spritePortraitDemonNETH;
        }
        else if (MainCharacter.chosenCommander == "SEARAIN")
        {
            imgAdventureMainPortrait.sprite = spritePortraitDemonSEARAIN;
        }
        else if (MainCharacter.chosenCommander == "SUCCUBAH")
        {
            imgAdventureMainPortrait.sprite = spritePortraitDemonSUCCUBAH;
        }
        else if (MainCharacter.chosenCommander == "CHEN")
        {
            imgAdventureMainPortrait.sprite = spritePortraitDragonCHEN;
        }
        else if (MainCharacter.chosenCommander == "ISIS")
        {
            imgAdventureMainPortrait.sprite = spritePortraitDragonISIS;
        }
        else if (MainCharacter.chosenCommander == "IXEN")
        {
            imgAdventureMainPortrait.sprite = spritePortraitDragonIXEN;
        }
        else if (MainCharacter.chosenCommander == "LOTH")
        {
            imgAdventureMainPortrait.sprite = spritePortraitDragonLOTH;
        }
        else if (MainCharacter.chosenCommander == "SANG")
        {
            imgAdventureMainPortrait.sprite = spritePortraitDragonSANG;
        }
        else if (MainCharacter.chosenCommander == "ARCLAN")
        {
            imgAdventureMainPortrait.sprite = spritePortraitElementalARCLAN;
        }
        else if (MainCharacter.chosenCommander == "HYDRANA")
        {
            imgAdventureMainPortrait.sprite = spritePortraitElementalHYDRANA;
        }
        else if (MainCharacter.chosenCommander == "IGNUS")
        {
            imgAdventureMainPortrait.sprite = spritePortraitElementalIGNUS;
        }
        else if (MainCharacter.chosenCommander == "MAG")
        {
            imgAdventureMainPortrait.sprite = spritePortraitElementalMAG;
        }
        else if (MainCharacter.chosenCommander == "WYD")
        {
            imgAdventureMainPortrait.sprite = spritePortraitElementalWYD;
        }
        else if (MainCharacter.chosenCommander == "ANGELUS")
        {
            imgAdventureMainPortrait.sprite = spritePortraitEtherealANGELUS;
        }
        else if (MainCharacter.chosenCommander == "NYX")
        {
            imgAdventureMainPortrait.sprite = spritePortraitEtherealNYX;
        }
        else if (MainCharacter.chosenCommander == "RAEL")
        {
            imgAdventureMainPortrait.sprite = spritePortraitEtherealRAEL;
        }
        else if (MainCharacter.chosenCommander == "THANATOS")
        {
            imgAdventureMainPortrait.sprite = spritePortraitEtherealTHANATOS;
        }
        else if (MainCharacter.chosenCommander == "THONIOS")
        {
            imgAdventureMainPortrait.sprite = spritePortraitEtherealTHONIOS;
        }
        else if (MainCharacter.chosenCommander == "DACAS")
        {
            imgAdventureMainPortrait.sprite = spritePortraitHumanDACAS;
        }
        else if (MainCharacter.chosenCommander == "FINEAS")
        {
            imgAdventureMainPortrait.sprite = spritePortraitHumanFINEAS;
        }
        else if (MainCharacter.chosenCommander == "FORLA")
        {
            imgAdventureMainPortrait.sprite = spritePortraitHumanFORLA;
        }
        else if (MainCharacter.chosenCommander == "HUNTER")
        {
            imgAdventureMainPortrait.sprite = spritePortraitHumanHUNTER;
        }
        else if (MainCharacter.chosenCommander == "TAMAZ")
        {
            imgAdventureMainPortrait.sprite = spritePortraitHumanTAMAZ;
        }
        else if (MainCharacter.chosenCommander == "CALIX")
        {
            imgAdventureMainPortrait.sprite = spritePortraitPhytoCALIX;
        }
        else if (MainCharacter.chosenCommander == "IRONBARK")
        {
            imgAdventureMainPortrait.sprite = spritePortraitPhytoIRONBARK;
        }
        else if (MainCharacter.chosenCommander == "OAKLAN")
        {
            imgAdventureMainPortrait.sprite = spritePortraitPhytoOAKLAN;
        }
        else if (MainCharacter.chosenCommander == "THORNET")
        {
            imgAdventureMainPortrait.sprite = spritePortraitPhytoTHORNET;
        }
        else if (MainCharacter.chosenCommander == "TULA")
        {
            imgAdventureMainPortrait.sprite = spritePortraitPhytoTULA;
        }
        else if (MainCharacter.chosenCommander == "AGAMET")
        {
            imgAdventureMainPortrait.sprite = spritePortraitReptilianAGAMET;
        }
        else if (MainCharacter.chosenCommander == "AMMIT")
        {
            imgAdventureMainPortrait.sprite = spritePortraitReptilianAMMIT;
        }
        else if (MainCharacter.chosenCommander == "MEDUSEL")
        {
            imgAdventureMainPortrait.sprite = spritePortraitReptilianMEDUSEL;
        }
        else if (MainCharacter.chosenCommander == "NAGA")
        {
            imgAdventureMainPortrait.sprite = spritePortraitReptilianNAGA;
        }
        else if (MainCharacter.chosenCommander == "THWARK")
        {
            imgAdventureMainPortrait.sprite = spritePortraitReptilianTHWARK;
        }
        else if (MainCharacter.chosenCommander == "LAMIA")
        {
            imgAdventureMainPortrait.sprite = spritePortraitUndeadLAMIA;
        }
        else if (MainCharacter.chosenCommander == "LICHEN")
        {
            imgAdventureMainPortrait.sprite = spritePortraitUndeadLICHEN;
        }
        else if (MainCharacter.chosenCommander == "QUIETUS")
        {
            imgAdventureMainPortrait.sprite = spritePortraitUndeadQUIETUS;
        }
        else if (MainCharacter.chosenCommander == "SWORT")
        {
            imgAdventureMainPortrait.sprite = spritePortraitUndeadSWORT;
        }
        else if (MainCharacter.chosenCommander == "ZOMM")
        {
            imgAdventureMainPortrait.sprite = spritePortraitUndeadZOMM;
        }
        else
        {
            imgAdventureMainPortrait.sprite = spritePortraitUndeadQUIETUS;       // Set default pic if none selected (eg. if jumped here from main)
        }
    }



    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
    //      FIRST EXPERIMENT WITH RANDOMLY GENERATING ALL ELEMENTS ON STRIP 1
    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- //

    /*
    public void Strip1SetElement()
    {
        strip1Element = GenerateRandomElement();

        if (strip1Element == "EARTH")                                // Change the element icon to the new element
        {
            imgStrip1Element.sprite = spriteElementalOrbEARTH;
        }
        else if (strip1Element == "METAL")
        {
            imgStrip1Element.sprite = spriteElementalOrbMETAL;
        }
        else if (strip1Element == "WATER")
        {
            imgStrip1Element.sprite = spriteElementalOrbWATER;
        }
        else if (strip1Element == "WOOD")
        {
            imgStrip1Element.sprite = spriteElementalOrbWOOD;
        }
        else
        {
            imgStrip1Element.sprite = spriteElementalOrbFIRE;
        }
    }

    public void Strip1SetFaction()
    {
        strip1Faction = GenerateRandomFaction();

        if (strip1Faction == "AMPHIBIAN")                                // Change the element icon to the new element
        {
            imgStrip1Faction.sprite = spriteRaceIconAMPHIBIAN;
        }
        else if (strip1Faction == "AQUATIC")
        {
            imgStrip1Faction.sprite = spriteRaceIconAQUATIC;
        }
        else if (strip1Faction == "AVIAN")
        {
            imgStrip1Faction.sprite = spriteRaceIconAVIAN;
        }
        else if (strip1Faction == "BEAST")
        {
            imgStrip1Faction.sprite = spriteRaceIconBEAST;
        }
        else if (strip1Faction == "BRUTE")
        {
            imgStrip1Faction.sprite = spriteRaceIconBRUTE;
        }
        else if (strip1Faction == "BUG")
        {
            imgStrip1Faction.sprite = spriteRaceIconBUG;
        }
        else if (strip1Faction == "DEMON")
        {
            imgStrip1Faction.sprite = spriteRaceIconDEMON;
        }
        else if (strip1Faction == "DRAGON")
        {
            imgStrip1Faction.sprite = spriteRaceIconDRAGON;
        }
        else if (strip1Faction == "ELEMENTAL")
        {
            imgStrip1Faction.sprite = spriteRaceIconELEMENTAL;
        }
        else if (strip1Faction == "ETHEREAL")
        {
            imgStrip1Faction.sprite = spriteRaceIconETHEREAL;
        }
        else if (strip1Faction == "HUMAN")
        {
            imgStrip1Faction.sprite = spriteRaceIconHUMAN;
        }
        else if (strip1Faction == "PHYTO")
        {
            imgStrip1Faction.sprite = spriteRaceIconPHYTO;
        }
        else if (strip1Faction == "REPTILIAN")
        {
            imgStrip1Faction.sprite = spriteRaceIconREPTILIAN;
        }
        else
        {
            imgStrip1Faction.sprite = spriteRaceIconUNDEAD;
        }
    }

    public void Strip1SetCost()
    {
        strip1Cost = GenerateRandomCostOrReward();

        if(strip1Cost == "STATISTICS")
        {
            imgStrip1Cost.sprite = spriteCostIconSTATISTICS;
        }
        else if(strip1Cost == "CREATURES")
        {
            imgStrip1Cost.sprite = spriteCostIconCREATURES;
        }
        else if (strip1Cost == "SPELLS")
        {
            imgStrip1Cost.sprite = spriteCostIconSPELLS;
        }
        else if (strip1Cost == "POTIONS")
        {
            imgStrip1Cost.sprite = spriteCostIconPOTIONS;
        }
        else if (strip1Cost == "SUPPORTS")
        {
            imgStrip1Cost.sprite = spriteCostIconSUPPORTS;
        }
        else if (strip1Cost == "AFFINITIES")
        {
            imgStrip1Cost.sprite = spriteCostIconAFFINITIES;
        }
        else
        {
            imgStrip1Cost.sprite = spriteCostIconGOLD;
        }
    }

    public void Strip1SetReward1()
    {
        strip1Reward1 = GenerateRandomCostOrReward();

        if (strip1Reward1 == "STATISTICS")
        {
            imgStrip1Reward1.sprite = spriteRewardIconSTATISTICS;
        }
        else if (strip1Reward1 == "CREATURES")
        {
            imgStrip1Reward1.sprite = spriteRewardIconCREATURES;
        }
        else if (strip1Reward1 == "SPELLS")
        {
            imgStrip1Reward1.sprite = spriteRewardIconSPELLS;
        }
        else if (strip1Reward1 == "POTIONS")
        {
            imgStrip1Reward1.sprite = spriteRewardIconPOTIONS;
        }
        else if (strip1Reward1 == "SUPPORTS")
        {
            imgStrip1Reward1.sprite = spriteRewardIconSUPPORTS;
        }
        else if (strip1Reward1 == "AFFINITIES")
        {
            imgStrip1Reward1.sprite = spriteRewardIconAFFINITIES;
        }
        else
        {
            imgStrip1Reward1.sprite = spriteRewardIconGOLD;
        }
    }

    public void Strip1SetReward2()
    {
        strip1Reward2 = GenerateRandomCostOrReward();

        if (strip1Reward2 == "STATISTICS")
        {
            imgStrip1Reward2.sprite = spriteRewardIconSTATISTICS;
        }
        else if (strip1Reward2 == "CREATURES")
        {
            imgStrip1Reward2.sprite = spriteRewardIconCREATURES;
        }
        else if (strip1Reward2 == "SPELLS")
        {
            imgStrip1Reward2.sprite = spriteRewardIconSPELLS;
        }
        else if (strip1Reward2 == "POTIONS")
        {
            imgStrip1Reward2.sprite = spriteRewardIconPOTIONS;
        }
        else if (strip1Reward2 == "SUPPORTS")
        {
            imgStrip1Reward2.sprite = spriteRewardIconSUPPORTS;
        }
        else if (strip1Reward2 == "AFFINITIES")
        {
            imgStrip1Reward2.sprite = spriteRewardIconAFFINITIES;
        }
        else
        {
            imgStrip1Reward2.sprite = spriteRewardIconGOLD;
        }
    }

    */

    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
    //      GENERIC TEST METHODS CALLED BY VARIOUS BUTTONS TO TEST VARIOUS THINGS WERE WORKING
    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- //

    /*

    public void Test()                                              // For testing purposes only
    {
        

        // SECONDARIES

        int DX = int.Parse(inputTestDX.text);
        int STR = int.Parse(inputTestSTR.text);
        int SPD = int.Parse(inputTestSPD.text);
        int INT = int.Parse(inputTestINT.text);

        UpdateDX(DX);
        UpdateSTR(STR);
        UpdateSPD(SPD);
        UpdateINT(INT);
        
        // VITALS

        int HPcurrent = int.Parse(inputTestHPcurrent.text);
        int HPmax = int.Parse(inputTestHPmax.text);
        UpdateHP(HPcurrent, HPmax);

        int SPcurrent = int.Parse(inputTestSPcurrent.text);
        int SPmax = int.Parse(inputTestSPmax.text);
        UpdateSP(SPcurrent, SPmax);

        int MPcurrent = int.Parse(inputTestMPcurrent.text);
        int MPmax = int.Parse(inputTestMPmax.text);
        UpdateMP(MPcurrent, MPmax); 
        
        // VITALS REGEN

        int HPregen = int.Parse(inputTestHPregen.text);
        int SPregen = int.Parse(inputTestSPregen.text);
        int MPregen = int.Parse(inputTestMPregen.text);

        UpdateHPregen(HPregen);
        UpdateSPregen(SPregen);
        UpdateMPregen(MPregen);

        // SHIELD REGEN

        int ShieldRegenEARTH = int.Parse(inputTestEARTHregen.text);
        int ShieldRegenMETAL = int.Parse(inputTestMETALregen.text);
        int ShieldRegenWATER = int.Parse(inputTestWATERregen.text);
        int ShieldRegenWOOD = int.Parse(inputTestWOODregen.text);
        int ShieldRegenFIRE = int.Parse(inputTestFIREregen.text);

        UpdateShieldEARTHRegen(ShieldRegenEARTH);
        UpdateShieldMETALRegen(ShieldRegenMETAL);
        UpdateShieldWATERRegen(ShieldRegenWATER);
        UpdateShieldWOODRegen(ShieldRegenWOOD);
        UpdateShieldFIRERegen(ShieldRegenFIRE);
                
        // DAMAGE

        int dmgEARTH = int.Parse(inputTestDamageEARTH.text);
        int dmgMETAL = int.Parse(inputTestDamageMETAL.text);
        int dmgWATER = int.Parse(inputTestDamageWATER.text);
        int dmgWOOD = int.Parse(inputTestDamageWOOD.text);
        int dmgFIRE = int.Parse(inputTestDamageFIRE.text);

        UpdateDamageEARTH(dmgEARTH);
        UpdateDamageMETAL(dmgMETAL);
        UpdateDamageWATER(dmgWATER);
        UpdateDamageWOOD(dmgWOOD);
        UpdateDamageFIRE(dmgFIRE); 

        

        // SHIELDS

        int shieldEARTH = int.Parse(inputTestShieldEARTH.text);
        int shieldMETAL = int.Parse(inputTestShieldMETAL.text);
        int shieldWATER = int.Parse(inputTestShieldWATER.text);
        int shieldWOOD = int.Parse(inputTestShieldWOOD.text);
        int shieldFIRE = int.Parse(inputTestShieldFIRE.text);

        UpdateShieldEARTH(shieldEARTH);
        UpdateShieldMETAL(shieldMETAL);
        UpdateShieldWATER(shieldWATER);
        UpdateShieldWOOD(shieldWOOD);
        UpdateShieldFIRE(shieldFIRE);

    }

    */

    /* 
   public void testAffinityColors()
   {
       MainCharacter.affinityBEAST = -50;
       txtTabAffinitiesBEAST.text = MainCharacter.affinityBEAST.ToString();
       SetAffinityColors();
   }
   */ 

    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
    //      PREVIOUS CHAIN CALCULATIONS - now superceded
    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- //

    /*
    
    public void UpdateChainElemental()                  // These are invoked from EventFunctionCaller.cs method UpdateChains(), during animation glowballElement
    {                                                   // Point is to update the chain labels with the new values while they are temporarily invisible
        Debug.Log("UpdateChainElementa()");
    
                                                      // Previously modified those values directly from here
        MainCharacter.chainLengthElemental += 1;
        MainCharacter.chainLengthTotal += 2;

        txtAdventureMainChainElemental.text = MainCharacter.chainLengthElemental.ToString();
        txtAdventureMainChainTotal.text = MainCharacter.chainLengthTotal.ToString();
         
        
                                                       // But now, run from other methods 

    }
   
    public void UpdateChainFaction()
    {
        Debug.Log("UpdateChainFaction()");
        MainCharacter.chainLengthRacial += 2;
        MainCharacter.chainLengthTotal += 2;

        txtAdventureMainChainAffinity.text = MainCharacter.chainLengthRacial.ToString();
        txtAdventureMainChainTotal.text = MainCharacter.chainLengthTotal.ToString();
    }

    public void UpdateChainCost()
    {
        Debug.Log("UpdateChainFaction()");
        MainCharacter.chainLengthRacial += 4;
        MainCharacter.chainLengthTotal += 4;

        txtAdventureMainChainCost.text = MainCharacter.chainLengthCost.ToString();
        txtAdventureMainChainTotal.text = MainCharacter.chainLengthTotal.ToString();
    }

    
    */


}
