﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventFunctionCaller : MonoBehaviour
{
    public GameObject myCanvas;                                         // Get the game object that has the needed script attached
    public AdventureMain myScript;                                      // Instantiate an object with the exact name of that .cs fIle

    public void ChangeChainIcons()
    {
        myCanvas = GameObject.Find("Canvas");                           // Reference the canvas
        myScript = myCanvas.GetComponent<AdventureMain>();              // Get the needed script attached to the canvas

        myScript.UpdateChainIcons();                                    // Run the method that transfers the Chain Icons from the Strip to the Commander Panel
    }

    public void NewTurn()
    {
        myCanvas = GameObject.Find("Canvas");                           // Reference the canvas
        myScript = myCanvas.GetComponent<AdventureMain>();              // Get the needed script attached to the canvas

        myScript.GoToNextTurn();                                        // Run the method that transfers the Chain Icons from the Strip to the Commander Panel
    }

    public void UpdateChains()                                          // Changes chain data to new values - Runs from Animation glowballElement while chain text is briefly hidden
    {
        myCanvas = GameObject.Find("Canvas");                           // Reference the canvas
        myScript = myCanvas.GetComponent<AdventureMain>();              // Get the needed script attached to the canvas

        myScript.CalculateChosenElements();
        myScript.CalculateChosenFactions();
        myScript.CalculateChosenCosts();
    }
}

