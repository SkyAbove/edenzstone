﻿// using System.Collections;
using UnityEngine;

public class PanelController : MonoBehaviour
{
	public GameObject panelMainMenu;                    // REFERENCE: various panels
    public GameObject panelCharacterCreation;
    public GameObject panelLoadGame;
    public GameObject panelCredits;
    public GameObject panelCharacterSetup;
    public GameObject panelAdventureAnnounce;
    public GameObject panelAdventureMain;
    public GameObject panelBattleAnnounce;
    public GameObject panelBattleMain;
    public GameObject panelBattleResults;

    private Animator animatorPanelMainMenu;				// REFERENCE: the Animator component on each panel
	private Animator animatorPanelCharacterCreation;											
	private Animator animatorPanelLoadGame;														
	private Animator animatorPanelCredits;
    private Animator animatorPanelCharacterSetup;
    private Animator animatorPanelAdventureAnnounce;
    private Animator animatorPanelAdventureMain;
    private Animator animatorPanelBattleAnnounce;
    private Animator animatorPanelBattleMain;
    private Animator animatorPanelBattleResults;

    


    void Start ()
	{
        AttachPanelAnimatorComponents();                                                // Get object references to all the Animator components attached to each panel
        DeactivateAllPanels();                                                          // Deactive every panel (to save resources)

        Invoke("PanelMainMenuSlideIn", 0.5f);                                           // Draw panelMainMenu after a brief delay
    } 
   
    public void AttachPanelAnimatorComponents()                         // ATTACH:  The Animator components for each panel
    {
        animatorPanelMainMenu           = panelMainMenu.GetComponent<Animator>();               
        animatorPanelCharacterCreation  = panelCharacterCreation.GetComponent<Animator>();
        animatorPanelLoadGame           = panelLoadGame.GetComponent<Animator>();
        animatorPanelCredits            = panelCredits.GetComponent<Animator>();
        animatorPanelCharacterSetup     = panelCharacterSetup.GetComponent<Animator>();
        animatorPanelAdventureAnnounce  = panelAdventureAnnounce.GetComponent<Animator>();
        animatorPanelAdventureMain      = panelAdventureMain.GetComponent<Animator>();
        animatorPanelBattleAnnounce     = panelBattleAnnounce.GetComponent<Animator>();
        animatorPanelBattleMain         = panelBattleMain.GetComponent<Animator>();
        animatorPanelBattleResults      = panelBattleResults.GetComponent<Animator>();
    }

    public void DeactivateAllPanels()                                   // Deactivate all panels to prevent accidental interactions
    {
        panelMainMenu.SetActive(false);		                                                    
        panelCharacterCreation.SetActive(false);
        panelLoadGame.SetActive(false);
        panelCredits.SetActive(false);
        panelCharacterSetup.SetActive(false);
        panelAdventureAnnounce.SetActive(false);
        panelAdventureMain.SetActive(false);
        panelBattleAnnounce.SetActive(false);
        panelBattleMain.SetActive(false);
        panelBattleResults.SetActive(false);
    }

    // ----- MAIN MENU -------------------------------------------------------------------------------------------------------------------------------------------------------------- //

        public void PanelMainMenuSlideIn()							        // Whenever panel [MAIN MENU] needs to be loaded
	    {
		    Debug.Log("MAIN MENU sliding in now.");

		    panelMainMenu.SetActive(true);									                // Activate the panel
		    animatorPanelMainMenu.Play("Panel_MainMenu_SlideIn");		                    // Play the panel's animation for sliding in
		    animatorPanelMainMenu.SetBool("CanPlay",false);                                 // Stop the animator after sliding in (so panel doesn't slide out immediately!)
	    }

	    public void PanelMainMenuSlideOut()						            // Whenever panel [MAIN MENU] needs to be unloaded
	    {
		    Debug.Log("MAIN MENU unloaded.");

		    animatorPanelMainMenu.SetBool("CanPlay", true);                                 // Triggers transition to animation state "Panel_MainMenu_SlideOut"
	    }


    // ----- CHARACTER CREATION ---------------------------------------------------------------------------------------------------------------------------------------------------- //

        public void PanelCharacterCreationSlideIn()				            // Whenever panel [CHARACTER CREATION] needs to be loaded
	    {
            Debug.Log("CHARACTER CREATION sliding in now.");

            panelCharacterCreation.SetActive(true);                                         // Activate the panel
            animatorPanelCharacterCreation.Play("Panel_CharacterCreation_SlideIn");         // Play the panel's animation for sliding in
        }

	    public void PanelCharacterCreationSlideOut()				        // Whenever panel [CHARACTER CREATION] needs to be unloaded
	    {
            Debug.Log("CHARACTER CREATION unloaded.");

            panelCharacterCreation.SetActive(true);
            animatorPanelCharacterCreation.Play("Panel_CharacterCreation_SlideOut");        // Play the panel's animation for sliding in
        }

        public void PanelCharacterCreationBack()                            // Whenever panel [CHARACTER CREATION] needs to be reloaded from the [CHARACTER SETUP] page
        {
            Debug.Log("CHARACTER CREATION sliding BACK in now.");

            panelCharacterCreation.SetActive(true);
            animatorPanelCharacterCreation.Play("Panel_CharacterCreation_Back");            // Play the panel's animation for sliding back in

        }

    // ----- LOAD GAME -------------------------------------------------------------------------------------------------------------------------------------------------------------- //

        public void PanelLoadGameSlideIn()							        // Whenever panel [LOAD GAME] needs to be loaded
	    {
            Debug.Log("LOAD GAME sliding in now.");

            panelLoadGame.SetActive(true);                                                  // Activate the panel
            animatorPanelLoadGame.Play("Panel_LoadGame_SlideIn");                           // Play the panel's animation for sliding in
        }
    
	    public void PanelLoadGameSlideOut()						            // Whenever panel [LOAD GAME] needs to be unloaded
	    {
            Debug.Log("LOAD GAME unloaded.");

            panelLoadGame.SetActive(true);                                                  // Activate the panel
            animatorPanelLoadGame.Play("Panel_LoadGame_SlideOut");                          // Play the panel's animation for sliding in
        }

    // ----- CREDITS -------------------------------------------------------------------------------------------------------------------------------------------------------------- //

        public void PanelCreditsSlideIn()							        // Whenever panel [CREDITS] needs to be loaded
	    {
            Debug.Log("CREDITS sliding in now.");

            panelCredits.SetActive(true);                                                   // Activate the panel
            animatorPanelCredits.Play("Panel_Credits_SlideIn");                             // Play the panel's animation for sliding in
        }

	    public void PanelCreditsSlideOut()							        // Whenever panel [CREDITS] needs to be unloaded
	    {
            Debug.Log("CREDITS unloaded.");

            panelCredits.SetActive(true);                                                   // Activate the panel
            animatorPanelCredits.Play("Panel_Credits_SlideOut");                            // Play the panel's animation for sliding out
        }

    // ----- CHARACTER SETUP -------------------------------------------------------------------------------------------------------------------------------------------------------------- //

        public void PanelCharacterSetupSlideIn()                           // Whenever panel [CHARACTER SETUP] needs to be loaded
        {
            Debug.Log("CHARACTER SETUP sliding in now.");

            panelCharacterSetup.SetActive(true);                                            // Activate the panel
            animatorPanelCharacterSetup.Play("Panel_CharacterSetup_SlideIn");               // Play the panel's animation for sliding in   
        }

        public void PanelCharacterSetupSlideOut()                          // Whenever panel [CHARACTER SETUP] needs to be unloaded
        {
            Debug.Log("CHARACTER SETUP unloaded.");

            panelCharacterSetup.SetActive(true);
            animatorPanelCharacterSetup.Play("Panel_CharacterSetup_SlideOut");              // Play the panel's animation for sliding in
        }

        public void PanelCharacterSetupBack()                              // Whenever panel [CHARACTER SETUP] needs to be unloaded when going back to the [CHARACTER CREATION] page
        {
            Debug.Log("CHARACTER SETUP sliding BACK out now.");

            panelCharacterSetup.SetActive(true);
            animatorPanelCharacterSetup.Play("Panel_CharacterSetup_Back");                  // Play the panel's animation for sliding in
        }

    // ----- ADVENTURE ANNOUNCE -------------------------------------------------------------------------------------------------------------------------------------------------------- //

        public void PanelAdventureAnnounceSlideIn()                             // Whenever panel [ADVENTURE ANNOUNCE] needs to be loaded
        {
            Debug.Log("ADVENTURE ANNOUNCE sliding in now.");

            panelAdventureAnnounce.SetActive(true);                                         // Activate the panel
            animatorPanelAdventureAnnounce.Play("Panel_AdventureAnnounce_SlideIn");         // Play the panel's animation for sliding in

            Invoke("PanelAdventureAnnounceSlideOut", 3.0f);                                 // Play the Slide Out animation after a brief delay
            Invoke("PanelAdventureMainSlideIn", 3.0f);                                      // Play the Slide Out animation after a brief delay
        }

        public void PanelAdventureAnnounceSlideOut()                            // Whenever panel [ADVENTURE ANNOUNCE] needs to be unloaded
        {
            Debug.Log("ADVENTURE ANNOUNCE unloaded.");

            panelAdventureAnnounce.SetActive(true);                                         // Activate the panel
            animatorPanelAdventureAnnounce.Play("Panel_AdventureAnnounce_SlideOut");        // Play the panel's animation for sliding out
        }

    // ----- ADVENTURE MAIN -------------------------------------------------------------------------------------------------------------------------------------------------------------- //

        public void PanelAdventureMainSlideIn()                             // Whenever panel [ADVENTURE MAIN] needs to be loaded
        {
            Debug.Log("ADVENTURE MAIN sliding in now.");

            panelAdventureMain.SetActive(true);                                             // Activate the panel
            animatorPanelAdventureMain.Play("Panel_AdventureMain_SlideIn");                 // Play the panel's animation for sliding in
        }

        public void PanelAdventureMainSlideOut()                            // Whenever panel [ADVENTURE MAIN] needs to be unloaded
        {
            Debug.Log("ADVENTURE MAIN unloaded.");

            panelAdventureMain.SetActive(true);                                             // Activate the panel
            animatorPanelAdventureMain.Play("Panel_AdventureMain_SlideOut");                // Play the panel's animation for sliding out
        }

    // ----- BATTLE ANNOUNCE -------------------------------------------------------------------------------------------------------------------------------------------------------- //

        public void PanelBattleAnnounceSlideIn()                                // Whenever panel [BATTLE ANNOUNCE] needs to be loaded
        {
            Debug.Log("BATTLE ANNOUNCE sliding in now.");

            panelBattleAnnounce.SetActive(true);                                            // Activate the panel
            animatorPanelBattleAnnounce.Play("Panel_BattleAnnounce_SlideIn");               // Play the panel's animation for sliding in

            Invoke("PanelBattleAnnounceSlideOut", 2.0f);                                    // Play the Slide Out animation after a brief delay
            Invoke("PanelBattleMainSlideIn", 2.0f);                                         // Play the Slide Out animation after a brief delay
        }

        public void PanelBattleAnnounceSlideOut()                               // Whenever panel [BATTLE ANNOUNCE] needs to be unloaded
        {
            Debug.Log("BATTLE ANNOUNCE unloaded.");

            panelBattleAnnounce.SetActive(true);                                            // Activate the panel
            animatorPanelBattleAnnounce.Play("Panel_BattleAnnounce_SlideOut");              // Play the panel's animation for sliding out
        }

    // ----- BATTLE MAIN -------------------------------------------------------------------------------------------------------------------------------------------------------------- //

        public void PanelBattleMainSlideIn()                             // Whenever panel [BATTLE MAIN] needs to be loaded
        {
            Debug.Log("BATTLE MAIN sliding in now.");

            panelBattleMain.SetActive(true);                                                // Activate the panel
            animatorPanelBattleMain.Play("Panel_BattleMain_SlideIn");                       // Play the panel's animation for sliding in
        }

        public void PanelBattleMainSlideOut()                            // Whenever panel [BATTLE MAIN] needs to be unloaded
        {
            Debug.Log("BATTLE MAIN unloaded.");

            panelBattleMain.SetActive(true);                                                // Activate the panel
            animatorPanelBattleMain.Play("Panel_BattleMain_SlideOut");                      // Play the panel's animation for sliding out
        }

    // ----- BATTLE RESULTS -------------------------------------------------------------------------------------------------------------------------------------------------------------- //

        public void PanelBattleResultsSlideIn()                             // Whenever panel [BATTLE RESULTS] needs to be loaded
        {
            Debug.Log("BATTLE RESULTS sliding in now.");

            panelBattleResults.SetActive(true);                                                // Activate the panel
            animatorPanelBattleResults.Play("Panel_BattleResults_SlideIn");                    // Play the panel's animation for sliding in
        }

        public void PanelBattleResultsSlideOut()                            // Whenever panel [BATTLE RESULTS] needs to be unloaded
        {
            Debug.Log("BATTLE RESULTS unloaded.");

            panelBattleResults.SetActive(true);                                                // Activate the panel
            animatorPanelBattleResults.Play("Panel_BattleResults_SlideOut");                   // Play the panel's animation for sliding out
        }

    // ----- EXIT -------------------------------------------------------------------------------------------------------------------------------------------------------------- //

        public void ExitGame()										            // When [MAIN MENU] > [EXIT] button is clicked
	    {
		    Debug.Log("EXIT selected.");
		    Application.Quit();
	    }
}
