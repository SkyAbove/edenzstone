﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterCreation : MonoBehaviour
{
    public GameObject panelPortraitGroupAmphibian;              // REFERENCE: the various portrait groups
    public GameObject panelPortraitGroupAquatic;
    public GameObject panelPortraitGroupAvian;
    public GameObject panelPortraitGroupBeast;
    public GameObject panelPortraitGroupBrute;
    public GameObject panelPortraitGroupBug;
    public GameObject panelPortraitGroupDemon;
    public GameObject panelPortraitGroupDragon;
    public GameObject panelPortraitGroupElemental;
    public GameObject panelPortraitGroupEthereal;
    public GameObject panelPortraitGroupHuman;
    public GameObject panelPortraitGroupPhyto;
    public GameObject panelPortraitGroupReptilian;
    public GameObject panelPortraitGroupUndead;

    public Text txtVitalPoints;                                 // REFERENCE: Vital points remaining
    public Text txtSecondaryPoints;                             // REFERENCE: Secondary points remaining
    public Text txtMaxHP;
    public Text txtMaxSP;
    public Text txtMaxMP;
    public Text txtDX;
    public Text txtSTR;
    public Text txtSPD;
    public Text txtINT;

    private int physicalSliderValueHP = 0;                       // The current physical values for HP / SP / MP / DX / STR / SPD / INT as read from their sliders
    private int physicalSliderValueSP = 0;                                      
    private int physicalSliderValueMP = 0;
    private int physicalSliderValueDX  = 0;                      
    private int physicalSliderValueSTR = 0;
    private int physicalSliderValueSPD = 0;
    private int physicalSliderValueINT = 0;

    private int sliderHPFreezePoint;                            // The values beyond which the slider values cannot increase, as there are no points left to allocated
    private int sliderSPFreezePoint;
    private int sliderMPFreezePoint;
    private int sliderDXFreezePoint;
    private int sliderSTRFreezePoint;
    private int sliderSPDFreezePoint;
    private int sliderINTFreezePoint;

    public Slider sliderHP;                                     // REFERENCE: The sliders for HP / SP / MP / DX / STR / SPD / INT
    public Slider sliderSP;
    public Slider sliderMP;
    public Slider sliderDX;                                     
    public Slider sliderSTR;
    public Slider sliderSPD;
    public Slider sliderINT;

    public InputField inputPlayerName;                          // REFERENCE: The custom commander name entered by the player
    public Text txtName;                                        // REFERENCE: The character name shown in panelCharacterSetup
    public Text txtDescriptionHeader;                           // REFERENCE: The heading for the description under name, in panelCharacterSetup
    public Text txtDescription;                                 // REFERENCE: The description under name, in panelCharacterSetup

    public Image imgCharacterSetupPortrait;                     // REFERENCE: Image that holds the character portrait in panelCharacterSetup

    public Button btnAcceptCreation;                            // REFERENCE: The "ACCEPT" button on panelCharacterCreation
    public Button btnAcceptSetup;                               // REFERENCE: The "ACCEPT" button on panelCharacterSetup

    public Sprite spritePortraitAmphibianAMPHORA;               // REFERENCE: Commander portrait sprites that will go into Image imgCharacterSetupPortrait
    public Sprite spritePortraitAmphibianCROACK;
    public Sprite spritePortraitAmphibianGULLET;
    public Sprite spritePortraitAmphibianROG;
    public Sprite spritePortraitAmphibianSALLA;

    public Sprite spritePortraitAquaticBLUB;
    public Sprite spritePortraitAquaticMERA;
    public Sprite spritePortraitAquaticOCTUS;
    public Sprite spritePortraitAquaticSHAREK;
    public Sprite spritePortraitAquaticTHULLU;

    public Sprite spritePortraitAvianBILL;
    public Sprite spritePortraitAvianGIBBS;
    public Sprite spritePortraitAvianHARPETTE;
    public Sprite spritePortraitAvianIZORA;
    public Sprite spritePortraitAvianYRAG;

    public Sprite spritePortraitBeastBLORN;
    public Sprite spritePortraitBeastGARLAND;
    public Sprite spritePortraitBeastLIONEL;
    public Sprite spritePortraitBeastMONKLEY;
    public Sprite spritePortraitBeastPIGLET;

    public Sprite spritePortraitBruteBLOODBORN;
    public Sprite spritePortraitBruteCYCLON;
    public Sprite spritePortraitBruteMOLAK;
    public Sprite spritePortraitBruteMORCA;
    public Sprite spritePortraitBruteSHEOG;

    public Sprite spritePortraitBugANETTE;
    public Sprite spritePortraitBugBUTTERCUP;
    public Sprite spritePortraitBugHIVERA;
    public Sprite spritePortraitBugMANTEL;
    public Sprite spritePortraitBugSECTON;

    public Sprite spritePortraitDemonDAMIAN;
    public Sprite spritePortraitDemonKRAM;
    public Sprite spritePortraitDemonNETH;
    public Sprite spritePortraitDemonSEARAIN;
    public Sprite spritePortraitDemonSUCCUBAH;

    public Sprite spritePortraitDragonCHEN;
    public Sprite spritePortraitDragonISIS;
    public Sprite spritePortraitDragonIXEN;
    public Sprite spritePortraitDragonLOTH;
    public Sprite spritePortraitDragonSANG;

    public Sprite spritePortraitElementalARCLAN;
    public Sprite spritePortraitElementalHYDRANA;
    public Sprite spritePortraitElementalIGNUS;
    public Sprite spritePortraitElementalMAG;
    public Sprite spritePortraitElementalWYD;

    public Sprite spritePortraitEtherealANGELUS;
    public Sprite spritePortraitEtherealNYX;
    public Sprite spritePortraitEtherealRAEL;
    public Sprite spritePortraitEtherealTHANATOS;
    public Sprite spritePortraitEtherealTHONIOS;

    public Sprite spritePortraitHumanDACAS;
    public Sprite spritePortraitHumanFINEAS;
    public Sprite spritePortraitHumanFORLA;
    public Sprite spritePortraitHumanHUNTER;
    public Sprite spritePortraitHumanTAMAZ;

    public Sprite spritePortraitPhytoCALIX;
    public Sprite spritePortraitPhytoIRONBARK;
    public Sprite spritePortraitPhytoOAKLAN;
    public Sprite spritePortraitPhytoTHORNET;
    public Sprite spritePortraitPhytoTULA;

    public Sprite spritePortraitReptilianAGAMET;
    public Sprite spritePortraitReptilianAMMIT;
    public Sprite spritePortraitReptilianMEDUSEL;
    public Sprite spritePortraitReptilianNAGA;
    public Sprite spritePortraitReptilianTHWARK;

    public Sprite spritePortraitUndeadLAMIA;
    public Sprite spritePortraitUndeadLICHEN;
    public Sprite spritePortraitUndeadQUIETUS;
    public Sprite spritePortraitUndeadSWORT;
    public Sprite spritePortraitUndeadZOMM;


    void Start ()
    {
        resetPanelCharacterSetup();
                                                             // Show the default message under the character name in panelCharacterSetup
    }
	
	void Update ()
    {
        if(MainCharacter.currentVitalPoints == 0 && MainCharacter.currentSecondaryPoints == 0)                // Vitality Points = 0 and Secondary Points = 0, activate ACCEPT button to proceed
        {
            btnAcceptSetup.interactable = true;

        }
        else
        {
            btnAcceptSetup.interactable = false;
        }
		
	}

    public void resetPanelCharacterSetup()                  // Attach to button NewGame in MainMenu, to ensure that panel is correctly reset for new game
    {
        Globals.currentPortraitGroup = "groupCommanderPortraits_AMPHIBIAN";             // Set the currently active group of portraits in panelCharacterCreation
        DeactivateAllPortraitGroups();                                                  // Deactivate all portrait groups in panelCharacterCreation
        panelPortraitGroupAmphibian.SetActive(true);                                    // Show default portrait group: AMPHIBIANS

        btnAcceptSetup.interactable = false;                                            // Disable the "ACCEPT" button until player has selected a portrait

        txtVitalPoints.text = MainCharacter.maxVitalPoints.ToString();                  // Update GUI with starting values for Vital Points
        txtSecondaryPoints.text = MainCharacter.maxSecondaryPoints.ToString();          // And Secondary Points

        DisplayDefaultMessage();                                                        // Below portrait, telling player to drag sliders

        txtMaxHP.text = MainCharacter.HPmax.ToString();                                 // Update GUI with starting values for HP / SP / MP / DX / STR / SPD / INT
        txtMaxSP.text = MainCharacter.SPmax.ToString();
        txtMaxMP.text = MainCharacter.MPmax.ToString();

        txtDX.text = MainCharacter.DX.ToString();
        txtSTR.text = MainCharacter.STR.ToString();
        txtSPD.text = MainCharacter.SPD.ToString();
        txtINT.text = MainCharacter.INT.ToString();

        physicalSliderValueHP = 0;                                                      // Reset physical slider handles to 0
        physicalSliderValueSP = 0;
        physicalSliderValueMP = 0;
        physicalSliderValueDX = 0;
        physicalSliderValueSTR = 0;
        physicalSliderValueSPD = 0;
        physicalSliderValueINT = 0;
}

    public void EnableAcceptButton()                                            // Enable the "ACCEPT" button only after the player has selected a portrait
    {
        btnAcceptCreation.interactable = true;                                  
    }

    public void movedSliderHP(float sliderValue)                         // Called onValueChanged() for sliderHP
    {
        physicalSliderValueHP = (int)sliderValue;                       // Get the current physical value of the HP slider
        setCurrentVitalPoints();                                        // Reduce remaining Vital Points by the value of this HP slider

        if (MainCharacter.currentVitalPoints >= 0)                                    // As long as there are still Vital Points remaining
        {
            MainCharacter.HPmax = MainCharacter.HPstartValue + physicalSliderValueHP;                // Increase the current HP by the HP slider value         
            sliderHPFreezePoint = physicalSliderValueHP;                // Remember what the current value of the MP slider is
            MainCharacter.HPcurrent = MainCharacter.HPmax;              // Update current HP to same as max HP

            if (MainCharacter.currentVitalPoints == 0)                                 // If the Vital Points reach 0
            {
                sliderHPFreezePoint = physicalSliderValueHP;            // Remember what the current values of all the sliders are
                sliderSPFreezePoint = physicalSliderValueSP;
                sliderMPFreezePoint = physicalSliderValueMP;
            }
        }
        else if (MainCharacter.currentVitalPoints < 0)                                // If the handle is still dragged beyond this point
        {
            MainCharacter.currentVitalPoints = 0;                                     // Force the current Vital Points to remain at 0 (no negatives allowed)
            txtVitalPoints.text = "0";                                  // Make sure the GUI still says 0 Vital Points
            sliderHP.value = sliderHPFreezePoint;                       // Keep the HP slider at the Freeze Point
        }      

        txtMaxHP.text = MainCharacter.HPmax.ToString();                           // Update GUI with new HP value     
    }

    public void movedSliderSP(float sliderValue)                         // Called onValueChanged() for sliderSP
    {
        physicalSliderValueSP = (int)sliderValue;                       // Get the current physical value of the SP slider
        setCurrentVitalPoints();                                        // Reduce remaining Vital Points by the value of this SP slider

        if (MainCharacter.currentVitalPoints >= 0)                                    // As long as there are still Vital Points remaining
        {
            MainCharacter.SPmax = MainCharacter.SPstartValue + physicalSliderValueSP;                // Increase the current SP by the SP slider value         
            sliderSPFreezePoint = physicalSliderValueSP;                // Remember what the current value of the MP slider is
            MainCharacter.SPcurrent = MainCharacter.SPmax;              // Update current SP to same as max SP

            if (MainCharacter.currentVitalPoints == 0)                                // If the Vital Points reach 0
            {
                sliderSPFreezePoint = physicalSliderValueSP;            // Remember what the current value of the SP slider is
                sliderHPFreezePoint = physicalSliderValueHP;
                sliderMPFreezePoint = physicalSliderValueMP;
            }
        }
        else if (MainCharacter.currentVitalPoints < 0)                                // If the handle is still dragged beyond this point
        {
            MainCharacter.currentVitalPoints = 0;                                     // Force the current Vital Points to remain at 0 (no negatives allowed)
            txtVitalPoints.text = "0";                                  // Make sure the GUI still says 0 Vital Points
            sliderSP.value = sliderSPFreezePoint;                       // Keep the SP slider at the Freeze Point
        }

        txtMaxSP.text = MainCharacter.SPmax.ToString();                           // Update GUI with new SP value     
    }

    public void movedSliderMP(float sliderValue)                         // Called onValueChanged() for sliderMP
    {
        physicalSliderValueMP = (int)sliderValue;                       // Get the current physical value of the MP slider
        setCurrentVitalPoints();                                        // Reduce remaining Vital Points by the value of this MP slider

        if (MainCharacter.currentVitalPoints >= 0)                                    // As long as there are still Vital Points remaining
        {
            MainCharacter.MPmax = MainCharacter.MPstartValue + physicalSliderValueMP;                // Increase the current MP by the MP slider value         
            sliderMPFreezePoint = physicalSliderValueMP;                // Remember what the current value of the MP slider is
            MainCharacter.MPcurrent = MainCharacter.MPmax;              // Update current MP to same as max MP 

            if (MainCharacter.currentVitalPoints == 0)                                // If the Vital Points reach 0
            {
                sliderMPFreezePoint = physicalSliderValueMP;            // Remember what the current value of the MP slider is
                sliderHPFreezePoint = physicalSliderValueHP;
                sliderSPFreezePoint = physicalSliderValueSP;
            }
        }
        else if (MainCharacter.currentVitalPoints < 0)                                // If the handle is still dragged beyond this point
        {
            MainCharacter.currentVitalPoints = 0;                                     // Force the current Vital Points to remain at 0 (no negatives allowed)
            txtVitalPoints.text = "0";                                  // Make sure the GUI still says 0 Vital Points
            sliderMP.value = sliderMPFreezePoint;                       // Keep the MP slider at the Freeze Point
        }

        txtMaxMP.text = MainCharacter.MPmax.ToString();                           // Update GUI with new MP value     
    }

    public void movedSliderDX(float sliderValue)                            // Called onValueChanged() for sliderDX
    {
        physicalSliderValueDX = (int)sliderValue;                           // Get the current physical value of the DX slider
        setCurrentSecondaryPoints();                                        // Reduce remaining Secondary Points by the value of this DX slider

        if (MainCharacter.currentSecondaryPoints >= 0)                                    // As long as there are still Secondary Points remaining
        {
            MainCharacter.DX = MainCharacter.DXstartValue + physicalSliderValueDX;                    // Increase the current DX by the DX slider value         
            sliderDXFreezePoint = physicalSliderValueDX;                    // Remember what the current value of the DX slider is

            if (MainCharacter.currentSecondaryPoints == 0)                                // If the Secondary Points reach 0
            {
                sliderDXFreezePoint = physicalSliderValueDX;                // Remember what the current value of the DX / STR / SPD / INT sliders are at this time
                sliderSTRFreezePoint = physicalSliderValueSTR;
                sliderSPDFreezePoint = physicalSliderValueSPD;
                sliderINTFreezePoint = physicalSliderValueINT;
            }
        }
        else if (MainCharacter.currentSecondaryPoints < 0)                                // If the handle is still dragged beyond this point
        {
            MainCharacter.currentSecondaryPoints = 0;                                     // Force the current Secondary Points to remain at 0 (no negatives allowed)
            txtSecondaryPoints.text = "0";                                  // Make sure the GUI still says 0 Secondary Points
            sliderDX.value = sliderDXFreezePoint;                           // Keep the DX slider at the Freeze Point
        }

        txtDX.text = MainCharacter.DX.ToString();                                  // Update GUI with new DX value     
    }

    public void movedSliderSTR(float sliderValue)                            // Called onValueChanged() for sliderSTR
    {
        physicalSliderValueSTR = (int)sliderValue;                          // Get the current physical value of the STR slider
        setCurrentSecondaryPoints();                                        // Reduce remaining Secondary Points by the value of this STR slider

        if (MainCharacter.currentSecondaryPoints >= 0)                                    // As long as there are still Secondary Points remaining
        {
            MainCharacter.STR = MainCharacter.STRstartValue + physicalSliderValueSTR;                 // Increase the current STR by the STR slider value         
            sliderSTRFreezePoint = physicalSliderValueSTR;                  // Remember what the current value of the STR slider is

            if (MainCharacter.currentSecondaryPoints == 0)                                // If the Secondary Points reach 0
            {
                sliderDXFreezePoint = physicalSliderValueDX;                // Remember what the current value of the DX / STR / SPD / INT sliders are at this time
                sliderSTRFreezePoint = physicalSliderValueSTR;
                sliderSPDFreezePoint = physicalSliderValueSPD;
                sliderINTFreezePoint = physicalSliderValueINT;
            }
        }
        else if (MainCharacter.currentSecondaryPoints < 0)                                // If the handle is still dragged beyond this point
        {
            MainCharacter.currentSecondaryPoints = 0;                                     // Force the current Secondary Points to remain at 0 (no negatives allowed)
            txtSecondaryPoints.text = "0";                                  // Make sure the GUI still says 0 Secondary Points
            sliderSTR.value = sliderSTRFreezePoint;                         // Keep the STR slider at the Freeze Point
        }

        txtSTR.text = MainCharacter.STR.ToString();                                // Update GUI with new STR value     
    }

    public void movedSliderSPD(float sliderValue)                            // Called onValueChanged() for sliderSPD
    {
        physicalSliderValueSPD = (int)sliderValue;                          // Get the current physical value of the SPD slider
        setCurrentSecondaryPoints();                                        // Reduce remaining Secondary Points by the value of this SPD slider

        if (MainCharacter.currentSecondaryPoints >= 0)                                    // As long as there are still Secondary Points remaining
        {
            MainCharacter.SPD = MainCharacter.SPDstartValue + physicalSliderValueSPD;                 // Increase the current SPD by the SPD slider value         
            sliderSPDFreezePoint = physicalSliderValueSPD;                  // Remember what the current value of the SPD slider is

            if (MainCharacter.currentSecondaryPoints == 0)                                // If the Secondary Points reach 0
            {
                sliderDXFreezePoint = physicalSliderValueDX;                // Remember what the current value of the DX / STR / SPD / INT sliders are at this time
                sliderSTRFreezePoint = physicalSliderValueSTR;
                sliderSPDFreezePoint = physicalSliderValueSPD;
                sliderINTFreezePoint = physicalSliderValueINT;
            }
        }
        else if (MainCharacter.currentSecondaryPoints < 0)                                // If the handle is still dragged beyond this point
        {
            MainCharacter.currentSecondaryPoints = 0;                                     // Force the current Secondary Points to remain at 0 (no negatives allowed)
            txtSecondaryPoints.text = "0";                                  // Make sure the GUI still says 0 Secondary Points
            sliderSPD.value = sliderSPDFreezePoint;                         // Keep the SPD slider at the Freeze Point
        }

        txtSPD.text = MainCharacter.SPD.ToString();                                // Update GUI with new SPD value     
    }

    public void movedSliderINT(float sliderValue)                            // Called onValueChanged() for sliderINT 
    {
        physicalSliderValueINT = (int)sliderValue;                          // Get the current physical value of the INT slider
        setCurrentSecondaryPoints();                                        // Reduce remaining Secondary Points by the value of this INT slider

        if (MainCharacter.currentSecondaryPoints >= 0)                                    // As long as there are still Secondary Points remaining
        {
            MainCharacter.INT = MainCharacter.INTstartValue + physicalSliderValueINT;                 // Increase the current INT by the INT slider value         
            sliderINTFreezePoint = physicalSliderValueINT;                  // Remember what the current value of the INT slider is

            if (MainCharacter.currentSecondaryPoints == 0)                                // If the Secondary Points reach 0
            {
                sliderDXFreezePoint = physicalSliderValueDX;                // Remember what the current value of the DX / STR / SPD / INT sliders are at this time
                sliderSTRFreezePoint = physicalSliderValueSTR;
                sliderSPDFreezePoint = physicalSliderValueSPD;
                sliderINTFreezePoint = physicalSliderValueINT;
            }
        }
        else if (MainCharacter.currentSecondaryPoints < 0)                                // If the handle is still dragged beyond this point
        {
            MainCharacter.currentSecondaryPoints = 0;                                     // Force the current Secondary Points to remain at 0 (no negatives allowed)
            txtSecondaryPoints.text = "0";                                  // Make sure the GUI still says 0 Secondary Points
            sliderINT.value = sliderINTFreezePoint;                         // Keep the INT slider at the Freeze Point
        }

        txtINT.text = MainCharacter.INT.ToString();                                // Update GUI with new INT value     
    }

    public void setCurrentVitalPoints()
    {
        MainCharacter.currentVitalPoints = MainCharacter.maxVitalPoints - physicalSliderValueHP - physicalSliderValueSP - physicalSliderValueMP;        // Remove the current HP amount from the total Vital Points available
        txtVitalPoints.text = MainCharacter.currentVitalPoints.ToString();                                                                // Update GUI with remaining Vital Points
    }

    public void setCurrentSecondaryPoints()
    {
        MainCharacter.currentSecondaryPoints = MainCharacter.maxSecondaryPoints - physicalSliderValueDX - physicalSliderValueSTR - physicalSliderValueSPD - physicalSliderValueINT;     // Remove the current HP amount from the total Vital Points available
        txtSecondaryPoints.text = MainCharacter.currentSecondaryPoints.ToString();                                                                                                // Update GUI with remaining Vital Points
    }

    public void DeactivateAllPortraitGroups()                                   // Deactivate all panels to prevent accidental interactions
    {
        panelPortraitGroupAmphibian.SetActive(false);
        panelPortraitGroupAquatic.SetActive(false);
        panelPortraitGroupAvian.SetActive(false);
        panelPortraitGroupBeast.SetActive(false);
        panelPortraitGroupBrute.SetActive(false);
        panelPortraitGroupBug.SetActive(false);
        panelPortraitGroupDemon.SetActive(false);
        panelPortraitGroupDragon.SetActive(false);
        panelPortraitGroupElemental.SetActive(false);
        panelPortraitGroupEthereal.SetActive(false);
        panelPortraitGroupHuman.SetActive(false);
        panelPortraitGroupPhyto.SetActive(false);
        panelPortraitGroupReptilian.SetActive(false);
        panelPortraitGroupUndead.SetActive(false);
    }

    public void NextPortraitGroup()
    {
        if (Globals.currentPortraitGroup == "groupCommanderPortraits_AMPHIBIAN")
        {
            Debug.Log("Current portrait group is AMPHIBIAN. Now changing to AQUATIC.");
            panelPortraitGroupAmphibian.SetActive(false);
            panelPortraitGroupAquatic.SetActive(true);
            Globals.currentPortraitGroup = "groupCommanderPortraits_AQUATIC";
        }
        else if (Globals.currentPortraitGroup == "groupCommanderPortraits_AQUATIC")
        {
            Debug.Log("Current portrait group is AQUATIC. Now changing to AVIAN.");
            panelPortraitGroupAquatic.SetActive(false);
            panelPortraitGroupAvian.SetActive(true);
            Globals.currentPortraitGroup = "groupCommanderPortraits_AVIAN";
        }
        else if (Globals.currentPortraitGroup == "groupCommanderPortraits_AVIAN")
        {
            Debug.Log("Current portrait group is AVIAN. Now changing to BEAST.");
            panelPortraitGroupAvian.SetActive(false);
            panelPortraitGroupBeast.SetActive(true);
            Globals.currentPortraitGroup = "groupCommanderPortraits_BEAST";
        }
        else if (Globals.currentPortraitGroup == "groupCommanderPortraits_BEAST")
        {
            Debug.Log("Current portrait group is BEAST. Now changing to BRUTE.");
            panelPortraitGroupBeast.SetActive(false);
            panelPortraitGroupBrute.SetActive(true);
            Globals.currentPortraitGroup = "groupCommanderPortraits_BRUTE";
        }
        else if (Globals.currentPortraitGroup == "groupCommanderPortraits_BRUTE")
        {
            Debug.Log("Current portrait group is BRUTE. Now changing to BUG.");
            panelPortraitGroupBrute.SetActive(false);
            panelPortraitGroupBug.SetActive(true);
            Globals.currentPortraitGroup = "groupCommanderPortraits_BUG";
        }
        else if (Globals.currentPortraitGroup == "groupCommanderPortraits_BUG")
        {
            Debug.Log("Current portrait group is BUG. Now changing to DEMON.");
            panelPortraitGroupBug.SetActive(false);
            panelPortraitGroupDemon.SetActive(true);
            Globals.currentPortraitGroup = "groupCommanderPortraits_DEMON";
        }
        else if (Globals.currentPortraitGroup == "groupCommanderPortraits_DEMON")
        {
            Debug.Log("Current portrait group is DEMON. Now changing to DRAGON.");
            panelPortraitGroupDemon.SetActive(false);
            panelPortraitGroupDragon.SetActive(true);
            Globals.currentPortraitGroup = "groupCommanderPortraits_DRAGON";
        }
        else if (Globals.currentPortraitGroup == "groupCommanderPortraits_DRAGON")
        {
            Debug.Log("Current portrait group is DRAGON. Now changing to ELEMENTAL.");
            panelPortraitGroupDragon.SetActive(false);
            panelPortraitGroupElemental.SetActive(true);
            Globals.currentPortraitGroup = "groupCommanderPortraits_ELEMENTAL";
        }
        else if (Globals.currentPortraitGroup == "groupCommanderPortraits_ELEMENTAL")
        {
            Debug.Log("Current portrait group is ELEMENTAL. Now changing to ETHEREAL.");
            panelPortraitGroupElemental.SetActive(false);
            panelPortraitGroupEthereal.SetActive(true);
            Globals.currentPortraitGroup = "groupCommanderPortraits_ETHEREAL";
        }
        else if (Globals.currentPortraitGroup == "groupCommanderPortraits_ETHEREAL")
        {
            Debug.Log("Current portrait group is ETHEREAL. Now changing to HUMAN.");
            panelPortraitGroupEthereal.SetActive(false);
            panelPortraitGroupHuman.SetActive(true);
            Globals.currentPortraitGroup = "groupCommanderPortraits_HUMAN";
        }
        else if (Globals.currentPortraitGroup == "groupCommanderPortraits_HUMAN")
        {
            Debug.Log("Current portrait group is HUMAN. Now changing to PHYTO.");
            panelPortraitGroupHuman.SetActive(false);
            panelPortraitGroupPhyto.SetActive(true);
            Globals.currentPortraitGroup = "groupCommanderPortraits_PHYTO";
        }
        else if (Globals.currentPortraitGroup == "groupCommanderPortraits_PHYTO")
        {
            Debug.Log("Current portrait group is PHYTO. Now changing to REPTILIAN.");
            panelPortraitGroupPhyto.SetActive(false);
            panelPortraitGroupReptilian.SetActive(true);
            Globals.currentPortraitGroup = "groupCommanderPortraits_REPTILIAN";
        }
        else if (Globals.currentPortraitGroup == "groupCommanderPortraits_REPTILIAN")
        {
            Debug.Log("Current portrait group is REPTILIAN. Now changing to UNDEAD.");
            panelPortraitGroupReptilian.SetActive(false);
            panelPortraitGroupUndead.SetActive(true);
            Globals.currentPortraitGroup = "groupCommanderPortraits_UNDEAD";
        }
        else if (Globals.currentPortraitGroup == "groupCommanderPortraits_UNDEAD")
        {
            Debug.Log("Current portrait group is UNDEAD. Now changing to AMPHIBIAN.");
            panelPortraitGroupUndead.SetActive(false);
            panelPortraitGroupAmphibian.SetActive(true);
            Globals.currentPortraitGroup = "groupCommanderPortraits_AMPHIBIAN";
        }
    }


    public void PreviousPortraitGroup()
    {
        if (Globals.currentPortraitGroup == "groupCommanderPortraits_AMPHIBIAN")
        {
            Debug.Log("Current portrait group is AMPHIBIAN. Now changing back to UNDEAD.");
            panelPortraitGroupAmphibian.SetActive(false);
            panelPortraitGroupUndead.SetActive(true);
            Globals.currentPortraitGroup = "groupCommanderPortraits_UNDEAD";
        }
        else if (Globals.currentPortraitGroup == "groupCommanderPortraits_UNDEAD")
        {
            Debug.Log("Current portrait group is UNDEAD. Now changing back to REPTILIAN.");
            panelPortraitGroupUndead.SetActive(false);
            panelPortraitGroupReptilian.SetActive(true);
            Globals.currentPortraitGroup = "groupCommanderPortraits_REPTILIAN";
        }
        else if (Globals.currentPortraitGroup == "groupCommanderPortraits_REPTILIAN")
        {
            Debug.Log("Current portrait group is REPTILIAN. Now changing back to PHYTO.");
            panelPortraitGroupReptilian.SetActive(false);
            panelPortraitGroupPhyto.SetActive(true);
            Globals.currentPortraitGroup = "groupCommanderPortraits_PHYTO";
        }
        else if (Globals.currentPortraitGroup == "groupCommanderPortraits_PHYTO")
        {
            Debug.Log("Current portrait group is PHYTO. Now changing back to HUMAN.");
            panelPortraitGroupPhyto.SetActive(false);
            panelPortraitGroupHuman.SetActive(true);
            Globals.currentPortraitGroup = "groupCommanderPortraits_HUMAN";
        }
        else if (Globals.currentPortraitGroup == "groupCommanderPortraits_HUMAN")
        {
            Debug.Log("Current portrait group is HUMAN. Now changing back to ETHEREAL.");
            panelPortraitGroupHuman.SetActive(false);
            panelPortraitGroupEthereal.SetActive(true);
            Globals.currentPortraitGroup = "groupCommanderPortraits_ETHEREAL";
        }
        else if (Globals.currentPortraitGroup == "groupCommanderPortraits_ETHEREAL")
        {
            Debug.Log("Current portrait group is ETHEREAL. Now changing back to ELEMENTAL.");
            panelPortraitGroupEthereal.SetActive(false);
            panelPortraitGroupElemental.SetActive(true);
            Globals.currentPortraitGroup = "groupCommanderPortraits_ELEMENTAL";
        }
        else if (Globals.currentPortraitGroup == "groupCommanderPortraits_ELEMENTAL")
        {
            Debug.Log("Current portrait group is ELEMENTAL. Now changing back to DRAGON.");
            panelPortraitGroupElemental.SetActive(false);
            panelPortraitGroupDragon.SetActive(true);
            Globals.currentPortraitGroup = "groupCommanderPortraits_DRAGON";
        }
        else if (Globals.currentPortraitGroup == "groupCommanderPortraits_DRAGON")
        {
            Debug.Log("Current portrait group is DRAGON. Now changing back to DEMON.");
            panelPortraitGroupDragon.SetActive(false);
            panelPortraitGroupDemon.SetActive(true);
            Globals.currentPortraitGroup = "groupCommanderPortraits_DEMON";
        }
        else if (Globals.currentPortraitGroup == "groupCommanderPortraits_DEMON")
        {
            Debug.Log("Current portrait group is DEMON. Now changing back to BUG.");
            panelPortraitGroupDemon.SetActive(false);
            panelPortraitGroupBug.SetActive(true);
            Globals.currentPortraitGroup = "groupCommanderPortraits_BUG";
        }
        else if (Globals.currentPortraitGroup == "groupCommanderPortraits_BUG")
        {
            Debug.Log("Current portrait group is BUG. Now changing back to BRUTE.");
            panelPortraitGroupBug.SetActive(false);
            panelPortraitGroupBrute.SetActive(true);
            Globals.currentPortraitGroup = "groupCommanderPortraits_BRUTE";
        }
        else if (Globals.currentPortraitGroup == "groupCommanderPortraits_BRUTE")
        {
            Debug.Log("Current portrait group is BRUTE. Now changing back to BEAST.");
            panelPortraitGroupBrute.SetActive(false);
            panelPortraitGroupBeast.SetActive(true);
            Globals.currentPortraitGroup = "groupCommanderPortraits_BEAST";
        }
        else if (Globals.currentPortraitGroup == "groupCommanderPortraits_BEAST")
        {
            Debug.Log("Current portrait group is BEAST. Now changing back to AVIAN.");
            panelPortraitGroupBeast.SetActive(false);
            panelPortraitGroupAvian.SetActive(true);
            Globals.currentPortraitGroup = "groupCommanderPortraits_AVIAN";
        }
        else if (Globals.currentPortraitGroup == "groupCommanderPortraits_AVIAN")
        {
            Debug.Log("Current portrait group is AVIAN. Now changing back to AQUATIC.");
            panelPortraitGroupAvian.SetActive(false);
            panelPortraitGroupAquatic.SetActive(true);
            Globals.currentPortraitGroup = "groupCommanderPortraits_AQUATIC";
        }
        else if (Globals.currentPortraitGroup == "groupCommanderPortraits_AQUATIC")
        {
            Debug.Log("Current portrait group is AQUATIC. Now changing back to AMPHIBIAN.");
            panelPortraitGroupAquatic.SetActive(false);
            panelPortraitGroupAmphibian.SetActive(true);
            Globals.currentPortraitGroup = "groupCommanderPortraits_AMPHIBIAN";
        }
    }

    public void SetChosenCommander(string commanderName)            // When the user clicks a portrait, the name of that chosen commander is stored globally
    {
        MainCharacter.chosenCommander = commanderName;
        Debug.Log("The chosen commander is " + MainCharacter.chosenCommander + ". Saved to global variable chosenCommander");
    }

    public void SaveCommanderName()                                 // When user clicks ACCEPT and moves from panelCharacterCreation to panelCharacterSetup 
    {
        if(inputPlayerName.text != "")                              // If player didn't leave the textbox blank
        {
            MainCharacter.name = inputPlayerName.text;              // Get the name entered into the textbox and save it globally 
        }
        else
        {
            MainCharacter.name = MainCharacter.chosenCommander;           // Else just use the default commander name
        }                      
    }

    public void SetCommanderName()                                  // When user clicks ACCEPT
    {
        txtName.text = MainCharacter.name;                          // Transfer the name from the textbox in panelCharacterCreation to panelCharacterSetup
    }

    public void SetCommanderRace(string race)
    {
        MainCharacter.race = race;
        Debug.Log("Chosen commander race, as read from MainCharacter.race = " + MainCharacter.race); 

        SetCommanderFactionAffinity(race);
    }

    public void SetAllAffinitiesToZero()
    {
        MainCharacter.affinityAMPHIBIAN = 0;
        MainCharacter.affinityAQUATIC = 0;
        MainCharacter.affinityAVIAN = 0;
        MainCharacter.affinityBEAST = 0;
        MainCharacter.affinityBRUTE = 0;
        MainCharacter.affinityBUG = 0;
        MainCharacter.affinityDEMON = 0;
        MainCharacter.affinityDRAGON = 0;
        MainCharacter.affinityELEMENTAL = 0;
        MainCharacter.affinityETHEREAL = 0;
        MainCharacter.affinityHUMAN = 0;
        MainCharacter.affinityPHYTO = 0;
        MainCharacter.affinityREPTILIAN = 0;
        MainCharacter.affinityUNDEAD = 0;
    }

    public void SetCommanderFactionAffinity(string race)
    {
        SetAllAffinitiesToZero();

        if (race == "Amphibian")
        {
            MainCharacter.affinityAMPHIBIAN = 100;
            Debug.Log("Faction Affinity for " + race + " = " + MainCharacter.affinityAMPHIBIAN);
        }
        else if (race == "Aquatic")
        {
            MainCharacter.affinityAQUATIC = 100;
            Debug.Log("Faction Affinity for " + race + " = " + MainCharacter.affinityAQUATIC);
        }
        else if (race == "Avian")
        {
            MainCharacter.affinityAVIAN = 100;
            Debug.Log("Faction Affinity for " + race + " = " + MainCharacter.affinityAVIAN);
        }
        else if (race == "Beast")
        {
            MainCharacter.affinityBEAST = 100;
            Debug.Log("Faction Affinity for " + race + " = " + MainCharacter.affinityBEAST);
        }
        else if (race == "Brute")
        {
            MainCharacter.affinityBRUTE = 100;
            Debug.Log("Faction Affinity for " + race + " = " + MainCharacter.affinityBRUTE);
        }
        else if (race == "Bug")
        {
            MainCharacter.affinityBUG = 100;
            Debug.Log("Faction Affinity for " + race + " = " + MainCharacter.affinityBUG);
        }
        else if (race == "Demon")
        {
            MainCharacter.affinityDEMON = 100;
            Debug.Log("Faction Affinity for " + race + " = " + MainCharacter.affinityDEMON);
        }
        else if (race == "Dragon")
        {
            MainCharacter.affinityDRAGON = 100;
            Debug.Log("Faction Affinity for " + race + " = " + MainCharacter.affinityDRAGON);
        }
        else if (race == "Elemental")
        {
            MainCharacter.affinityELEMENTAL = 100;
            Debug.Log("Faction Affinity for " + race + " = " + MainCharacter.affinityELEMENTAL);
        }
        else if (race == "Ethereal")
        {
            MainCharacter.affinityETHEREAL = 100;
            Debug.Log("Faction Affinity for " + race + " = " + MainCharacter.affinityETHEREAL);
        }
        else if (race == "Human")
        {
            MainCharacter.affinityHUMAN = 100;
            Debug.Log("Faction Affinity for " + race + " = " + MainCharacter.affinityHUMAN);
        }
        else if (race == "Phyto")
        {
            MainCharacter.affinityPHYTO = 100;
            Debug.Log("Faction Affinity for " + race + " = " + MainCharacter.affinityPHYTO);
        }
        else if (race == "Reptilian")
        {
            MainCharacter.affinityREPTILIAN = 100;
            Debug.Log("Faction Affinity for " + race + " = " + MainCharacter.affinityREPTILIAN);
        }
        else if (race == "Undead")
        {
            MainCharacter.affinityUNDEAD = 100;
            Debug.Log("Faction Affinity for " + race + " = " + MainCharacter.affinityUNDEAD);
        }

        
    }

    public void SetImageCharacterSetup()
    {
        if (MainCharacter.chosenCommander == "AMPHORA")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitAmphibianAMPHORA;
        }
        else if (MainCharacter.chosenCommander == "CROACK")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitAmphibianCROACK;
        }
        else if (MainCharacter.chosenCommander == "GULLET")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitAmphibianGULLET;
        }
        else if (MainCharacter.chosenCommander == "ROG")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitAmphibianROG;
        }
        else if (MainCharacter.chosenCommander == "SALLA")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitAmphibianSALLA;
        }
        else if (MainCharacter.chosenCommander == "BLUB")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitAquaticBLUB;
        }
        else if (MainCharacter.chosenCommander == "MERA")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitAquaticMERA;
        }
        else if (MainCharacter.chosenCommander == "OCTUS")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitAquaticOCTUS;
        }
        else if (MainCharacter.chosenCommander == "SHAREK")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitAquaticSHAREK;
        }
        else if (MainCharacter.chosenCommander == "THULLU")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitAquaticTHULLU;
        }
        else if (MainCharacter.chosenCommander == "BILL")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitAvianBILL;
        }
        else if (MainCharacter.chosenCommander == "GIBBS")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitAvianGIBBS;
        }
        else if (MainCharacter.chosenCommander == "HARPETTE")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitAvianHARPETTE;
        }
        else if (MainCharacter.chosenCommander == "IZORA")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitAvianIZORA;
        }
        else if (MainCharacter.chosenCommander == "YRAG")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitAvianYRAG;
        }
        else if (MainCharacter.chosenCommander == "BLORN")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitBeastBLORN;
        }
        else if (MainCharacter.chosenCommander == "GARLAND")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitBeastGARLAND;
        }
        else if (MainCharacter.chosenCommander == "LIONEL")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitBeastLIONEL;
        }
        else if (MainCharacter.chosenCommander == "MONKLEY")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitBeastMONKLEY;
        }
        else if (MainCharacter.chosenCommander == "PIGLET")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitBeastPIGLET;
        }
        else if (MainCharacter.chosenCommander == "BLOODBORN")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitBruteBLOODBORN;
        }
        else if (MainCharacter.chosenCommander == "CYCLON")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitBruteCYCLON;
        }
        else if (MainCharacter.chosenCommander == "MOLAK")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitBruteMOLAK;
        }
        else if (MainCharacter.chosenCommander == "MORCA")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitBruteMORCA;
        }
        else if (MainCharacter.chosenCommander == "SHEOG")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitBruteSHEOG;
        }
        else if (MainCharacter.chosenCommander == "ANETTE")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitBugANETTE;
        }
        else if (MainCharacter.chosenCommander == "BUTTERCUP")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitBugBUTTERCUP;
        }
        else if (MainCharacter.chosenCommander == "HIVERA")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitBugHIVERA;
        }
        else if (MainCharacter.chosenCommander == "MANTEL")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitBugMANTEL;
        }
        else if (MainCharacter.chosenCommander == "SECTON")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitBugSECTON;
        }
        else if (MainCharacter.chosenCommander == "DAMIAN")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitDemonDAMIAN;
        }
        else if (MainCharacter.chosenCommander == "KRAM")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitDemonKRAM;
        }
        else if (MainCharacter.chosenCommander == "NETH")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitDemonNETH;
        }
        else if (MainCharacter.chosenCommander == "SEARAIN")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitDemonSEARAIN;
        }
        else if (MainCharacter.chosenCommander == "SUCCUBAH")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitDemonSUCCUBAH;
        }
        else if (MainCharacter.chosenCommander == "CHEN")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitDragonCHEN;
        }
        else if (MainCharacter.chosenCommander == "ISIS")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitDragonISIS;
        }
        else if (MainCharacter.chosenCommander == "IXEN")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitDragonIXEN;
        }
        else if (MainCharacter.chosenCommander == "LOTH")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitDragonLOTH;
        }
        else if (MainCharacter.chosenCommander == "SANG")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitDragonSANG;
        }
        else if (MainCharacter.chosenCommander == "ARCLAN")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitElementalARCLAN;
        }
        else if (MainCharacter.chosenCommander == "HYDRANA")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitElementalHYDRANA;
        }
        else if (MainCharacter.chosenCommander == "IGNUS")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitElementalIGNUS;
        }
        else if (MainCharacter.chosenCommander == "MAG")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitElementalMAG;
        }
        else if (MainCharacter.chosenCommander == "WYD")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitElementalWYD;
        }
        else if (MainCharacter.chosenCommander == "ANGELUS")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitEtherealANGELUS;
        }
        else if (MainCharacter.chosenCommander == "NYX")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitEtherealNYX;
        }
        else if (MainCharacter.chosenCommander == "RAEL")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitEtherealRAEL;
        }
        else if (MainCharacter.chosenCommander == "THANATOS")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitEtherealTHANATOS;
        }
        else if (MainCharacter.chosenCommander == "THONIOS")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitEtherealTHONIOS;
        }
        else if (MainCharacter.chosenCommander == "DACAS")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitHumanDACAS;
        }
        else if (MainCharacter.chosenCommander == "FINEAS")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitHumanFINEAS;
        }
        else if (MainCharacter.chosenCommander == "FORLA")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitHumanFORLA;
        }
        else if (MainCharacter.chosenCommander == "HUNTER")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitHumanHUNTER;
        }
        else if (MainCharacter.chosenCommander == "TAMAZ")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitHumanTAMAZ;
        }
        else if (MainCharacter.chosenCommander == "CALIX")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitPhytoCALIX;
        }
        else if (MainCharacter.chosenCommander == "IRONBARK")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitPhytoIRONBARK;
        }
        else if (MainCharacter.chosenCommander == "OAKLAN")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitPhytoOAKLAN;
        }
        else if (MainCharacter.chosenCommander == "THORNET")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitPhytoTHORNET;
        }
        else if (MainCharacter.chosenCommander == "TULA")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitPhytoTULA;
        }
        else if (MainCharacter.chosenCommander == "AGAMET")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitReptilianAGAMET;
        }
        else if (MainCharacter.chosenCommander == "AMMIT")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitReptilianAMMIT;
        }
        else if (MainCharacter.chosenCommander == "MEDUSEL")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitReptilianMEDUSEL;
        }
        else if (MainCharacter.chosenCommander == "NAGA")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitReptilianNAGA;
        }
        else if (MainCharacter.chosenCommander == "THWARK")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitReptilianTHWARK;
        }
        else if (MainCharacter.chosenCommander == "LAMIA")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitUndeadLAMIA;
        }
        else if (MainCharacter.chosenCommander == "LICHEN")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitUndeadLICHEN;
        }
        else if (MainCharacter.chosenCommander == "QUIETUS")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitUndeadQUIETUS;
        }
        else if (MainCharacter.chosenCommander == "SWORT")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitUndeadSWORT;
        }
        else if (MainCharacter.chosenCommander == "ZOMM")
        {
            imgCharacterSetupPortrait.sprite = spritePortraitUndeadZOMM;
        }
    }

    public void DisplayDefaultMessage()
    {
        txtDescriptionHeader.text = "DRAG THE SLIDERS";
        txtDescription.text = "...to allocate starting points to each stat. \n\n When done, click ACCEPT.";
    }

    public void InfoHPHover()
    {
        txtDescriptionHeader.text = "HEALTH";
        txtDescription.text = "The amount of life you have. \n\n The more you have, the longer you last in battle.";
    }

    public void InfoSPHover()
    {
        txtDescriptionHeader.text = "STAMINA";
        txtDescription.text = "Needed for performing physical attacks and retaliations in battle.";
    }

    public void InfoMPHover()
    {
        txtDescriptionHeader.text = "MANA";
        txtDescription.text = "Needed for casting spells.";
    }

    public void InfoDXHover()
    {
        txtDescriptionHeader.text = "DEXTERITY";
        txtDescription.text = "DODGE physical attacks \n\n Perform CRITICAL strikes \n (extra damage).";
    }

    public void InfoSTRHover()
    {
        txtDescriptionHeader.text = "STRENGTH";
        txtDescription.text = "Increased CRITICAL damage. \n\n Reduced SP costs. \n\n Regain MP after casting a spell.";
    }

    public void InfoSPDHover()
    {
        txtDescriptionHeader.text = "SPEED";
        txtDescription.text = "Your turn in battle comes earlier. \n\n Attack before enemies can retaliate. \n\n Retaliate before enemy attacks land.";
    }

    public void InfoINTHover()
    {
        txtDescriptionHeader.text = "INTELLIGENCE";
        txtDescription.text = "Increased spell strength. \n\n Perform CRITICAL spells \n\n Increased magic resistance.";
    }

    public void PrepareAdventureMain()              // Things needed to initialize panelAdventureMain
    {
        Globals.characterHasBeenSetup = true;
        Debug.Log("Character has been set up.");
    }
}
